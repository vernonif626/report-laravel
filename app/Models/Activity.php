<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'activity_date_id',
        'date',
        'activity_type_id',
        'role',
        'document_type',
        'quantity',
        'activity_minutes',
        'average_time',
        'travel_from',
        'travel_to',
        'km',
        'km_google',
        'travel_minutes',
        'travel_minutes_google',
        'client_id',
        'address_id',
        'target_user_id',
        'target_user_name',
        'task_id',
        'course_id',
        'task_matched',
        'note',
        'f626_date',
        'exit_status',
        'rdc_pa',
        'value',
        'outcome_in_person',
        'outcome_call',
        'outcome_pa',
];

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function activityDate(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(ActivityDate::class);
    }

    public function activityType(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(ActivityType::class, 'activity_type_id');
    }

    protected function name(): Attribute
    {
        $name = $this->activityType->name;

        $extra = '';
        if ($this->role) {
            $extra .= match($this->role) {
                'r' => ' (RSPP)',
                'h' => ' (HACCP)',
            };
        }

        if ($this->document_type) {
            $extra .= match($this->document_type) {
                'd' => ' (DVR)',
                'h' => ' (Man. HACCP)',
                'di' => ' (Integraz. DVR)',
                'hi' => ' (integraz. man. HACCP)',
            };
        }
        return Attribute::make(
            get: fn ($value) => $name . $extra,
        );

    }

    /**
     * Aggiunge alla tabella activities il tempo medio attività per l'intervallo di date indicato.<br>
     * Funzione temporanea da utilizzare una volta sola da linea di comando (tinker): in seguito i dati di verranno inseriti automaticamente.
     *
     * @param string $dateStart Data iniziale in formato Y-m-d (aaaa-mm-gg)
     * @param string $dateEnd Data finale in formato Y-m-d (aaaa-mm-gg)
     * @return void
     */
    public static function addMissingAverageTimeData(string $dateStart, string $dateEnd): void
    {
        $activities = self::where([['date', '>=', $dateStart], ['date', '<=', $dateEnd]])->get();
        foreach ($activities as $activity) {
            $activity->update(['average_time' => $activity->activityType->average_time ?: null]);
        }
    }
}
