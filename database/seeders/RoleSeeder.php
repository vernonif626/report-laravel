<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!Role::where('name', 'admin')->count()) {
            Role::create(['name' => 'admin']);
        }
        if (!Role::where('name', 'responsabile')->count()) {
            Role::create(['name' => 'responsabile']);
        }
        if (!Role::where('name', 'operativo')->count()) {
            Role::create(['name' => 'operativo']);
        }
        if (!Role::where('name', 'tecnico')->count()) {
            $tecnico = Role::create(['name' => 'tecnico']);
            $tecnico->givePermissionTo('compilare report');
        }
        if (!Role::where('name', 'responsabile tecnici')->count()) {
            $resp_tecnici = Role::create(['name' => 'responsabile tecnici']);
            $resp_tecnici->givePermissionTo('visualizzare report');
            $resp_tecnici->givePermissionTo('visualizzare report tecnici');
            $resp_tecnici->givePermissionTo('esportare report');
            $resp_tecnici->givePermissionTo('esportare report tecnici');
        }
        if (!Role::where('name', 'addetto customer care')->count()) {
            $customer_care = Role::create(['name' => 'addetto customer care']);
            $customer_care->givePermissionTo('compilare report');
        }
        if (!Role::where('name', 'responsabile customer care')->count()) {
            $resp_customer_care = Role::create(['name' => 'responsabile customer care']);
            $resp_customer_care->givePermissionTo('visualizzare report');
            $resp_customer_care->givePermissionTo('visualizzare report customer care');
            $resp_customer_care->givePermissionTo('esportare report');
            $resp_customer_care->givePermissionTo('esportare report customer care');
        }
        if (!Role::where('name', 'responsabile venditori')->count()) {
            $resp_venditori = Role::create(['name' => 'responsabile venditori']);
            $resp_venditori->givePermissionTo('visualizzare report');
            $resp_venditori->givePermissionTo('visualizzare report venditori');
            $resp_venditori->givePermissionTo('esportare report');
            $resp_venditori->givePermissionTo('esportare report venditori');
        }
        if (!Role::where('name', 'business process manager')->count()) {
            $bpm = Role::create(['name' => 'business process manager']);
            $bpm->givePermissionTo('visualizzare report');
            $bpm->givePermissionTo('visualizzare report tecnici');
            $bpm->givePermissionTo('visualizzare report customer care');
            $bpm->givePermissionTo('esportare report');
            $bpm->givePermissionTo('esportare report tecnici');
            $bpm->givePermissionTo('esportare report customer care');
        }
    }
}
