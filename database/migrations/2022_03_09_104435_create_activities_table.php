<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activities', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('activity_date_id');
            $table->date('date');
            $table->unsignedBigInteger('activity_type_id');
            $table->unsignedTinyInteger('quantity')->nullable();
            $table->unsignedSmallInteger('activity_minutes')->nullable();
            $table->unsignedSmallInteger('km')->nullable();
            $table->unsignedSmallInteger('travel_minutes')->nullable();
            $table->unsignedBigInteger('client_id')->nullable()->comment("ID cliente del gestionale");
            $table->unsignedBigInteger('address_id')->nullable()->comment("ID unità locale del gestionale");
            $table->unsignedBigInteger('target_user_id')->nullable()->comment("ID dell'utente per cui viene eseguita questa attività (es. preso appuntamento per un tecnico)");
            $table->string('target_user_name')->nullable()->comment("Nome dell'utente per cui viene eseguita questa attività");

            $table->foreign('activity_type_id')
                ->references('id')
                ->on('activity_types')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            // Disabilito la chiave sull'utente "target" (es. affiancamento): è previsto che si possa sceglierne
            // uno esistente oppure indicare "altro", che ovviamente non ha un id corrispondente.
            /*
            $table->foreign('target_user_id')
                ->references('id')
                ->on('users');
            */

            $table->foreign('activity_date_id')
                ->references('id')
                ->on('activity_dates')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activities');
    }
};
