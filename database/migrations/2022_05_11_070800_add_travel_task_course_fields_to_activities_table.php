<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('activities', function (Blueprint $table) {
            $table->unsignedMediumInteger('task_id')->nullable()->comment("Id del task gestito per questa attività");
            $table->unsignedMediumInteger('course_id')->nullable()->comment("Id del corso svolto da docente");
            $table->string('travel_from')->nullable()->after('activity_minutes')->comment("Località di partenza");
            $table->string('travel_to')->nullable()->after('travel_from')->comment("Località di destinazione");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('activities', function (Blueprint $table) {
            $table->dropColumn(['task_id', 'course_id', 'travel_from', 'travel_to']);
        });
    }
};
