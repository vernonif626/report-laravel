<x-app-layout>
    <x-slot name="header">
        <div class="container mt-5 mb-5">
            <h2>Esportazione</h2>
        </div>
    </x-slot>
    <div class="container">

        @can('esportare report')
            <h3>Dati statistici</h3>
            <form method="post" action="{{ route('report.export') }}" class="mb-4">
                @csrf

                <div class="row">

                    <div class="col-md-3 mb-3">
                        <input
                            name="date_start"
                            type="date"
                            class="form-control"
                            aria-describedby="dateStartDesc"
                            style="max-height: 38px; /*safari patch*/"
                            required
                            value="{{ now()->format('Y-m-d') }}"
                            max="{{ now()->format('Y-m-d') }}"
                        >
                        <div id="dateStartDesc" class="form-text">Data iniziale</div>
                    </div>

                    <div class="col-md-3 mb-3">
                        <input
                            name="date_end"
                            type="date"
                            class="form-control"
                            aria-describedby="dateEndDesc"
                            style="max-height: 38px; /*safari patch*/"
                            max="{{ now()->format('Y-m-d') }}"
                        >
                        <div id="dateEndDesc" class="form-text">Data finale, opzionale</div>
                    </div>

                    {{-- Reparto --}}
                    <div class="col-lg-3 mb-3">
                        <select
                            class="form-select"
                            name="department"
                            value="{{ old('department') }}"
                            aria-describedby="departmentDesc"
                            required
                        >
                            @can('esportare report tecnici')
                                <option value="tecnici">Tecnici</option>
                            @endcan
                            @can('esportare report tecnici')
                                <option value="customer care">Customer care</option>
                            @endcan
                            @can('esportare report venditori')
                                <option value="venditori">Venditori</option>
                            @endcan
                            @can('esportare report marketing')
                                <option value="venditori_marketing">Venditori (per Marketing)</option>
                            @endcan
                            @can('esportare report amministrazione')
                                <option value="amministrazione">Amministrazione</option>
                            @endcan
                        </select>
                        <div id="departmentDesc" class="form-text">Reparto</div>
                    </div>

                    @can('esportare report marketing')
                        {{-- Venditore --}}
                        <div class="col-lg-3 mb-3">
                            <select
                                class="form-select"
                                name="user_id"
                                value="{{ old('user_id') }}"
                                aria-describedby="userIdDesc"
                                required
                            >
                                <option value="0">Tutti</option>
                                @foreach(\App\Models\User::role('venditore')->orderBy('name')->get() as $user)
                                    <option value="{{ $user->id }}">{{ $user->fullName }}</option>
                                @endforeach
                            </select>
                            <div id="userIdDesc" class="form-text">Venditore</div>
                        </div>
                    @endcan

                    <div class="col-md-3">
                        <button type="submit" class="btn btn-outline-primary">Scarica file Excel</button>
                    </div>

                </div>
            </form>
        @endcan


        @can('esportare report km')
            <h3>Excel per rimborsi chilometrici</h3>
            <form method="post" action="{{ route('report.export_km') }}" class="mb-4">
                @csrf

                <div class="row">

                    <div class="col-md-3 mb-3">
                        <input
                            name="date_start"
                            type="date"
                            class="form-control"
                            aria-describedby="dateStartDesc"
                            style="max-height: 38px; /*safari patch*/"
                            required
                            value="{{ now()->format('Y-m-d') }}"
                            max="{{ now()->format('Y-m-d') }}"
                        >
                        <div id="dateStartDesc" class="form-text">Data iniziale</div>
                    </div>

                    <div class="col-md-3 mb-3">
                        <input
                            name="date_end"
                            type="date"
                            class="form-control"
                            aria-describedby="dateEndDesc"
                            style="max-height: 38px; /*safari patch*/"
                            required
                            value="{{ now()->format('Y-m-d') }}"
                            max="{{ now()->format('Y-m-d') }}"
                        >
                        <div id="dateEndDesc" class="form-text">Data finale</div>
                    </div>

                    <div class="col-md-3">
                        <button type="submit" class="btn btn-outline-primary">Scarica file Excel</button>
                    </div>

                </div>
            </form>
        @endcan


        @can('controllare rimborsi')
            <h3>Verifica rimborsi chilometrici</h3>
            <form method="post" action="{{ route('report.export_km') }}" class="mb-4">
                @csrf

                <div class="row">

                    <div class="col-md-3 mb-3">
                        <input
                            name="date_start"
                            type="date"
                            class="form-control"
                            aria-describedby="dateStartDesc"
                            style="max-height: 38px; /*safari patch*/"
                            required
                            value="{{ now()->format('Y-m-d') }}"
                            max="{{ now()->format('Y-m-d') }}"
                        >
                        <div id="dateStartDesc" class="form-text">Data iniziale</div>
                    </div>

                    <div class="col-md-3 mb-3">
                        <input
                            name="date_end"
                            type="date"
                            class="form-control"
                            aria-describedby="dateEndDesc"
                            style="max-height: 38px; /*safari patch*/"
                            required
                            value="{{ now()->format('Y-m-d') }}"
                            max="{{ now()->format('Y-m-d') }}"
                        >
                        <div id="dateEndDesc" class="form-text">Data finale</div>
                    </div>

                    {{-- Persona --}}
                    <div class="col-lg-3 mb-3">
                        <select
                            class="form-select"
                            name="user_id"
                            aria-describedby="userIdDesc"
                            required
                        >
                            @foreach($trasfertisti as $user)
                                <option value="{{ $user->id }}">{{ $user->fullName }}</option>
                            @endforeach
                        </select>
                        <div id="userIdDesc" class="form-text">Persona</div>
                    </div>

                    <div class="col-md-3">
                        <button type="submit" class="btn btn-outline-primary">Scarica file Excel</button>
                    </div>

                </div>
            </form>
        @endcan




    </div>
</x-app-layout>
