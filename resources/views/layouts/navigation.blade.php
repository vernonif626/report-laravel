<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #eee">{{-- bg-light navbar-dark bg-dark --}}
    <div class="container-fluid">
        <a class="navbar-brand" href="{{ route('dashboard') }}">
            <img src="{{ asset('img/logo.svg') }}" width="100" height="28">
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            @auth
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    {{-- Dashboard --}}
                    <li class="nav-item">
                        <a class="nav-link {{ Route::currentRouteName() == 'dashboard' ? 'active' : '' }}" aria-current="page" href="{{ route('dashboard') }}">
                            Dashboard
                            @if(auth()->user()->unfinishedReports->count())
                                <span class="badge rounded-pill bg-danger">{{ auth()->user()->unfinishedReports->count() }}</span>
                            @endif
                        </a>
                    </li>

                    {{-- Report compilati --}}
                    @can('compilare report')
                    <li class="nav-item">
                        <a class="nav-link {{ Route::currentRouteName() == 'report.user_index' ? 'active' : '' }}" aria-current="page" href="{{ route('report.user_index', auth()->user()) }}">Report compilati</a>
                    </li>
                    @endcan

                    {{-- Esportazioni --}}
                    @canany(['esportare report', 'controllare rimborsi', 'esportare report km'])
                    <li class="nav-item">
                        <a class="nav-link {{ Route::currentRouteName() == 'report.export_index' ? 'active' : '' }}" href="{{ route('report.export_index') }}">Esportazione</a>
                    </li>
                    @endcanany

                    {{-- Vendite --}}
                    @can('visualizzare report venditori')
                    <li class="nav-item">
                        <a class="nav-link {{ Route::currentRouteName() == 'report.vendite_show' ? 'active' : '' }}" href="{{ route('report.vendite_show', ['date_start' => 'oggi']) }}">Vendite</a>
                    </li>
                    @endcan

                    {{-- Per avere il menù utente a dx, togliere il commento alle due righe seguenti --}}

{{--                </ul>--}}
{{--                <ul class="navbar-nav mb-2 mb-lg-0">--}}

                    {{-- Nome utente + log out --}}
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            {{ Auth::user()->full_name }}
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li>
                                <form method="POST" action="{{ route('logout') }}">
                                    @csrf
                                    <a class="dropdown-item" href="#" onclick="event.preventDefault(); this.closest('form').submit();">
                                        {{ __('Log Out') }}
                                    </a>
                                </form>
                            </li>
                            {{--                            <li><hr class="dropdown-divider"></li>--}}
                        </ul>
                    </li>
                </ul>
            @endauth
        </div>
    </div>
</nav>
