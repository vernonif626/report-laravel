<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder6 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Silvia Bigolaro è già presente con ruolo responsabile venditori.
        // NB Silvia Bigolaro ed Erika Moretti condividono l'account "Vendite".
        // Aggiungo Erika Moretti
        $moretti = User::create([
            'name'         => 'Erika',
            'surname'      => 'Moretti',
            'email'        => 'e.moretti@facile626.it',
            'password'     => (app()->environment('local') ? Hash::make('123') : Hash::make('YY8KgT')),
            'f626_user_id' => 57009,
        ]);
        $moretti->assignRole('responsabile', 'responsabile venditori');

        // Venditori

        // Grazia Fiorentino è già presente
        $fiorentino = User::where('surname', 'Fiorentino')->first();
        $fiorentino->assignRole(['venditore', 'operativo']);

        $venditori = [
            [
                'name'         => 'Facile626',
                'surname'      => 'Vendite',
                'email'        => 'vendite626@facile626.it',
                'password'     => (app()->environment('local') ? Hash::make('123') : Hash::make('&OCm')),
                'f626_user_id' => 59281,
            ],
            [
                'name'         => 'Giovanna',
                'surname'      => 'Carpi',
                'email'        => 'g.carpi@facile626.it',
                'password'     => (app()->environment('local') ? Hash::make('123') : Hash::make('K2J67')),
                'f626_user_id' => 68670,
            ],
            [
                'name'         => 'Paolo',
                'surname'      => 'Colautti',
                'email'        => 'p.colautti@facile626.it',
                'password'     => (app()->environment('local') ? Hash::make('123') : Hash::make('Af32Y')),
                'f626_user_id' => 72198,
            ],
            [
                'name'         => 'Bruno',
                'surname'      => 'Croce',
                'email'        => 'b.croce@facile626.it',
                'password'     => (app()->environment('local') ? Hash::make('123') : Hash::make('LdQ97')),
                'f626_user_id' => 73848,
            ],
            [
                'name'         => 'Simone',
                'surname'      => 'Felcaro',
                'email'        => 's.felcaro@facile626.it',
                'password'     => (app()->environment('local') ? Hash::make('123') : Hash::make('xTFvU#')),
                'f626_user_id' => 64112,
            ],
            [
                'name'         => 'Riccardo',
                'surname'      => 'Gallarati',
                'email'        => 'r.gallarati@facile626.it',
                'password'     => (app()->environment('local') ? Hash::make('123') : Hash::make('W&mm')),
                'f626_user_id' => 66338,
            ],
            [
                'name'         => 'Sandro',
                'surname'      => 'Pizzolato',
                'email'        => 's.pizzolato@facile626.it',
                'password'     => (app()->environment('local') ? Hash::make('123') : Hash::make('IqqM%')),
                'f626_user_id' => 64214,
            ],
            [
                'name'         => 'Donatello',
                'surname'      => 'Rorato',
                'email'        => 'd.rorato@facile626.it',
                'password'     => (app()->environment('local') ? Hash::make('123') : Hash::make('AxYu')),
                'f626_user_id' => 57074,
            ],
            [
                'name'         => 'Matteo',
                'surname'      => 'Serena',
                'email'        => 'm.serena@facile626.it',
                'password'     => (app()->environment('local') ? Hash::make('123') : Hash::make('rKObGn')),
                'f626_user_id' => 54409,
            ],
            [
                'name'         => 'Erika',
                'surname'      => 'Spada',
                'email'        => 'e.spada@facile626.it',
                'password'     => (app()->environment('local') ? Hash::make('123') : Hash::make('PApk')),
                'f626_user_id' => 71806,
            ],
            [
                'name'         => 'Walther',
                'surname'      => 'Valentini',
                'email'        => 'w.valentini@facile626.it',
                'password'     => (app()->environment('local') ? Hash::make('123') : Hash::make('r6ci&')),
                'f626_user_id' => 69362,
            ],
            [
                'name'         => 'Paolo',
                'surname'      => 'Zampini',
                'email'        => 'p.zampini@facile626.it',
                'password'     => (app()->environment('local') ? Hash::make('123') : Hash::make('YPmVOt')),
                'f626_user_id' => 55714,
            ],
        ];

        foreach ($venditori as $venditore) {
            $u = User::create([
                'name' => $venditore['name'],
                'surname' => $venditore['surname'],
                'email' => $venditore['email'],
                'password' => $venditore['password'],
                'f626_user_id' => $venditore['f626_user_id'],
            ]);
            $u->assignRole(['venditore', 'operativo']);
        }

    }
}
