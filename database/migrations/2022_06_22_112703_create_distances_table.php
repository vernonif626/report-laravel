<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('distances', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('origins');
            $table->string('destinations');
            $table->timestamp('arrival_time')->nullable();
            $table->string('avoid', 15)->nullable();
            $table->timestamp('departure_time')->nullable();
            $table->string('language', 5)->nullable();
            $table->string('mode', 15)->nullable();
            $table->string('region', 2)->nullable();
            $table->string('transit_mode', 15)->nullable();
            $table->string('transit_routing_preference', 20)->nullable();
            $table->string('units', 10)->nullable();
            $table->text('response'); // MariaDB non supporta i campi JSON, salvo come testo
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('distances');
    }
};
