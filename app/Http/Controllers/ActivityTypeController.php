<?php

namespace App\Http\Controllers;

use App\Models\ActivityType;
use Illuminate\Http\Request;

class ActivityTypeController extends Controller
{
    /**
     * Risponde alla chiamata ajax con il render degli elementi HTML dell'attività selezionata dall'utente
     *
     * @param Request $request
     * @return mixed
     */
    public function htmlElements(Request $request)
    {
        $validated = $request->validate([
            'activity_type' => 'required|integer',
        ]);
        return ActivityType::find($validated['activity_type'])->renderHtml();
    }
}
