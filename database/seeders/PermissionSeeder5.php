<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionSeeder5 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!Permission::where('name', 'trasferta')->count()) {
            // Creo il permesso "trasferta"
            $permissionTrasferta = Permission::create(['name' => 'trasferta']);

            // Assegno il permesso al ruolo "tecnico"
            $roleTecnico = Role::where('name', 'tecnico')->first();
            $roleTecnico->givePermissionTo($permissionTrasferta);
        }
    }
}
