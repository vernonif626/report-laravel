<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleSeeder2 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!Role::where('name', 'venditore')->count()) {
            $roleVenditore = Role::create(['name' => 'venditore']);
            $roleVenditore->givePermissionTo('compilare report', 'trasferta', 'esportare report km');
        }
    }
}
