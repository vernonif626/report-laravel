<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ActivityCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('activity_categories')->insert([
            [ 'name' => "Consegna doc"],
            [ 'name' => "Contatto"],
            [ 'name' => "Docenza"],
            [ 'name' => "Formazione interna"],
            [ 'name' => "Gestionale"],
            [ 'name' => "Imprevisto"],
            [ 'name' => "Pianificazione"],
            [ 'name' => "Redazione doc"],
            [ 'name' => "Regia"],
            [ 'name' => "Riunione"],
            [ 'name' => "Social"],
            [ 'name' => "Sopralluogo"],
        ]);
    }
}
