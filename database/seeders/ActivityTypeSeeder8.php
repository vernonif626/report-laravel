<?php

namespace Database\Seeders;

use App\Models\ActivityCategory;
use App\Models\ActivityType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ActivityTypeSeeder8 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = ActivityCategory::all()->pluck('id', 'name');

        ActivityType::where('name', "Consegna documento")->update([
            'fields' => "document_type,activity_minutes,travel_from*,travel_to*,km*,travel_minutes*,client_id,address_id,task_id",
            'description' => "Spiegazione documento in presenza/telefonica"
        ]);

        ActivityType::where('name', "Firma nomina (telefono)")->update([
            'name' => "Firma nomina",
            'description' => "Firma della nomina (in presenza o telefonica) non eseguita durante il sopralluogo.",
            'fields' => "role,activity_minutes,travel_from*,travel_to*,km*,travel_minutes*,client_id,address_id,task_id",
        ]);

        ActivityType::where('name', "Docenza")->update([
            'fields' => "quantity,activity_minutes,travel_from*,travel_to*,km*,travel_minutes*,course_id",
        ]);

        ActivityType::where('name', "Corso fruito")->update([
            'fields' => "quantity,activity_minutes,travel_from*,travel_to*,km*,travel_minutes*",
        ]);

        ActivityType::where('name', "Riunione")->update([
            'fields' => "activity_minutes,travel_from*,travel_to*,km*,travel_minutes*",
        ]);

        ActivityType::where('name', "Gestione sanzione/ispezione")->update([
            'fields' => "activity_minutes,travel_from*,travel_to*,km*,travel_minutes*,client_id,address_id",
        ]);

        ActivityType::where('name', "Sopralluogo annullato da cliente")->update([
            'fields' => "activity_minutes,travel_from*,travel_to*,km*,travel_minutes*,client_id,address_id,task_id",
            'description' => "Sopralluogo annullato dal cliente (non inserire dati di viaggio se annullato in anticipo rispetto all'appuntamento)",
        ]);

        ActivityType::where('name', "Recensione")->update([
            'description' => "Recensione pubblicata dal cliente",
        ]);

        ActivityType::where('name', "Consulenza interna")->update([
            'description' => "Consulenza fornita ai colleghi",
        ]);

        ActivityType::where('name', "Regia")->update([
            'visible' => 0,
        ]);

        ActivityType::where('name', "Telefonata periodica")->update([
            'visible' => 0,
        ]);

        ActivityType::where('name', "Ticket aperto")->update([
            'tecnico' => 1,
        ]);

        ActivityType::create([
            'name' => "Contatto periodico",
            'description' => "Attività sul cliente (task, telefonata, e-mail, upload) come da ricetta contrattuale",
            'average_time' => null,
            'activity_category_id' => $categories['Contatto'],
            'fields' => "quantity,activity_minutes",
            'paulitti_id' => 0,
            'tecnico' => 1,
            'customer_care' => 0,
            'venditore' => 0,
            'order' => 10,

        ]);
    }
}
