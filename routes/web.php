<?php

use App\Http\Controllers\ActivityController;
use App\Http\Controllers\ActivityTypeController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ActivityDateController;
use App\Http\Controllers\ReportController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('auth.login');
//});

Route::redirect('/', '/dashboard');

Route::middleware(['auth'])->group(function () {

    // Dashboard
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');

    Route::middleware(['can:compilare report'])->group(function () {

        // Activity date
        Route::post('/report', [ActivityDateController::class, 'store'])->name('report.store');
        Route::get('/report/{activity_date}/edit', [ActivityDateController::class, 'edit'])->name('report.edit');
        Route::patch('/report/{activity_date}', [ActivityDateController::class, 'update'])->name('report.update');

        // Activity
        Route::post('/activity', [ActivityController::class, 'store'])->name('activity.store');
        Route::get('/activity/delete/{activity}', [ActivityController::class, 'delete'])->name('activity.destroy');

    });

    // Indice esportazioni
    Route::get('/export_index', [ActivityDateController::class, 'exportIndex'])->name('report.export_index');

    // Esportazione rimborsi km
    Route::post('/report/export_km', [ActivityDateController::class, 'exportKm'])->name('report.export_km');

    // Indice e visualizzazione report inseriti
    Route::get('/report/user/{user}', [ActivityDateController::class, 'userIndex'])->name('report.user_index');
    Route::get('/report/{activity_date}', [ActivityDateController::class, 'show'])->name('report.show');

    // Esportazioni per i responsabili
    Route::post('/report/export', [ActivityDateController::class, 'export'])
        ->name('report.export')
        ->middleware('can:esportare report');
    Route::get('/report/vendite/{date_start}/{date_end?}', [ReportController::class, 'vendite_show'])
        ->name('report.vendite_show')
        ->middleware('can:visualizzare report venditori');
    Route::post('/report/vendite', [ReportController::class, 'vendite_range'])
        ->name('report.vendite_range')
        ->middleware('can:visualizzare report venditori');

    // Chiamate ajax
    Route::prefix('ajax')->group(function () {
        Route::post('/html_elements', [ActivityTypeController::class, 'htmlElements'])->name('activity_type.render');

        // Sblocca (rende "non chiuso") un report
        Route::post('/activity_date/unlock', [ActivityDateController::class, 'unlock'])->name('activity_date.unlock');

        // Collegamento al gestionale, mai andate in produzione
        Route::post('/verify_activity', [ActivityDateController::class, 'f626VerifyActivity'])->name('activity.verify');
        Route::post('/send_activity', [ActivityDateController::class, 'f626SendActivity'])->name('activity.send');
        Route::post('/verify_report', [ActivityDateController::class, 'f626VerifyReport'])->name('report.verify');
    });

});

require __DIR__.'/auth.php';
