<?php

namespace Database\Seeders;

use App\Models\ActivityCategory;
use App\Models\ActivityType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ActivityTypeSeeder6 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = ActivityCategory::all()->pluck('id', 'name');
        //'fields' => "rdc_pa,activity_minutes,quantity,task_id,client_id,value,travel_from*,travel_to*,km*,travel_minutes*,outcome_in_person,outcome_call,outcome_pa,target_user_id",

        // Le attività di affiancamento e rientro a casa sono disponibili anche per i venditori
        ActivityType::where('name', "Affiancamento fatto")->update([
            'venditore' => 1,
            'description' => "Supporto fornito su attività organizzative o tematiche aziendali. Inserire solo il tempo esclusivamente dedicato alla formazione.",
        ]); // paulitti_id = 57
        ActivityType::where('name', "Affiancamento ricevuto")->update(['venditore' => 1]); // paulitti_id = 56
        ActivityType::where('name', "Rientro a casa")->update(['venditore' => 1]); // paulitti_id = 75

        // Dato che sul sito report la categoria dell'attività non viene mai usata,
        // non ho aggiunto nuove categorie per i venditori. Per ora tutte le attività hanno come categoria "Contatto".
        // La colonna "order" non viene più usata, le attività vengono presentate in ordine alfabetico.

        $activityTypes = [
            [
                'name' => "RDC ricevuto",
                'description' => null,
                'average_time' => null,
                'activity_category_id' => $categories['Contatto'],
                'fields' => "task_id",
                'paulitti_id' => 1,
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 1,
                'order' => 10,
            ],
            [
                'name' => "PowerApp inserito",
                'description' => null,
                'average_time' => 10,
                'activity_category_id' => $categories['Contatto'],
                'fields' => "activity_minutes,task_id,client_id,travel_from*,travel_to*,km*,travel_minutes*,outcome_pa",
                'paulitti_id' => 2,
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 1,
                'order' => 10,
            ],
            [
                'name' => "Primo appuntamento",
                'description' => null,
                'average_time' => 90,
                'activity_category_id' => $categories['Contatto'],
                'fields' => "rdc_pa,activity_minutes,task_id,client_id,value*,travel_from*,travel_to*,km*,travel_minutes*,outcome_in_person",
                'paulitti_id' => 3,
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 1,
                'order' => 10,
            ],
            [
                'name' => "Ripasso PA",
                'description' => null,
                'average_time' => 20,
                'activity_category_id' => $categories['Contatto'],
                'fields' => "activity_minutes,task_id,client_id,value*,travel_from*,travel_to*,km*,travel_minutes*,outcome_pa",
                'paulitti_id' => 4,
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 1,
                'order' => 10,
            ],
            [
                'name' => "Ripasso contratto",
                'description' => null,
                'average_time' => 60,
                'activity_category_id' => $categories['Contatto'],
                'fields' => "rdc_pa,activity_minutes,task_id,client_id,value*,travel_from*,travel_to*,km*,travel_minutes*,outcome_in_person",
                'paulitti_id' => 5,
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 1,
                'order' => 10,
            ],
            [
                'name' => "Contatto lavorato",
                'description' => null,
                'average_time' => 5,
                'activity_category_id' => $categories['Contatto'],
                'fields' => "rdc_pa,activity_minutes,task_id,client_id,outcome_call",
                'paulitti_id' => 6,
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 1,
                'order' => 10,
            ],
            [
                'name' => "Contratto F626",
                'description' => null,
                'average_time' => 30,
                'activity_category_id' => $categories['Contatto'],
                'fields' => "rdc_pa,activity_minutes,task_id,client_id,value,travel_from*,travel_to*,km*,travel_minutes*",
                'paulitti_id' => 7,
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 1,
                'order' => 10,
            ],
            [
                'name' => "Contratto corsi",
                'description' => null,
                'average_time' => 30,
                'activity_category_id' => $categories['Contatto'],
                'fields' => "rdc_pa,activity_minutes,task_id,client_id,value,travel_from*,travel_to*,km*,travel_minutes*",
                'paulitti_id' => 8,
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 1,
                'order' => 10,
            ],
            [
                'name' => "Incasso",
                'description' => null,
                'average_time' => 20,
                'activity_category_id' => $categories['Contatto'],
                'fields' => "activity_minutes,client_id,value,travel_from*,travel_to*,km*,travel_minutes*",
                'paulitti_id' => 9,
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 1,
                'order' => 10,
            ],
            [
                'name' => "Gradimento",
                'description' => null,
                'average_time' => 20,
                'activity_category_id' => $categories['Contatto'],
                'fields' => "activity_minutes,task_id,client_id,travel_from*,travel_to*,km*,travel_minutes*",
                'paulitti_id' => 10,
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 1,
                'order' => 10,
            ],
            [
                'name' => "Aggiornamento gestionale",
                'description' => null,
                'average_time' => null,
                'activity_category_id' => $categories['Contatto'],
                'fields' => "activity_minutes",
                'paulitti_id' => 11,
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 1,
                'order' => 10,
            ],
            [
                'name' => "Recensione Google/Facebook",
                'description' => null,
                'average_time' => 20,
                'activity_category_id' => $categories['Contatto'],
                'fields' => "activity_minutes,client_id,travel_from*,travel_to*,km*,travel_minutes*",
                'paulitti_id' => 12,
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 1,
                'order' => 10,
            ],
            [
                'name' => "Riunione",
                'description' => null,
                'average_time' => null,
                'activity_category_id' => $categories['Contatto'],
                'fields' => "activity_minutes,travel_from*,travel_to*,km*,travel_minutes*",
                'paulitti_id' => 13,
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 1,
                'order' => 10,
            ],
            [
                'name' => "Appuntamento richiesto dal cliente",
                'description' => null,
                'average_time' => 30,
                'activity_category_id' => $categories['Contatto'],
                'fields' => "activity_minutes,client_id,travel_from*,travel_to*,km*,travel_minutes*",
                'paulitti_id' => 14,
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 1,
                'order' => 10,
            ],
            [
                'name' => "Appuntamento con commercialista o consulente del lavoro",
                'description' => null,
                'average_time' => 90,
                'activity_category_id' => $categories['Contatto'],
                'fields' => "activity_minutes,travel_from*,travel_to*,km*,travel_minutes*",
                'paulitti_id' => 15,
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 1,
                'order' => 10,
            ],
            [
                'name' => "Appuntamento con segnalatore",
                'description' => null,
                'average_time' => 30,
                'activity_category_id' => $categories['Contatto'],
                'fields' => "activity_minutes,travel_from*,travel_to*,km*,travel_minutes*",
                'paulitti_id' => 16,
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 1,
                'order' => 10,
            ],
        ];

        foreach ($activityTypes as $activityType) {
            ActivityType::create($activityType);
        }
    }
}
