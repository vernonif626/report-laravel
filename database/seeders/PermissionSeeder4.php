<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionSeeder4 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Tolgo il permesso di esportazione per i rimborsi chilometrici che avevo assegnato erroneamente a ogni tecnico...
        foreach (User::role(['tecnico'])->get() as $user) {
            $user->revokePermissionTo('esportare report km');
        }
        // ...e lo assegno al tecnico in quanto ruolo.
        $roleTecnico = Role::where('name', 'tecnico')->first();
        $permissionExportKm = Permission::where('name', 'esportare report km')->first();
        $roleTecnico->givePermissionTo($permissionExportKm);

        // Tolgo il permesso di visualizzazione totali che avevo assegnato erroneamente a ogni responsabile...
        foreach (User::role(['responsabile'])->get() as $user) {
            $user->revokePermissionTo('visualizzare totali');
        }
        // ...e lo assegno al responsabile in quanto ruolo.
        $roleResponsabile = Role::where('name', 'responsabile')->first();
        $permissionVisualizzaTotali = Permission::where('name', 'visualizzare totali')->first();
        $roleResponsabile->givePermissionTo($permissionVisualizzaTotali);

    }
}
