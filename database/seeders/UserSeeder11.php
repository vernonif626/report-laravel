<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder11 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Disabilitazione accessi Felcaro
        User::where('email', 's.felcaro@facile626.it')->update([
            'password' => (app()->environment('local') ? Hash::make('123') : Hash::make('iuwerh!£$sfihbgi783iebv87trt')),
        ]);

        // Disabilitazione utenti non più in forza
        User::whereIn('email', ['s.felcaro@facile626.it', 'd.micoli@facile626.it', 'm.morabito@facile626.it', 'n.dellaschiava@facile626.it'])->update([
            'active' => 0,
        ]);
    }
}
