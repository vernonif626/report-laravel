<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('activities', function (Blueprint $table) {
            $table->string('rdc_pa', 20)->after('course_id')->nullable();
            $table->unsignedSmallInteger('value')->after('rdc_pa')->nullable();
            $table->string('outcome_in_person', 20)->after('value')->nullable();
            $table->string('outcome_call', 20)->after('outcome_in_person')->nullable();
            $table->string('outcome_pa', 20)->after('outcome_call')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('activities', function (Blueprint $table) {
            $table->dropColumn(['rdc_pa', 'value', 'outcome_in_person', 'outcome_call', 'outcome_pa']);
        });
    }
};
