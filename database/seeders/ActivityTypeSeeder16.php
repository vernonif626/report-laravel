<?php

namespace Database\Seeders;

use App\Models\ActivityCategory;
use App\Models\ActivityType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ActivityTypeSeeder16 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = ActivityCategory::all()->pluck('id', 'name');

        // Per questa attività vanno inseriti solo i minuti
        ActivityType::where('name', "Approvazione contratto")->update(
            [
                'fields' => "activity_minutes",
            ]
        );

        // Disabilito le attività "Protocollo contratto" e "Emissione fattura corrente" per unirle in un'unica attività
        ActivityType::where('name', "Protocollo contratto")->update(
            [
                'visible' => 0,
            ]
        );

        ActivityType::where('name', "Emissione fattura corrente")->update(
            [
                'visible' => 0,
            ]
        );

        ActivityType::create(
            [
                'name' => "Protocollo contratto ed emissione fattura corrente",
                'description' => "Gestione task protocollo contratto ed emissione fattura corrente",
                'average_time' => 30,
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        // Disabilito l'attività "Report attività settimanale" e aggiungo "Compilazione report giornaliero"
        ActivityType::where('name', "Report attività settimanale")->update(
            [
                'visible' => 0,
            ]
        );

        ActivityType::create(
            [
                'name' => "Compilazione report giornaliero",
                'description' => null,
                'average_time' => 10,
                'activity_category_id' => $categories['KPI'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        // Per questa attività vanno inseriti solo i minuti
        ActivityType::where('name', "Task eSOLVER")->update(
            [
                'fields' => "activity_minutes",
            ]
        );

        // Per questa attività vanno inseriti solo i minuti
        ActivityType::where('name', "Addebiti POS virtuali")->update(
            [
                'fields' => "activity_minutes",
            ]
        );

        // Per questa attività vanno inseriti solo i minuti
        ActivityType::where('name', "Emissione fattura e-commerce")->update(
            [
                'fields' => "activity_minutes",
            ]
        );

        // Attività non più necessaria
        ActivityType::where('name', "Emissione fattura mensile Gallas")->update(
            [
                'visible' => 0,
            ]
        );

        // Attività non più necessaria
        ActivityType::where('name', "Emissione fatture consolidato")->update(
            [
                'visible' => 0,
            ]
        );

        // Attività sostituita da "Invio incassi F626"
        ActivityType::where('name', "Registrazione incasso")->update(
            [
                'visible' => 0,
            ]
        );

        // Attività rinominata
        ActivityType::where('name', "Verifica mensile incassi")->update(
            [
                'name' => "Verifica incassi",
                'description' => null,
            ]
        );

        // Attività rinominata
        ActivityType::where('name', "Invio incassi eSOLVER")->update(
            [
                'name' => "Registrazione incassi eSOLVER",
            ]
        );

        // Attività rinominata
        ActivityType::where('name', "Emissione Ri.Ba.")->update(
            [
                'name' => "Emissione Ri.Ba. / SDD",
            ]
        );

        // Aggiungere la descrizione
        ActivityType::where('name', "Verifica mensile incassi registrati")->update(
            [
                'description' => "Verifica mensile incassi banche, PayPal, Stripe",
            ]
        );

        // Attivare "Fermo produzione" anche all'amministrazione
        ActivityType::where('name', "Fermo produzione")->update(
            [
                'amministrativo' => 1,
            ]
        );

        // Nuova attività "Supporto Gestione Crediti"
        ActivityType::create(
            [
                'name' => "Supporto Gestione Crediti",
                'description' => null,
                'average_time' => 30,
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

    }
}
