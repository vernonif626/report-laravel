<?php

namespace Database\Seeders;

use App\Models\ActivityCategory;
use App\Models\ActivityType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ActivityTypeSeeder19 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = ActivityCategory::all()->pluck('id', 'name');

        // Nuova attività "Emissione fattura XT/NC"
        ActivityType::create(
            [
                'name' => "Emissione fattura XT/NC",
                'description' => null,
                'average_time' => 20,
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "quantity,activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        // Nuova attività "Upload documento"
        ActivityType::create(
            [
                'name' => "Upload documento",
                'description' => null,
                'average_time' => null, // Tempo medio da definire
                'activity_category_id' => $categories['Gestionale'],
                'fields' => "quantity,activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        // Nuova attività "Acquisizione informazioni per protocollo contratti"
        ActivityType::create(
            [
                'name' => "Acquisizione informazioni per protocollo contratti",
                'description' => null,
                'average_time' => null, // Tempo medio da definire
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        // Nuova attività "Verifica versamenti rete vendita"
        ActivityType::create(
            [
                'name' => "Verifica versamenti rete vendita",
                'description' => null,
                'average_time' => null, // Tempo medio da definire
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        // Attività rinominata
        ActivityType::where('name', "Invio incassi F626")->update(
            [
                'name' => "Registrazione e invio incassi F626",
            ]
        );

    }
}
