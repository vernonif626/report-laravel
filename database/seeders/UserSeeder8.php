<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder8 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Martina Gianosi
        $gianosi = User::create([
            'name'         => 'Martina',
            'surname'      => 'Gianosi',
            'email'        => 'm.gianosi@facile626.it',
            'password'     => (app()->environment('local') ? Hash::make('123') : Hash::make('5RX&')),
            'f626_user_id' => 74953,
        ]);
        $gianosi->assignRole('operativo');
        $gianosi->assignRole('addetto customer care');
    }
}
