<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder15 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Davide Colombin
        $colombin = User::create([
            'name'         => 'Davide',
            'surname'      => 'Colombin',
            'email'        => 'd.colombin@facile626.it',
            'password'     => (app()->environment('local') ? Hash::make('123') : Hash::make('52w2i')),
            'f626_user_id' => 75807,
        ]);
        $colombin->assignRole(['tecnico', 'operativo']);

        // Disabilitazione accessi Celaj
        User::where('email', 'f.celaj@facile626.it')->update([
            'password' => (app()->environment('local') ? Hash::make('123') : Hash::make('iuwerh!£$sfihbgi783iebv87trt')),
            'active' => 0,
        ]);
    }
}
