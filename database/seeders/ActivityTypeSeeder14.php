<?php

namespace Database\Seeders;

use App\Models\ActivityCategory;
use App\Models\ActivityType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ActivityTypeSeeder14 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = ActivityCategory::all()->pluck('id', 'name');

        ActivityType::where('name', 'Comunicazioni')->update(['amministrativo' => 1]);
        ActivityType::where('name', 'Ticket aperto')->update(['amministrativo' => 1]);

        ActivityType::create(
            [
                'name' => "Addebiti POS virtuali",
                'description' => null,
                'average_time' => "10",
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "quantity,activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Aggiornamento annuale magazzino ",
                'description' => null,
                'average_time' => "240",
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Aggiornamento business plan e cash flow",
                'description' => null,
                'average_time' => "120",
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Aggiornamento normativo",
                'description' => "Aggiornamenti dal Sole 24 Ore, Pratica Professionale, ecc.",
                'average_time' => "30",
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Aggiornamento pratiche legali",
                'description' => null,
                'average_time' => "30",
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes,client_id",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Aggiornamento report crediti e vendite",
                'description' => null,
                'average_time' => "30",
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Aggiornamento report Omniadata",
                'description' => null,
                'average_time' => "30",
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Approvazione contratto",
                'description' => "Gestione task approvazione contratto",
                'average_time' => "15",
                'activity_category_id' => $categories['Gestionale'],
                'fields' => "activity_minutes,client_id,task_id",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Bonifici stipendi",
                'description' => null,
                'average_time' => "20",
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Compilazione moduli noleggio auto",
                'description' => null,
                'average_time' => "30",
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Emissione fattura corrente",
                'description' => "Gestione task emissione fattura corrente",
                'average_time' => "1",
                'activity_category_id' => $categories['Gestionale'],
                'fields' => "activity_minutes,client_id,task_id",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Emissione fattura e-commerce",
                'description' => "Gestione task fattura e-commerce",
                'average_time' => "10",
                'activity_category_id' => $categories['Gestionale'],
                'fields' => "activity_minutes,client_id,task_id",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Emissione fattura mensile Gallas",
                'description' => null,
                'average_time' => "5",
                'activity_category_id' => $categories['Gestionale'],
                'fields' => "activity_minutes,client_id,task_id",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Emissione fatture consolidato",
                'description' => "Gestione task emissione fatture consolidato",
                'average_time' => "480",
                'activity_category_id' => $categories['Gestionale'],
                'fields' => "quantity,activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Emissione Ri.Ba.",
                'description' => null,
                'average_time' => "13",
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "eSOLVER - Bonifici fornitori e registrazioni contabili",
                'description' => null,
                'average_time' => "5",
                'activity_category_id' => $categories['eSOLVER'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "eSOLVER - Calcolo e versamento IVA e ritenute",
                'description' => "Calcolo e versamento IVA e ritenute per F24 al 16 del mese",
                'average_time' => "60",
                'activity_category_id' => $categories['eSOLVER'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "eSOLVER - Caricamento documenti Confimprenditori",
                'description' => "Raccolta e caricamento documenti Confimprenditori - annuale",
                'average_time' => "2400",
                'activity_category_id' => $categories['eSOLVER'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "eSOLVER - Controllo mastrini clienti",
                'description' => null,
                'average_time' => "10",
                'activity_category_id' => $categories['eSOLVER'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "eSOLVER - Controllo polizza 2 decreti",
                'description' => "Controllo trimestrale polizza 2 decreti",
                'average_time' => "120",
                'activity_category_id' => $categories['eSOLVER'],
                'fields' => "quantity,activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "eSOLVER - Dichiarazione IVA annuale",
                'description' => "Predisposizione e invio dichiarazione IVA annuale",
                'average_time' => "480",
                'activity_category_id' => $categories['eSOLVER'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "eSOLVER - Emissione e invio distinta SDD",
                'description' => "Emissione e invio distinta SDD su Banca Sella",
                'average_time' => "30",
                'activity_category_id' => $categories['eSOLVER'],
                'fields' => "quantity,activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "eSOLVER - Fatture e sezionali IVA",
                'description' => "Predisposizione fatture e sezionali IVA (per omaggi corsi)",
                'average_time' => "120",
                'activity_category_id' => $categories['eSOLVER'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "eSOLVER - Gestione ticket",
                'description' => null,
                'average_time' => "3",
                'activity_category_id' => $categories['eSOLVER'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "eSOLVER - Gestione tutela legale",
                'description' => null,
                'average_time' => "2",
                'activity_category_id' => $categories['eSOLVER'],
                'fields' => "quantity,activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "eSOLVER - Invio CU ai percipienti",
                'description' => null,
                'average_time' => "120",
                'activity_category_id' => $categories['eSOLVER'],
                'fields' => "quantity,activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "eSOLVER - Invio fatture al SDI",
                'description' => "Correzioni errori su import fatture e invio al SDI",
                'average_time' => "20",
                'activity_category_id' => $categories['eSOLVER'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "eSOLVER - Invio LIPE IVA trimestre",
                'description' => "Predisposizione e invio LIPE IVA trimestre",
                'average_time' => "120",
                'activity_category_id' => $categories['eSOLVER'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "eSOLVER - Quietanze F23/F24",
                'description' => "Verifica e stampa quietanze mancanti F23/F24 da cassetto fiscale",
                'average_time' => "10",
                'activity_category_id' => $categories['eSOLVER'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "eSOLVER - Raccolta dati per certificazioni uniche",
                'description' => "Raccolta dati assieme a prospetto excel per certificazione uniche e invio al consulente del lavoro - annuale",
                'average_time' => "960",
                'activity_category_id' => $categories['eSOLVER'],
                'fields' => "quantity,activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "eSOLVER - Recupero documenti per accertamento o ispezione",
                'description' => "Recupero documenti per accertamenti o ispezione (es. Enasarco 2017-2021)",
                'average_time' => null,
                'activity_category_id' => $categories['eSOLVER'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "eSOLVER - Registrazione fatture di acquisto estere mensili",
                'description' => "Stampa PDF e registrazione fatture di acquisto estere mensili (compresa creazione nuova anagrafica fornitore se necessaria)",
                'average_time' => "240",
                'activity_category_id' => $categories['eSOLVER'],
                'fields' => "quantity,activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "eSOLVER - Registrazione fatture di acquisto in regime agevolato",
                'description' => "Stampa PDF e registrazione fatture di acquisto regime agevolato (compresa creazione nuova anagrafica fornitore se necessaria)",
                'average_time' => "5",
                'activity_category_id' => $categories['eSOLVER'],
                'fields' => "quantity,activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "eSOLVER - Registrazione fatture di acquisto in reverse charge mensili",
                'description' => "Stampa PDF e registrazione fatture di acquisto in reverse charge mensili (compresa creazione nuova anagrafica fornitore se necessaria)",
                'average_time' => "10",
                'activity_category_id' => $categories['eSOLVER'],
                'fields' => "quantity,activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "eSOLVER - Registrazione fatture di acquisto italiane mensili",
                'description' => "Stampa PDF e registrazione fatture di acquisto italiane mensili (compresa creazione nuova anagrafica fornitore se necessaria)",
                'average_time' => "1020",
                'activity_category_id' => $categories['eSOLVER'],
                'fields' => "quantity,activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "eSOLVER - Registrazione incasso",
                'description' => null,
                'average_time' => "1",
                'activity_category_id' => $categories['eSOLVER'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "eSOLVER - Registrazione paghe in prima nota",
                'description' => "Raccolta, stampa e registrazione paghe in prima nota - mensile",
                'average_time' => "600",
                'activity_category_id' => $categories['eSOLVER'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "eSOLVER - Riconciliazione scadenziario clienti",
                'description' => "Riconciliazione trimestrale scadenzario clienti eSOLVER/gestionale",
                'average_time' => "720",
                'activity_category_id' => $categories['eSOLVER'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "eSOLVER - Scadenziario fornitori",
                'description' => "Elaborazione scadenzario fornitori",
                'average_time' => "20",
                'activity_category_id' => $categories['eSOLVER'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "eSOLVER - Upload fatture di acquisto",
                'description' => null,
                'average_time' => "1",
                'activity_category_id' => $categories['eSOLVER'],
                'fields' => "quantity,activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "eSOLVER - Verifica bilancio annuale",
                'description' => null,
                'average_time' => "2400",
                'activity_category_id' => $categories['eSOLVER'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "eSOLVER - Verifica bilancio mensile",
                'description' => null,
                'average_time' => "120",
                'activity_category_id' => $categories['eSOLVER'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "eSOLVER - Verifica e invio autofatture",
                'description' => "Verifica e invio autofatture a integrazione delle fatture estere - mensile",
                'average_time' => "30",
                'activity_category_id' => $categories['eSOLVER'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "eSOLVER - Verifica mensile carte di credito",
                'description' => "Verifica mensile carte di credito e aggiornamento scritture",
                'average_time' => "60",
                'activity_category_id' => $categories['eSOLVER'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "eSOLVER - Verifica mensile movimenti altre banche e cassa",
                'description' => "Verifica mensile movimenti altre banche e cassa, e aggiornamento scritture",
                'average_time' => "600",
                'activity_category_id' => $categories['eSOLVER'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "eSOLVER - Verifica settimanale movimenti banca principale",
                'description' => "Verifica settimanale movimenti banca principale e aggiornamento scritture",
                'average_time' => "120",
                'activity_category_id' => $categories['eSOLVER'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Gestione cambiali",
                'description' => "Distinte da compilare, variazioni scadenze, presentazione in banca",
                'average_time' => "60",
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Gestione stati estinti",
                'description' => null,
                'average_time' => "10",
                'activity_category_id' => $categories['Gestionale'],
                'fields' => "activity_minutes,client_id",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Invio disdetta a fornitore",
                'description' => "Invio disdetta a fornitore e caricamento a gestionale",
                'average_time' => "20",
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes,client_id",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Invio PEC per gestione crediti",
                'description' => "Invio PEC per gestione crediti e upload a gestionale",
                'average_time' => "20",
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes,client_id",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Invio report mensile Telepass",
                'description' => null,
                'average_time' => "15",
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Nuovo fornitore",
                'description' => "Inserimento nuovo fornitore",
                'average_time' => "20",
                'activity_category_id' => $categories['Gestionale'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Nuovo segnalatore",
                'description' => "Inserimento nuovo segnalatore",
                'average_time' => "20",
                'activity_category_id' => $categories['Gestionale'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "PEG - Archiviazione fatture acquisto",
                'description' => null,
                'average_time' => "10",
                'activity_category_id' => $categories['PEG'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "PEG - Bonifico fornitore",
                'description' => null,
                'average_time' => "2",
                'activity_category_id' => $categories['PEG'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "PEG - Fattura affitto",
                'description' => null,
                'average_time' => "2",
                'activity_category_id' => $categories['PEG'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 0,
            ]
        );

        ActivityType::create(
            [
                'name' => "PEG - Raccolta documenti per commercialista",
                'description' => null,
                'average_time' => "60",
                'activity_category_id' => $categories['PEG'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Protocollo contratto",
                'description' => "Gestione task protocollo contratto",
                'average_time' => "1",
                'activity_category_id' => $categories['Gestionale'],
                'fields' => "activity_minutes,client_id,task_id",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Recupero dati per apertura ticket",
                'description' => null,
                'average_time' => "20",
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Registrazione incasso",
                'description' => "Registrazione singolo incasso",
                'average_time' => "1",
                'activity_category_id' => $categories['Gestionale'],
                'fields' => "activity_minutes,client_id",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Registrazione insoluto",
                'description' => null,
                'average_time' => "5",
                'activity_category_id' => $categories['Gestionale'],
                'fields' => "activity_minutes,client_id",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Report attività settimanale",
                'description' => null,
                'average_time' => "30",
                'activity_category_id' => $categories['KPI'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Report flussi di cassa",
                'description' => "Analisi dati e predisposizione report flussi di cassa",
                'average_time' => "60",
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Riassegnazione task",
                'description' => null,
                'average_time' => "3",
                'activity_category_id' => $categories['Gestionale'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Ricerca contributi",
                'description' => null,
                'average_time' => "60",
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Richiesta visura",
                'description' => "Richiesta visura e caricamento a gestionale",
                'average_time' => "10",
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes,client_id",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Riunione aggiornamento bilancio",
                'description' => null,
                'average_time' => "120",
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Riunione amministrazione",
                'description' => null,
                'average_time' => "60",
                'activity_category_id' => $categories['KPI'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Riunione gestione crediti",
                'description' => null,
                'average_time' => "120",
                'activity_category_id' => $categories['KPI'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Riunione Kredis + avvocato",
                'description' => null,
                'average_time' => "60",
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Riunione tecnici",
                'description' => null,
                'average_time' => "60",
                'activity_category_id' => $categories['KPI'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Riunione vendite",
                'description' => null,
                'average_time' => "60",
                'activity_category_id' => $categories['KPI'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Supporto ufficio acquisti",
                'description' => "Supporto all'ufficio acquisti per registrazioni, pagamenti, ecc.",
                'average_time' => "20",
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Supporto ufficio HR",
                'description' => "Recupero vecchi contratti o dati per ufficio HR",
                'average_time' => "15",
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Supporto ufficio IT",
                'description' => "Supporto ufficio IT per pagamenti o nuove iscrizioni (es. Google, OVH)",
                'average_time' => "10",
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Supporto ufficio marketing",
                'description' => "Supporto ufficio marketing per pagamenti o nuove iscrizioni (es. verifica contratto Spoki, account Apple)",
                'average_time' => "20",
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Task eSOLVER",
                'description' => "Gestione task esolver SDD e nuove scadenze",
                'average_time' => "5",
                'activity_category_id' => $categories['Gestionale'],
                'fields' => "activity_minutes,client_id,task_id",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Upload contabili bancarie",
                'description' => "Stampa PDF e upload contabili bancarie",
                'average_time' => "7",
                'activity_category_id' => $categories['Gestionale'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Upload quietanze F24 mensile",
                'description' => null,
                'average_time' => "15",
                'activity_category_id' => $categories['Gestionale'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Verifica assegni da versare",
                'description' => null,
                'average_time' => "10",
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Verifica insoluti Ri.Ba. / SDD",
                'description' => null,
                'average_time' => "15",
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Verifica mensile incassi",
                'description' => "Verifica mensile incassi banche, PayPal, Stripe",
                'average_time' => "180",
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Verifica mensile incassi registrati",
                'description' => null,
                'average_time' => "120",
                'activity_category_id' => $categories['Gestionale'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Verifica provvigioni",
                'description' => null,
                'average_time' => "180",
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Verifica saldo e stralcio",
                'description' => "Verifica saldo e stralcio per un singolo cliente",
                'average_time' => "15",
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes,client_id",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Verifica scadenzario clienti",
                'description' => null,
                'average_time' => "60",
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Versamento assegni o prelievi",
                'description' => null,
                'average_time' => "30",
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );



    }
}
