<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected function fullName(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => trim(implode(' ', [$this->name, $this->surname])),
        );
    }

    public function activityDates(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(ActivityDate::class);
    }

    public function activities(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Activity::class);
    }

    protected function unfinishedReports(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => $this->activityDates()->where('archived', false)->get(),
        );
    }

    /**
     * Fornisce la lista di ruoli che devono compilare un report quotidiano,
     * insieme al reparto e alla corrispondente colonna nella tabella activity_types
     * @return string[]
     */
    public static function reportRoles(): array
    {
        return [
            'addetto customer care' => [
                'field' => 'customer_care',
                'dept' => 'customer care',
            ],
            'tecnico' => [
                'field' => 'tecnico',
                'dept' => 'tecnici',
            ],
            'venditore' => [
                'field' => 'venditore',
                'dept' => 'venditori',
            ],
            'amministrativo' => [
                'field' => 'amministrativo',
                'dept' => 'amministrazione',
            ],
        ];
    }
}
