<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_fields', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('name', 20);
            $table->string('description', 50);
            $table->string('type', 20);
            $table->string('inputmode', 15)->nullable();
            $table->unsignedTinyInteger('size')->comment('Larghezza del campo in dodicesimi');
            $table->string('min', 10)->nullable();
            $table->string('max', 10)->nullable();
            $table->string('step', 5)->nullable();
            $table->string('extra', 50)->nullable();
            $table->string('option_values', 20)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_fields');
    }
};
