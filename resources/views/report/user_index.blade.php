<x-app-layout>
    <x-slot name="header">
        <div class="container mt-5 mb-5">
            <h2>
                Report compilati
                {{-- mostro il nome solo se l'utente loggato non corrisponde all'utente richiesto --}}
                @if($user->id <> auth()->user()->id)
                    da {{ $user->fullName }}
                @endif
            </h2>
        </div>
    </x-slot>
    <div class="container">
        @if(!$reports->count())
            <p>
                Nessun report presente.
            </p>
        @else
            <div class="table-responsive mb-4">
                <table class="table">
                    <thead>
                    <tr>
                        <th></th> {{-- Icona lucchetto --}}
                        <th scope="col">Data</th>
                        <th scope="col">Task rit.</th>
                        <th scope="col">Comunicaz. rit.</th>
                        <th scope="col">Ore timbrate</th>
                        @can('visualizzare totali')
                            <th scope="col">Minuti attività</th>
                        @endcan
                        @if($user->can('trasferta'))
                            <th scope="col">Km</th>
                            @can('visualizzare totali')
                                <th scope="col">Minuti viaggio</th>
                            @endcan
                        @endif
                        @can('visualizzare totali')
                            <th scope="col">Ore lavorate</th>
                        @endcan
                        @if($user->hasRole('venditore'))
                            <th scope="col">N° contratti</th>
                            <th scope="col">Valore contratti</th>
                        @endif
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($reports as $activity_date)
                        <tr>
                            {{-- Icona lucchetto e funzione di riapertura report chiuso --}}
                            <td>
                                <div id="activity_date-{{ $activity_date->id }}">
                                    @if($activity_date->archived)
                                        @can('visualizzare report')
                                            <a class="text-decoration-none link-success" id="lock-{{ $activity_date->id }}" data-activity_date_id="{{ $activity_date->id }}" href="#">
                                        @endcan
                                            <span style="color: gray;">
                                                <i class="fa-solid fa-xs fa-lock"></i>
                                            </span>
                                        @can('visualizzare report')
                                            </a>
                                        @endcan
                                    @else
                                        <span style="color: black;">
                                            <i class="fa-solid fa-xs fa-lock-open"></i>
                                        </span>
                                    @endif
                                </div>
                            </td>

                            @if($activity_date->absent)
                                {{-- Data --}}
                                <th scope="row">{{ $activity_date->date->format('j/n/Y') }}</th>

                                {{-- Se assente la riga finisce qui --}}
                                <td colspan="7">Assente</td>
                            @else
                                {{-- Data con link al singolo report --}}
                                <th scope="row">
                                    <a href="{{ route('report.show', $activity_date) }}">{{ $activity_date->date->format('j/n/Y') }}</a>
                                </th>

                                {{-- Task in ritardo --}}
                                <td>{{ $activity_date->tasks }}</td>

                                {{-- Comunicazioni in ritardo --}}
                                <td>{{ $activity_date->communications }}</td>

                                {{-- Ore timbrate --}}
                                <td>{{ $activity_date->hours }}</td>

                                @can('visualizzare totali')
                                    {{-- Minuti attività --}}
                                    <td>
                                        <strong>{{ $activity_date->activities->sum('activity_minutes') }}</strong>
                                        @if($activity_date->activities->sum('activity_minutes') > 59)
                                            <small>{{ floor($activity_date->activities->sum('activity_minutes') / 60)}}h&nbsp;{{ $activity_date->activities->sum('activity_minutes') % 60 }}'</small>
                                        @endif
                                    </td>
                                @endcan

                                @if($user->can('trasferta'))
                                    {{-- Km --}}
                                    <td>{{ $activity_date->activities->sum('km') }}</td>

                                    @can('visualizzare totali')
                                        {{-- Minuti di viaggio --}}
                                        <td>
                                            @if($activity_date->activities->sum('travel_minutes'))
                                                <strong>{{ $activity_date->activities->sum('travel_minutes') }}</strong>
                                                @if($activity_date->activities->sum('travel_minutes') > 59)
                                                    <small>{{ floor($activity_date->activities->sum('travel_minutes') / 60)}}h&nbsp;{{ $activity_date->activities->sum('travel_minutes') % 60 }}'</small>
                                                @endif
                                            @endif
                                        </td>
                                    @endcan
                                @endif

                                @can('visualizzare totali')
                                    {{-- Ore lavorate (inclusi i tempi di spostamento) --}}
                                    <td>{!! \App\Models\ActivityDate::formattedMinutes($activity_date->totalMinutes) !!}</td>
                                @endcan
                            @endif

                            @if($user->hasRole('venditore'))
                                {{-- Numero contratti --}}
                                <td>{{ $activity_date->sum_contratti ?: '' }}</td>

                                {{-- Valore contratti --}}
                                <td>{{ $activity_date->sum_contratti_value ?: '' }}</td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>

    <x-slot name="script">
        <script>
            $("[id^=lock-]").click(function () {
                if(confirm('Confermi la riattivazione di questo report? Potrà chiuderlo solo chi lo ha compilato.')) {
                    //alert($(this).data('activity_date_id'));
                    let activityDateId = $(this).data('activity_date_id');
                    let activityDateDiv = $('#activity_date-' + activityDateId);
                    //activityDateDiv.html('');

                    $.ajax({
                        url: "{{ route('activity_date.unlock') }}",
                        method: "POST",
                        async: false,
                        data: {
                            "_token": "{{ csrf_token() }}",
                            activity_date_id: activityDateId
                        }
                    })
                    .done(function (response) {
                        if(response) {
                            activityDateDiv.html('<span style="color: black;"><i class="fa-solid fa-xs fa-lock-open"></i></span>');
                        }
                    });
                }
            });
        </script>
    </x-slot>
</x-app-layout>
