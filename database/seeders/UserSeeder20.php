<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder20 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Disabilitazione accesso Cappellaro
        User::where('email', 's.cappellaro@facile626.it')->update([
            'password' => (app()->environment('local') ? Hash::make('123') : Hash::make('iuwerh!£$sfihbgi783iebv87trt')),
            'active' => 0,
        ]);
    }
}
