<?php

namespace Database\Seeders;

use App\Models\ActivityCategory;
use App\Models\ActivityType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ActivityTypeSeeder17 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = ActivityCategory::all()->pluck('id', 'name');

        // Nuova attività "Gestione nuovi mandati SDD"
        ActivityType::create(
            [
                'name' => "Gestione nuovi mandati SDD",
                'description' => null,
                'average_time' => 20,
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "quantity,activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        // Aggiunta quantità su "Task eSOLVER"
        ActivityType::where('name', "Task eSOLVER")->update(
            [
                'fields' => "quantity,activity_minutes",
            ]
        );

    }
}
