<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ActivityType extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'average_time',
        'activity_category_id',
        'fields',
        'paulitti_id',
        'tecnico',
        'customer_care',
        'order',
        'visible',
    ];

    // Attività fittizie per l'esportazione, non selezionabili dall'utente
    const ACTIVITY_ABSENT_ID         = 99; // Non lavorato
    const ACTIVITY_COMMUNICATIONS_ID = 0;  // Comunicazioni in ritardo
    const ACTIVITY_TASKS_ID          = 0;  // Task in ritardo
    const ACTIVITY_HOURS_ID          = 0;  // Ore timbrate

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope('default', function (Builder $builder) {
            $builder->orderBy('name', 'asc');
            // Disabilitato perché altrimenti quando si guarda un vecchio report con attività non più presenti (rese non visibili) ovviamente va in errore
            //$builder->where('visible', 1);
        });
    }


    public function category(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(ActivityCategory::class, 'activity_category_id');
    }

    public function activities(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Activity::class);
    }

    protected function categoryName(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => $this->category->name,
        );
    }

    /**
     * Crea il codice HTML per gli elementi del form relativi all'attività
     *
     * @return string HTML (Bootstrap 5)
     */
    public function renderHtml(): string
    {
        $html = '';
        $nl = "\n";
        $fields = explode(',', $this->fields);

        foreach ($fields as $field) {
            // I campi dell'attività sono di default obbligatori.
            // Se il nome del campo ha un asterisco, allora vuol dire che è opzionale.
            $required = ' required';
            if (str_contains($field, '*')) {
                $required = '';
                $field = str_replace('*', '', $field);
            }
            $activityField = ActivityField::where('name', $field)->first();
            $html .= '<div class="col-lg-' . $activityField->size . ' mb-3">' . $nl;
            switch ($activityField->type) {
                case 'select':
                    $html .= '<select'
                        . ' name="' . $field . '"'
                        . ' class="form-select"'
                        . ' aria-describedby="' . $field .  '_desc"'
                        . $required
                        . ($activityField->extra ? ' ' . $activityField->extra : '')
                        . '>' . $nl;
                    $options = $activityField->optionValues();
                    foreach ($options as $option) {
                        $html .= '<option value="' . $option['value'] . '">' . $option['text'] . '</option>' . $nl;
                    }
                    $html .= '</select>' . $nl;
                    break;

                default:
                    $html .= '<input class="form-control"'
                        . $required
                        . ' name="' . $field . '"'
                        . ' id="' . $field . '"'
                        . ' type="' . $activityField->type . '"'
                        . ($activityField->max ? ' max="' . $activityField->max . '"' : '')
                        . ($activityField->inputmode ? ' inputmode="' . $activityField->inputmode . '"' : '')
                        . ' aria-describedby="' . $field . '_desc"'
                        . ($activityField->extra ? ' ' . $activityField->extra : '')
                        . '>' . $nl;
            }
            $html .= '<div id="' . $field . '_desc" class="form-text">' . $activityField->description . /*($required ? '' : ' (opzionale)') .*/ '</div>' . $nl
                . '</div>' . $nl;
        }
        return $html;
    }

}
