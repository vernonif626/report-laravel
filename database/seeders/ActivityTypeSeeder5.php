<?php

namespace Database\Seeders;

use App\Models\ActivityType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ActivityTypeSeeder5 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Tecnici: inserimento tempi medi + attivazione "stampa documenti" a customer care
        ActivityType::where('name', 'Compilazione rimborsi')->update(['average_time' => 15]);
        ActivityType::where('name', 'Consegna documento')->update(['average_time' => 30]);
        ActivityType::where('name', 'Contatto / risposta extra al cliente')->update(['average_time' => 10]);
        ActivityType::where('name', 'Contatto covid')->update(['average_time' => 5]);
        ActivityType::where('name', 'Firma nomina (telefono)')->update(['average_time' => 5]);
        ActivityType::where('name', 'Primo sopralluogo')->update(['average_time' => 80]);
        ActivityType::where('name', 'Recensione')->update(['average_time' => 10]);
        ActivityType::where('name', 'Redazione documento')->update(['average_time' => 120]);
        ActivityType::where('name', 'Redazione modello DVR')->update(['average_time' => 180]);
        ActivityType::where('name', 'Ritardo cliente')->update(['average_time' => 15]);
        ActivityType::where('name', 'Sopralluogo annullato da cliente')->update(['average_time' => 5]);
        ActivityType::where('name', 'Sopralluogo annullato da tecnico')->update(['average_time' => 5]);
        ActivityType::where('name', 'Sopralluogo covid')->update(['average_time' => 20]);
        ActivityType::where('name', 'Sopralluogo extra')->update(['average_time' => 60]);
        ActivityType::where('name', 'Sopralluogo periodico')->update(['average_time' => 60]);
        ActivityType::where('name', 'Stampa documenti')->update([
            'average_time' => 6,
            'customer_care' => 1,
        ]);
        ActivityType::where('name', 'Telefonata periodica')->update(['average_time' => 5]);
        ActivityType::where('name', 'Upload documento')->update(['average_time' => 5]);

        // Customer care: inserimento tempi medi e aggiornamento di alcuni nomi/descrizioni
        ActivityType::where('name', 'Appuntamento fissato')->update(['average_time' => 15]);
        ActivityType::where('name', 'Attestato caricato')->update(['average_time' => 1]);
        ActivityType::where('name', 'Centralino')->update(['average_time' => 2]);
        ActivityType::where('name', 'Comunicazione cliente')->update(['average_time' => 3]);
        ActivityType::where('name', 'Comunicazione fornitore')->update(['average_time' => 5]);
        ActivityType::where('name', 'Comunicazione interna')->update(['average_time' => 3]);
        ActivityType::where('name', 'Comunicazione tecnico')->update(['average_time' => 3]);
        ActivityType::where('name', 'Corso attivato')->update([
            'average_time' => 7,
            'name' => "Creazione data corso",
            'description' => "Corso in presenza o FAD sincrona creato a gestionale",
        ]);
        ActivityType::where('name', 'Corso chiuso')->update([
            'average_time' => 10,
            'description' => "Ritiro materiale corso; registrazione dati e documenti a gestionale",
        ]);
        ActivityType::where('name', 'Dato lavoratore aggiornato')->update(['average_time' => 5]);
        ActivityType::where('name', 'Discente disiscritto corso')->update(['average_time' => 2]);
        ActivityType::where('name', 'Discente iscritto corso')->update(['average_time' => 2]);
        ActivityType::where('name', 'Materiale corso')->update([
            'average_time' => 10,
            'description' => "Preparazione e spedizione materiale corso",
        ]);
        ActivityType::where('name', 'Nomina / sostituzione tecnico')->update(['average_time' => 5]);
        ActivityType::where('name', 'Reception')->update(['average_time' => 1]);
        ActivityType::where('name', 'Ricerca fornitore')->update(['average_time' => 15]);
        ActivityType::where('name', 'Task pulito')->update(['average_time' => 2]);
        ActivityType::where('name', 'Telefonata corso')->update(['average_time' => 5]);
        ActivityType::where('name', 'Ticket aperto')->update(['average_time' => 5]);
        ActivityType::where('name', 'Verifica adesioni al corso')->update([
            'average_time' => 3,
            'name' => "Attivazione o annullamento corso",
        ]);
        ActivityType::where('name', 'Verifica fabbisogno formativo')->update(['average_time' => 15]);
    }
}
