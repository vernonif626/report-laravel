<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class UserAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Super',
            'surname' => 'Admin',
            'email' => 'procedure@facile626.it',
            'password' => Hash::make('F4c1l3626'),
        ]);

        $admin = User::where('surname', 'Admin')->first();
        $admin->assignRole('admin');
    }
}
