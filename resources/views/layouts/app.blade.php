<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        {{--@livewireStyles--}}

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
    </head>
    <body>
        <div class="container-fluid p-0">
            @include('layouts.navigation')
        </div>

        <!-- Page Heading -->
        <header>
            {{ $header }}
        </header>

        <!-- Page Content -->
        <main>
            {{ $slot }}
        </main>

        <div class="container-fluid p-0">
            <footer class="small text-center mt-5 p-3 border-top">
                <x-copyright />
            </footer>
        </div>

        {{--@livewireScripts--}}
        <script   src="https://code.jquery.com/jquery-3.6.0.min.js"   integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="   crossorigin="anonymous"></script>
        {{ $script ?? '' }}
    </body>
</html>
