<?php

namespace App\Http\Controllers;

use App\Models\ActivityDate;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index()
    {
        $activity_dates = Auth::user()->activityDates()->where('archived', 0)->orderBy('date')->get();
        $usersTecnici = auth()->user()->can('visualizzare report') ? User::role('tecnico')->where('active', 1)->orderBy('surname')->get() : null;
        $usersCustomerCare = auth()->user()->can('visualizzare report') ? User::role('addetto customer care')->where('active', 1)->orderBy('surname')->get() : null;
        $usersVenditori = auth()->user()->can('visualizzare report') ? User::role('venditore')->where('active', 1)->orderBy('surname')->get() : null;
        $usersAmministrativi = auth()->user()->can('visualizzare report') ? User::role('amministrativo')->where('active', 1)->orderBy('surname')->get() : null;

        // Prepara un array con i report dall'inizio della settimana scorsa fino a oggi e indicazione se presenti, mancanti o da concludere
        $days = [];
        if (auth()->user()->can('compilare report')) {
            for ($day = today()->subWeek()->startOfWeek(); $day <= today(); $day->addDay()) {
                if ($day->dayOfWeek != Carbon::SATURDAY && $day->dayOfWeek != Carbon::SUNDAY) {
                    $attributes = ['date' => $day->toDateString(), 'day' => $day->day. "/" . $day->month];
                    if (ActivityDate::where([ ['user_id', auth()->user()->id], ['date', $day->toDateString()], ['archived', 0] ])->count() > 0) {
                        // Report non concluso
                        $attributes['class'] = 'warning';
                    } elseif (ActivityDate::where([ ['user_id', auth()->user()->id], ['date', $day->toDateString()] ])->count() > 0) {
                        // Report concluso
                        $attributes['class'] = 'success';
                    } else {
                        // Report non presente
                        $attributes['class'] = 'danger';
                    }
                    $days[] = $attributes;
                }
            }
        }

        return view('dashboard', compact(['activity_dates', 'usersTecnici', 'usersCustomerCare', 'usersVenditori', 'usersAmministrativi', 'days']));
    }
}
