<?php

namespace Database\Seeders;

use App\Models\ActivityField;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ActivityFieldSeeder3 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $activityFields = [
            [
                'name' => "role",
                'description' => "Mansione",
                'type' => "select",
                'inputmode' => null,
                'size' => 2,
                'min' => null,
                'max' => null,
                'step' => null,
                'extra' => null,
                'option_values' => "role",
            ],
            [
                'name' => "document_type",
                'description' => "Tipo documento",
                'type' => "select",
                'inputmode' => null,
                'size' => 2,
                'min' => null,
                'max' => null,
                'step' => null,
                'extra' => null,
                'option_values' => "document_type",
            ],
            /*
             [
                 'name' => "",
                 'description' => "",
                 'type' => "",
                 'inputmode' => "",
                 'size' => 2,
                 'min' => null,
                 'max' => null,
                 'step' => null,
                 'extra' => null,
                 'option_values' => null,
             ],
             */
        ];

        foreach ($activityFields as $activityField) {
            ActivityField::create($activityField);
        }
    }
}
