<x-app-layout>
    <x-slot name="header">
        <div class="container mt-5 mb-5">
            <h2>Report del {{ $activityDate->localizedLongDate }}</h2>
            @can('visualizzare totali')
                @if($activityDate->totalMinutes > 0)
                    <p>
                        Totale lavorato: <strong>{!! \App\Models\ActivityDate::formattedMinutes($activityDate->totalMinutes, true) !!}</strong>
                    </p>
                @endif
            @endcan
        </div>
    </x-slot>
    <div class="container">

        {{-- Form inserimento nuova attività --}}
        <div class="card mb-5{{-- bg-light--}}">
            <div class="card-body">
                <form method="post" id="activityForm" action="{{ route('activity.store') }}">
                    @csrf
                    <input type="hidden" name="date" value="{{ $activityDate->date->format('Y-m-d') }}">
                    <input type="hidden" name="activity_date_id" value="{{ $activityDate->id }}">
                    <input type="hidden" name="task_matched" id="task_matched" value="1">
                    <input type="hidden" name="f626_date" id="f626_date" value="">
                    <input type="hidden" name="note" id="note" value="">

                    <div class="row">
                        {{-- Tipo attività --}}
                        <div class="col-lg-4 mb-3">
                            <select
                                class="form-select"
                                name="activity_type_id"
                                id="activity"
                                value="{{ old('activity_type_id') }}"
                                aria-describedby="activityDesc"
                                required
                            >
                                <option value="0">Scegli un'attività</option>
                                @foreach($activityList as $activity)
                                    <option value="{{ $activity->id }}" data-fields="{{ $activity->fields }}" data-description="{{ $activity->description }}">{{ $activity->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        {{-- Descrizione attività, riempito a seconda dell'attività scelta --}}
                        <div class="col mb-3">
                            <h4 id="activityDesc" class="col-form-label"></h4>
                        </div>
                    </div>

                    {{-- div riempito con gli input relativi all'attività scelta --}}
                    <div class="row" id="activityRow">
                    </div>

                    {{-- Submit --}}
                    <div class="row">
                        <div class="col-lg">
                            <button type="submit" id="activityAdd" class="btn btn-outline-primary" disabled>Aggiungi</button>
                        </div>

                    </div>
                </form>

            </div>
        </div>

        @include('report.activities_summary_table', compact(['activities']))

        {{-- Form chiusura report --}}
        <div class="card mt-5 bg-light">
            <div class="card-body">
                <h5 class="card-title">
                    Chiusura report
                </h5>
                <h6 class="card-subtitle text-muted mb-3">
                    Quando hai inserito tutte le attività della giornata è necessario chiudere il report indicando le seguenti informazioni:
                </h6>
                <form method="post" action="{{ route('report.update', $activityDate) }}" id="reportForm">
                    @csrf
                    @method('patch')
                    <input type="hidden" name="tasks_matched" id="tasks_matched" value="1">
                    <div class="row">

                        <div class="col-lg mb-3">
                            <input
                                name="tasks"
                                type="number"
                                value="{{ $activityDate->tasks }}"
                                step="1"
                                min="0"
                                class="form-control"
                                aria-describedby="tasksDesc"
                                required
                            >
                            <div id="tasksDesc" class="form-text">Task in ritardo</div>
                        </div>
                        <div class="col-lg mb-3">
                            <input
                                name="communications"
                                type="number"
                                value="{{ $activityDate->communications }}"
                                step="1"
                                min="0"
                                class="form-control"
                                aria-describedby="communicationsDesc"
                                required
                            >
                            <div id="communicationsDesc" class="form-text">Comunicazioni in ritardo</div>
                        </div>
                        <div class="col-lg mb-3">
                            <input
                                name="hours"
                                type="number"
                                value="{{ $activityDate->hours }}"
                                step="0.5"
                                min="0"
                                max="8"
                                class="form-control"
                                aria-describedby="hoursDesc"
                                required
                            >
                            <div id="hoursDesc" class="form-text">Ore timbrate</div>
                        </div>
                        <div class="col-lg">
                            <button type="submit"class="btn btn-outline-primary">Concludi report</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <x-slot name="script">
        <script>
            {{-- Mostra i campi dell'attività scelta --}}
            $('#activity').change(function () {
                if($('#activity').val() > 0) {
                    $('#activityAdd').prop("disabled", false);
                } else {
                    $('#activityAdd').prop("disabled", true);
                }
                let activityRow = $('#activityRow');
                activityRow.html('');
                $('#activityDesc').text('');
                let activitySelected = $(this).children("option:selected");
                let fields = activitySelected.data('fields');
                if(fields.length) {
                    $('#activityDesc').text(activitySelected.data('description'));
                    $.ajax({
                        url: "{{ route('activity_type.render') }}",
                        method: "POST",
                        async: false,
                        data: {
                            "_token": "{{ csrf_token() }}",
                            activity_type: $('#activity').val()
                        }
                    })
                    .done(function( response ) {
                        activityRow.append(response);
                    });
                }
            });

            {{-- Cancellazione di una riga (attività) del report --}}
            $('.js-delete').click(function (event) {
                if (!confirm('Confermi la cancellazione di questa attività? Non è possibile annullare questa operazione.')) {
                    event.preventDefault()
                }
            });

            {{-- Verifica se il task_id inserito corrisponde a quanto esitato a gestionale --}}
            {{-- DISABILITATO, RIUNIONE 2022-07-26 PERNA/CAPPELLARO --}}
            {{--
            $('#activityForm').submit(function( event ) {
                if ( $( "#task_id" ).length ) {
                    $.ajax({
                        url: "{{ route('activity.verify') }}",
                        method: "POST",
                        async: false,
                        data: {
                            "_token": "{{ csrf_token() }}",
                            task_id: $('#task_id').val(),
                            user_id: {{ auth()->user()->f626_user_id }}
                        }
                        // , success: function(result) {
                        // }
                    })
                    .done(function( response ) {
                        if (response["match"]) {
                            $('#task_matched').val("1");
                            $('#f626_date').val(response['date_completed']);
                            $('#note').val(response['task_description']);
                        } else {
                            let msg = "L'id del task non corrisponde a quanto esitato a gestionale. " +
                                "Scegli annulla per correggere il dato oppure OK per forzare l'invio dell'attività.";
                            if(confirm(msg)) {
                                $('#task_matched').val("0");
                            } else {
                                event.preventDefault();
                            }
                        }
                    });
                }
            });
            --}}

            {{-- Verifica se nel report manca qualche task rispetto a quelli evasi a gestionale --}}
            {{-- DISABILITATO, RIUNIONE 2022-07-26 PERNA/CAPPELLARO --}}
            {{--
            $('#reportForm').submit(function( event ) {
                $.ajax({
                    url: "{{ route('report.verify') }}",
                    method: "POST",
                    async: false,
                    data: {
                        "_token": "{{ csrf_token() }}",
                        activity_id: {{ $activityDate->id }}
                    }
                })
                .done(function( response ) {
                    if (response.length) {
                        let msg = "A gestionale risultano alcuni task non presenti nel report. " +
                            "Scegli OK per inviare comunque il report oppure annulla per aggiungerli.\n" +
                            "Puoi verificare i task a gestionale nella pagina Report attività.";
                        for (i = 0; i < response.length; i++) {
                            msg = msg + "\n#" + response[i]["task_id"] + " " + response[i]["description"];
                        }
                        if(confirm(msg)) {
                            $('#tasks_matched').val("0");
                        } else {
                            event.preventDefault();
                        }
                    }
                });
            });
            --}}
</script>
</x-slot>
</x-app-layout>
