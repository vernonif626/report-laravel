<?php

namespace Database\Seeders;

use App\Models\ActivityCategory;
use App\Models\ActivityType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ActivityTypeSeeder15 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Questo Seeder formalizza le modifiche fatte direttamente nel database di produzione il 16/2/2023

        $categories = ActivityCategory::all()->pluck('id', 'name');

        ActivityType::create(
            [
                'name' => "Gestione mail/PEC",
                'description' => "Lettura, invio, inoltro di e-mail e PEC",
                'average_time' => 30,
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Corso di formazione/aggiornamento",
                'description' => null,
                'average_time' => 30,
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Corrispondenza/telefonate con banche/fornitori/clienti",
                'description' => null,
                'average_time' => 30,
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Richieste report/dati dalla Direzione",
                'description' => null,
                'average_time' => 30,
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Adempimenti fiscali",
                'description' => null,
                'average_time' => 30,
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Controllo fatture di consolidato",
                'description' => null,
                'average_time' => 30,
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Invio incassi",
                'description' => null,
                'average_time' => 30,
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Supporto ufficio Corsi",
                'description' => null,
                'average_time' => 30,
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Supporto rete vendita",
                'description' => null,
                'average_time' => 30,
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Controllo import fatture da gestionale a eSOLVER",
                'description' => null,
                'average_time' => 30,
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Consulenza interna",
                'description' => "Comunicazione e-mail o telefonica per corsi, agenda, ecc.",
                'average_time' => "3",
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes,target_user_id",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Invio incassi F626",
                'description' => null,
                'average_time' => "30",
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        ActivityType::create(
            [
                'name' => "Invio incassi eSOLVER",
                'description' => null,
                'average_time' => "30",
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

    }
}
