<?php

namespace Database\Seeders;

use App\Models\ActivityCategory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ActivityCategorySeeder3 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ActivityCategory::insert(
            [
                ['name' => "Amministrazione"],
                ['name' => "eSOLVER"],
                // ['name' => "Gestionale"], // Già presente
                ['name' => "KPI"],
                ['name' => "PEG"],
            ]
        );
    }
}
