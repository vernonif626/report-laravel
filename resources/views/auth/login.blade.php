<x-guest-layout>
    <div class="container">
            <div class="row align-items-center justify-content-center" style="min-height: 100vh">
                <div class="col-lg-4">
                    <div class="border rounded-3 bg-light p-3">
                        <div class="text-center">
                            <img src="{{ asset('img/logo.svg') }}" width="100" height="28">
                        </div>
                        <form method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="mb-3">
                                <label for="email" class="form-label">Indirizzo e-mail</label>
                                <input name="email" id="email" type="email" class="form-control">
                            </div>
                            <div class="mb-3">
                                <label for="password" class="form-label">Password</label>
                                <input name="password" id="password" type="password" class="form-control">
                            </div>
                            <div class="mb-3 form-check">
                                <input name="remember" id="remember" type="checkbox" class="form-check-input">
                                <label class="form-check-label" for="remember">Ricordami</label>
                            </div>
                            <button type="submit" class="btn btn-primary">Login</button>
                        </form>
                    </div>
                    <x-copyright class="small text-center mt-3 text-black-50" />
                </div>
            </div>
    </div>
</x-guest-layout>

{{--<div class="container">--}}
{{--    <div--}}
{{--        class="row align-items-center bg-success text-light"--}}
{{--        style="min-height: 100vh">--}}
{{--        <div class="col-md-12">--}}
{{--            <h1>GeeksforGeeks</h1>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}

{{--                @if (Route::has('password.request'))--}}
{{--                    <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('password.request') }}">--}}
{{--                        {{ __('Forgot your password?') }}--}}
{{--                    </a>--}}
{{--                @endif--}}
