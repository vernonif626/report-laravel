<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class PermissionSeeder7 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Aggiungo i permessi di visualizzare/esportare i report dei venditori al BPM (Paulitti)
        $roleBpm = Role::where('name', 'business process manager')->first();
        $roleBpm->givePermissionTo('visualizzare report venditori');
        $roleBpm->givePermissionTo('esportare report venditori');
    }
}
