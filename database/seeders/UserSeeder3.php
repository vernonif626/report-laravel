<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder3 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $u = User::create([
            'name'         => 'Isabella',
            'surname'      => 'Lui',
            'email'        => 'i.lui@facile626.it',
            'password'     => (app()->environment('local') ? Hash::make('123') : Hash::make('jImQ')),
            'f626_user_id' => 71494,
        ]);
        $u->save();

        $u->assignRole('responsabile tecnici');
        $u->assignRole('responsabile customer care');
        $u->assignRole('responsabile');
    }
}
