<?php

namespace Database\Seeders;

use App\Models\ActivityType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ActivityTypeSeeder12 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ActivityType::where('name', "Manutenzione veicolo")->update([
            'fields' => "activity_minutes,travel_from*,travel_to*,km*,travel_minutes*",
        ]);
    }
}
