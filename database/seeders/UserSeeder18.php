<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;

class UserSeeder18 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Romina Zanutta
        $zanutta = User::create([
            'name'         => 'Romina',
            'surname'      => 'Zanutta',
            'email'        => 'r.zanutta@facile626.it',
            'password'     => (app()->environment('local') ? Hash::make('123') : Hash::make('e46i9J')),
            'f626_user_id' => 44972,
        ]);
        $zanutta->assignRole(['amministrativo', 'operativo']);

        // Annalisa Codispoti
        $codispoti = User::create([
            'name'         => 'Annalisa',
            'surname'      => 'Codispoti',
            'email'        => 'a.codispoti@facile626.it',
            'password'     => (app()->environment('local') ? Hash::make('123') : Hash::make('UMBG')),
            'f626_user_id' => 77664,
        ]);
        $codispoti->assignRole(['amministrativo', 'operativo']);

        // Aggiunge il ruolo operativo a Lucia per poter compilare i report
        $iervasutti = User::where('surname', 'Iervasutti')->first();
        $iervasutti->assignRole(['responsabile amministrazione', 'amministrativo', 'operativo']);

    }
}
