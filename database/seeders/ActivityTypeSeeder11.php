<?php

namespace Database\Seeders;

use App\Models\ActivityCategory;
use App\Models\ActivityType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ActivityTypeSeeder11 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Nascondo l'attività "Redazione modello DVR"
        ActivityType::where('name', "Redazione modello DVR")->update([
            'visible' => 0,
        ]);

        // Creo la nuova attività "Redazione modello documento"
        $categories = ActivityCategory::all()->pluck('id', 'name');
        ActivityType::create([
            'name' => "Redazione modello documento",
            'description' => "Predisposizione modello/integrazione DVR su Suite Sicurezza o modello/integrazione manuale HACCP in Word",
            'average_time' => 180,
            'fields' => "document_type,quantity,activity_minutes",
            'activity_category_id' => $categories['Redazione doc'],
            'paulitti_id' => 0, // non utilizzato
            'tecnico' => 1,
            'customer_care' => 0,
            'venditore' => 0,
            'order' => 0, // non utilizzato
            'visible' => 1,
        ]);
    }
}
