<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Http;

class Distance extends Model
{
    use HasFactory;
    const GOOGLE_API_KEY = 'AIzaSyBmfUHiBBMaa2dHG55wyyN6twJz8bfXZ28';
    const GOOGLE_API_URL = 'https://maps.googleapis.com/maps/api/distancematrix/json?';

    // Riferimento: https://developers.google.com/maps/documentation/distance-matrix/distance-matrix
    protected $fillable = [
        'origins',
        'destinations',
        'arrival_time',
        'avoid',
        'departure_time',
        'language',
        'mode',
        'region',
        'transit_mode',
        'transit_routing_preference',
        'units',
        'response',
        'counter',
    ];

    /**
     * Ritorna la distanza e il tempo di viaggio utilizzando le API Distance Matrix di Google.<br>
     * Salva in db la risposta per successive richieste identiche.<br>
     * Attualmente supporta località di partenza e arrivo singoli.
     *
     * @param array $parameters
     * @param bool $test Se valorizzato, forza la chiamata a Google e non salva il risultato in db.
     * @return array
     */
    public static function getDistanceAndTime(array $parameters, bool $test=false): array
    {
        $origins = isset($parameters['origins']) ? mb_strtolower($parameters['origins'], 'UTF-8') : null;
        $destinations = isset($parameters['destinations']) ? mb_strtolower($parameters['destinations'], 'UTF-8') : null;

        // Questi parametri per ora non vengono utilizzati nei report, implementare in futuro se necessario
        /*
        $arrival_time = $parameters['arrival_time'] ?? null;
        $avoid = $parameters['avoid'] ?? null;
        $language = $parameters['language'] ?? null;
        $mode = $parameters['mode'] ?? null;
        $region = $parameters['region'] ?? null;
        $transit_mode = $parameters['transit_mode'] ?? null;
        $transit_routing_preference = $parameters['transit_routing_preference'] ?? null;
        $units = $parameters['units'] ?? null;
        */

        if (!$test && self::where([['origins', $origins], ['destinations', $destinations]])->count()) {
            // Il dato è già presente in db
            $distance = self::where([['origins', $origins], ['destinations', $destinations]])->first();

            // Aumento il contatore per tenere traccia di quante volte è stata usata la chiamata (ed eventualmente calcolare il risparmio)
            $distance->counter += 1;
            $distance->save();

            $json = $distance->response;
            $data = json_decode($json, false);
            $distance_metres = $data->rows[0]->elements[0]->distance->value;
            $travel_seconds = $data->rows[0]->elements[0]->duration->value;
        } else {
            // Chiedo il dato a Google
            $response = Http::get(
                self::GOOGLE_API_URL,
                [
                    'origins' => $origins,
                    'destinations' => $destinations,
                    'language' => 'it',
                    'units' => 'km',
                    'key' => self::GOOGLE_API_KEY,
                ]
            );
            if ($response->successful()) {
                $data = json_decode($response->body(), false);
                $distance_metres = $data->rows[0]->elements[0]->distance->value;
                $travel_seconds = $data->rows[0]->elements[0]->duration->value;
                if (!$test) {
                    $distance = Distance::create(['origins' => $origins, 'destinations' => $destinations, 'response' => $response->body()]);
                }
            } else {
                $distance_metres = 0;
                $travel_seconds = 0;
            }
        }
        return [
            'distance_m' => $distance_metres,
            'distance_km' => round($distance_metres/1000, 1),
            'travel_sec' => $travel_seconds,
            'travel_min' => (int) round($travel_seconds/60, 0),
        ];
    }


    /**
     * Aggiunge i dati di Google all'attività
     *
     * @param Activity $activity
     */
    public static function addDistanceData(Activity $activity): void
    {
        // Se sono compilati km, località di partenza e località di arrivo, eseguo la verifica con Google
        if ($activity->km && $activity->travel_from && $activity->travel_to) {
            $distance = self::getDistanceAndTime(['origins' => $activity->travel_from, 'destinations' => $activity->travel_to]);
            $activity->km_google = $distance['distance_km'];
            $activity->travel_minutes_google = $distance['travel_min'];
            $activity->save();
        }
    }


    /**
     * Aggiunge alla tabella activities i dati mancanti di km e minuti di viaggio ottenuti da Google Distance Matrix API per l'intervallo di tempo indicato.<br>
     * Funzione temporanea da utilizzare una volta sola da linea di comando (tinker): in seguito i dati di Google verranno inseriti automaticamente.
     *
     * @param string $dateStart Data iniziale in formato Y-m-d (aaaa-mm-gg)
     * @param string $dateEnd Data finale in formato Y-m-d (aaaa-mm-gg)
     * @return void
     */
    public static function addMissingDistanceData(string $dateStart, string $dateEnd): void
    {
        $activityDates = ActivityDate::with('activities')->where([['date', '>=', $dateStart], ['date', '<=', $dateEnd]])->get();
        foreach ($activityDates as $activityDate) {
            $activities = $activityDate->activities()->where('km', '>' , 0)->whereNull('km_google')->get();
            foreach ($activities as $activity) {
                $distance = self::getDistanceAndTime(['origins' => $activity['travel_from'], 'destinations' => $activity['travel_to']]);
                $activity['km_google'] = $distance['distance_km'];
                $activity['travel_minutes_google'] = $distance['travel_min'];
                $activity->save();
            }
        }
    }

}
