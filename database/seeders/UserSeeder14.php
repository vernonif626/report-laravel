<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder14 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Riccardo Bonanni
        $bonanni = User::create([
            'name'         => 'Riccardo',
            'surname'      => 'Bonanni',
            'email'        => 'r.bonanni@facile626.it',
            'password'     => (app()->environment('local') ? Hash::make('123') : Hash::make('s72F#')),
            'f626_user_id' => 75314,
        ]);
        $bonanni->assignRole(['tecnico', 'operativo']);
    }
}
