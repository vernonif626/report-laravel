<?php

namespace App\Exports;

use App\Models\Activity;
use App\Models\ActivityDate;
use App\Models\ActivityType;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

/**
 * Esportazione report customer care.<br>
 * Utilizza <a href="https://docs.laravel-excel.com/3.1/getting-started/">Laravel Excel</a>,
 * basato su <a href="https://phpspreadsheet.readthedocs.io/">PhpSpreadsheet</a>.
 */
class ReportCustomerCareExport implements FromCollection, WithStrictNullComparison, WithHeadings
{
    protected string $date_start;
    protected string $date_end;
    protected int $user_id;

    public function __construct($date_start, $date_end=null, $user_id=false)
    {
        $this->date_start = $date_start;
        $this->date_end = $date_end ?: $date_start;
        $this->user_id = $user_id;
    }

    public function headings(): array
    {
        return [
            'Data',
            'Cognome',
            'Attività',
            'IDAttività',
            'Quant',
            'MinEffet',
            'MinMedia',
            'MinMediaTot',
            'IDCliente',
            'Persona',
            'DbId',
        ];
    }


    /**
    * @return Collection
    */
    public function collection(): Collection
    {
        $activityDates = ActivityDate::with('activities')->with('user')
            ->where('date', '>=', $this->date_start)
            ->where('date', '<=', $this->date_end)
            ->where('archived', '=', 1)
            // Dove il ruolo dell'utente è 'addetto customer care'
            ->whereHas('user', function (Builder $query) {
                $users = User::role('addetto customer care')->get()->pluck('id');
                $query->whereIn('id', $users);
            });
        if ($this->user_id) {
            $activityDates = $activityDates->where('user_id', $this->user_id);
        }
        $activityDates = $activityDates->orderBy('date')->get();

        // Creo un array che corrisponde al file Excel di Paulitti
        $activitiesExport = [];

        foreach ($activityDates as $activityDate) {

            // Se l'addetto ha inserito un'assenza non c'è alcuna attività in questa giornata
            if ($activityDate->absent) {
                $activitiesExport[] = [
                    'Data' => $activityDate->date->format('Y-m-d'),
                    'Cognome' => $activityDate->user->surname,
                    'Attività' => "Non lavorato",
                    'IDAttività' => ActivityType::ACTIVITY_ABSENT_ID,
                    'Quant' => 0,
                    'MinEffet' => 0,
                    'MinMedia' => 0,
                    'MinMediaTot' => 0,
                    'IDCliente' => 0,
                    'Persona' => 0,
                    'DbId' => $activityDate->id,
                ];
            } else {
                // Le prime righe riportano i dati giornalieri del report

                // Comunicazioni in ritardo
                $activitiesExport[] = [
                    'Data' => $activityDate->date->format('Y-m-d'),
                    'Cognome' => $activityDate->user->surname,
                    'Attività' => "Comunicazioni in ritardo",
                    'IDAttività' => ActivityType::ACTIVITY_COMMUNICATIONS_ID,
                    'Quant' => $activityDate->communications ?: 0, // in caso di assenza il valore a db è null
                    'MinEffet' => 0,
                    'MinMedia' => 0,
                    'MinMediaTot' => 0,
                    'IDCliente' => 0,
                    'Persona' => 0,
                    'DbId' => $activityDate->id,
                ];

                // Task in ritardo
                $activitiesExport[] = [
                    'Data' => $activityDate->date->format('Y-m-d'),
                    'Cognome' => $activityDate->user->surname,
                    'Attività' => "Task in ritardo",
                    'IDAttività' => ActivityType::ACTIVITY_TASKS_ID,
                    'Quant' => $activityDate->tasks ?: 0, // in caso di assenza il valore a db è null
                    'MinEffet' => 0,
                    'MinMedia' => 0,
                    'MinMediaTot' => 0,
                    'IDCliente' => 0,
                    'Persona' => 0,
                    'DbId' => $activityDate->id,
                ];

                // Ore timbrate
                $activitiesExport[] = [
                    'Data' => $activityDate->date->format('Y-m-d'),
                    'Cognome' => $activityDate->user->surname,
                    'Attività' => "Ore timbrate",
                    'IDAttività' => ActivityType::ACTIVITY_HOURS_ID,
                    'Quant' => $activityDate->hours ?: 0, // in caso di assenza il valore a db è null
                    'MinEffet' => $activityDate->hours ? $activityDate->hours * 60 : 0, // Richiesta Michele 2022-06-13
                    'MinMedia' => 0,
                    'MinMediaTot' => 0,
                    'IDCliente' => 0,
                    'Persona' => 0,
                    'DbId' => $activityDate->id,
                ];

                // Attività
                foreach ($activityDate->activities as $activity) {
                    $averageTime = $activity->average_time ?: 0;
                    $quantity = $activity->quantity ?: 1;
                    $activitiesExport[] = [
                        'Data' => $activity->date,
                        'Cognome' => $activity->user->surname,
                        'Attività' => $activity->activityType->name,
                        'IDAttività' => $activity->activityType->id,
                        'Quant' => $quantity,
                        'MinEffet' => $activity->activity_minutes ?: 0,
                        'MinMedia' => $averageTime,
                        'MinMediaTot' => $averageTime * $quantity,
                        'IDCliente' => $activity->client_id ?: 0,
                        'Persona' => $activity->target_user_name ?: 0,
                        'DbId' => $activity->id,
                    ];
                }
            }
        }
        return collect($activitiesExport);
    }
}
