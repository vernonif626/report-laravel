<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder3 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!Permission::where('name', 'visualizzare totali')->count()) {
            Permission::create(['name' => 'visualizzare totali']);
        }

        foreach (User::role(['responsabile'])->get() as $user) {
            $user->givePermissionTo('visualizzare totali');
        }
    }
}
