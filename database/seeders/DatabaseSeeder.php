<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            PermissionSeeder::class,
            RoleSeeder::class,
            //UserAdminSeeder::class,
            //UserDemoSeeder ::class,
            ActivityCategorySeeder::class,
            ActivityTypeSeeder::class,
            UserSeeder::class,
            UserSeeder2::class,
            ActivityTypeSeeder2::class,
            PermissionSeeder2::class,
            ActivityCategorySeeder2::class,
            ActivityTypeSeeder3::class,
            ActivityTypeSeeder4::class,
            UserSeeder3::class,
            PermissionSeeder3::class,
            UserSeeder4::class,
            PermissionSeeder4::class,
            PermissionSeeder5::class,
            ActivityTypeSeeder5::class,
            ActivityFieldSeeder::class,
            PermissionSeeder6::class,
            UserSeeder5::class,
            RoleSeeder2::class,
            UserSeeder6::class,
            ActivityTypeSeeder6::class,
            ActivityFieldSeeder2::class,
            PermissionSeeder7::class,
            UserSeeder7::class,              // Aggiunge Simone Cappellaro
            UserSeeder8::class,              // Aggiunge Martina Gianosi
            UserSeeder9::class,              // Cambia la password di Maurizio Morabito per disabilitare l'accesso
            ActivityFieldSeeder3::class,     // Aggiunge le definizioni dei campi role e document_type
            ActivityTypeSeeder7::class,      // Aggiunge i campi role (Mansione) e document_type (Tipo documento) nelle attività
            UserSeeder10::class,             // Aggiunge Debora Panzeri; Cambia la password di Della Schiava e Micoli per disabilitare l'accesso
            ActivityTypeSeeder8::class,      // Aggiornamenti vari come da ultime riunioni con i tecnici
            UserSeeder11::class,             // Disabilita Felcaro e rende non attivi i dipendenti non più in forza
            PermissionSeeder8::class,        // Aggiunge il permesso 'esportare report marketing'
            UserSeeder12::class,             // Aggiunge Fabio Zenarolla
            UserSeeder13::class,             // Aggiunge Giovannella Pechini (venditrice)
            UserSeeder14::class,             // Aggiunge Riccardo Bonanni (tecnico HACCP)
            ActivityTypeSeeder9::class,      // Aggiunge i dati di viaggio all'attività di affiancamento ricevuto (es. Bonanni ha raggiunto Celaj a Spilimbergo)
            UserSeeder15::class,             // Aggiunge Davide Colombin (tecnico RSPP) e disabilita Fabiola Celaj
            ActivityTypeSeeder10::class,     // Aggiunge i dati di viaggio all'attività di affiancamento fatto (es. Driutti ha raggiunto Bonanni)
            ActivityTypeSeeder11::class,     // Nasconde l'attività "Redazione modello DVR" e aggiunge "Redazione modello documento"
            ActivityTypeSeeder12::class,     // Aggiunge i dati di viaggio all'attività "Manutenzione veicolo"
            UserSeeder16::class,             // Disabilita i venditori Felcaro, Gallarati e Zampini
            ActivityTypeSeeder13::class,     // Aggiunge l'attività "Downgrade"
            UserSeeder17::class,             // Aggiunge Vanessa Cristante

            // Nuovo reparto amministrazione
            ActivityCategorySeeder3::class,  // Aggiunge le categorie per le attività dell'amministrazione
            ActivityTypeSeeder14::class,     // Aggiunge le attività dell'amministrazione
            PermissionSeeder9::class,        // Aggiunge il permesso "esportare report amministrazione"
            RoleSeeder3::class,              // Aggiunge il ruolo "amministrativo"
            UserSeeder18::class,             // Aggiunge Romina Zanutta e Annalisa Codispoti

            UserSeeder19::class,             // Aggiunge Francesco Fanetti (vendite)

            // Aggiornamento attività amministrazione
            ActivityTypeSeeder15::class,     // Formalizza le modifiche manuali al db fatte in produzione 16/2/2023
            ActivityTypeSeeder16::class,     // Modifiche richieste da Lucia Iervasutti
            ActivityTypeSeeder17::class,     // Modifiche richieste da Lucia Iervasutti

            ActivityTypeSeeder18::class,     // Attività customer care "Chiamata gradimento" e "Appuntamento modificato o annullato"
            UserSeeder20::class,             // Disabilitazione accesso Cappellaro
            ActivityTypeSeeder19::class,     // Modifiche richieste da Lucia Iervasutti

        ]);
    }
}
