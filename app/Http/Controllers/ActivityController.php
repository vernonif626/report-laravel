<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\Models\Distance;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ActivityController extends Controller
{
    const API_VERIFY_ACTIVITY = 'report_verify_activity.php';
    const API_SEND_ACTIVITY = 'report_receive_activity.php';

    /**
     * Aggiunge l'attività a db
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        //dump($request->all());
        $validated = $request->validate([
            'activity_date_id' => 'required|integer',
            'date' => 'required|date',
            'activity_type_id' => 'required|integer',
            'role' => 'nullable|string',
            'document_type' => 'nullable|string',
            'quantity' => 'nullable|integer',
            'activity_minutes' => 'nullable|integer',
            'travel_from' => 'nullable|string',
            'travel_to' => 'nullable|string',
            'km' => 'nullable|integer',
            'travel_minutes' => 'nullable|integer',
            'client_id' => 'nullable|integer',
            'address_id' => 'nullable|integer',
            'target_user_id' => 'nullable|integer',
            'task_id' => 'nullable|integer',
            'course_id' => 'nullable|integer',
            'task_matched' => 'required|boolean',
            'f626_date' => 'nullable|date',
            'note' => 'nullable|string',
            'rdc_pa' => 'nullable|string',
            'value' => 'nullable|string',
            'outcome_in_person' => 'nullable|string',
            'outcome_call' => 'nullable|string',
            'outcome_pa' => 'nullable|string',
        ]);
        $validated['user_id'] = auth()->user()->id;
        if (isset($validated['target_user_id'])) {
            $validated['target_user_name'] = $validated['target_user_id'] ? User::find($validated['target_user_id'])->fullName : "Altro";
        }
        //dump($validated);
        $activity = Activity::create($validated);
        //dd($activity);

        // Aggiungo i dati di Google
        Distance::addDistanceData($activity);

        // Aggiungo il tempo medio alla riga come dato storico
        $activity->average_time = $activity->activityType->average_time ?: null;

        // Se l'attività include task_id e km allora bisogna inviare tutti i dati al gestionale Facile626
        if ((isset($validated['task_id']) && $validated['task_id']) && (isset($validated['km']) && $validated['km'])) {
            $response = self::f626SendActivity($activity);
            if ($response->successful()) {
                $data = json_decode($response->body());
                $activity->exit_status = $data->exit_status;
            } else {
                $activity->exit_status = $response->status();
            }
        }
        $activity->save();
        return back();
    }


    /**
     * Elimina un'attività da un report non ancora concluso
     * @param Activity $activity
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Activity $activity)
    {
        // TODO add policy
        $activity->delete();
        return back();
    }


    /**
     * Verifica con una chiamata al gestionale se l'id del task esiste e se è assegnato all'utente che sta compilando il report.<br>
     * Il metodo viene usato da una chiamata ajax.<br>
     * La risposta attesa è un oggetto JSON con chiavi "match" (richiesto), "task_description" (opzionale) e "date_completed" (opzionale).
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public static function f626VerifyActivity(Request $request)
    {
        $validated = $request->validate([
            'task_id' => 'integer|required',
            'user_id' => 'integer|required',
        ]);

        // In locale simulo una risposta (varie opzioni per debug)
        if (config('app.env') == 'local') {
            $fake = self::f626VerifyActivityFaker(2);
            Http::fake([
                '*.facile626.it/*' => Http::response($fake['data'], $fake['status']),
            ]);
        }

        $url = config('gestionale.url') . config('gestionale.api_folder') . self::API_VERIFY_ACTIVITY;
        $response = Http::post($url, $validated);
        return $response->successful() ? $response->json() : response()->json(['match' => false], 200);
    }


    /**
     * Ritorna i due parametri da utilizzare con Http::fake()
     *
     * @param $type integer Tipo di richiesta
     * @return array { data: array, status: integer } Array con chiavi "data" e "status"
     */
    private static function f626VerifyActivityFaker(int $type=99)
    {
        switch ($type) {
            case 1: // successo, RSPP
                $fakeData = [
                    'match' => true,
                    'task_description' => 'Sopralluogo periodico RSPP di unità locale a basso rischio + relazione',
                    'date_completed' => now()->format('Y-m-d'),
                ];
                $fakeStatus = 200;
                break;
            case 2: // successo, HACCP
                $fakeData = [
                    'match' => true,
                    'task_description' => 'Sopralluogo periodico HACCP + relazione',
                    'date_completed' => now()->format('Y-m-d'),
                ];
                $fakeStatus = 200;
                break;
            case 3: // successo, campi opzionali non presenti
                $fakeData = [
                    'match' => true,
                    'task_description' => null,
                    'date_completed' => null,
                ];
                $fakeStatus = 200;
                break;
            case 98: // errore 500
                $fakeData = null;
                $fakeStatus = 500;
                break;
            case 99: // successo, il task non corrisponde
            default:
                $fakeData = ['match' => false];
                $fakeStatus = 200;
                break;
        }
        return ['data' => $fakeData, 'status' => $fakeStatus];
    }


    /**
     * Invia i dati dell'attività al gestionale utilizzando task_id come id univoco di collegamento.<br>
     * Il gestionale ritorna un file JSON con un'unica chiave "exit_status".
     *
     * @param Activity $activity
     * @return \Illuminate\Http\Client\Response
     */
    public static function f626SendActivity(Activity $activity)
    {
        $url = config('gestionale.url') . config('gestionale.api_folder') . self::API_SEND_ACTIVITY;
        $data = [
            'task_id' => $activity->task_id,
            'km' => $activity->km ?? null,
            'km_google' => $activity->km_google ?? null,
            'travel_minutes' => $activity->travel_minutes ?? null,
            'travel_minutes_google' => $activity->travel_minutes_google ?? null,
            'activity_minutes' => $activity->activity_minutes ?? null,
            'travel_from' => $activity->travel_from ?? null,
            'travel_to' => $activity->travel_to ?? null,
        ];

        // In locale simulo una risposta "tutto OK"
        // RIUNIONE 2022-07-26 PERNA/CAPPELLARO: salvo i dati in locale senza inviarli al gestionale;
        // per semplicità lascio attivo sempre il faker
        //if (config('app.env') == 'local') {
            Http::fake([
                '*.facile626.it/*' => Http::response(['exit_status' => 0], 200),
            ]);
        //}

        // sarebbe formalmente più corretto il metodo patch ma non credo che il gestionale lo supporti
        return Http::post($url, $data);
    }
}
