<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder12 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Fabio Zenarolla
        $zenarolla = User::create([
            'name'         => 'Fabio',
            'surname'      => 'Zenarolla',
            'email'        => 'f.zenarolla@facile626.it',
            'password'     => (app()->environment('local') ? Hash::make('123') : Hash::make('TgNu4v')),
            'f626_user_id' => 36933,
        ]);
        $zenarolla->assignRole('responsabile');
        $zenarolla->givePermissionTo(
            'esportare report',
            'esportare report marketing',
        );
    }
}
