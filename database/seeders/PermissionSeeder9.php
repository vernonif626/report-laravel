<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder9 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!Permission::where('name', 'visualizzare report amministrazione')->count()) {
            Permission::create(['name' => 'visualizzare report amministrazione']);
        }
        if (!Permission::where('name', 'esportare report amministrazione')->count()) {
            Permission::create(['name' => 'esportare report amministrazione']);
        }
    }
}
