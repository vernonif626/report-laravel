<?php

namespace Database\Seeders;

use App\Models\ActivityCategory;
use App\Models\ActivityType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ActivityTypeSeeder13 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Creo la nuova attività "Downgrade"
        $categories = ActivityCategory::all()->pluck('id', 'name');
        ActivityType::create([
            'name' => "Downgrade",
            'description' => null,
            'average_time' => 30,
            'activity_category_id' => $categories['Contatto'], // Non usato
            'fields' => "activity_minutes,task_id,client_id,value,travel_from*,travel_to*,km*,travel_minutes*",
            'paulitti_id' => 0, // Non usato
            'tecnico' => 0,
            'customer_care' => 0,
            'venditore' => 1,
            'order' => 10,
            'visible' => 1,
        ]);
    }
}
