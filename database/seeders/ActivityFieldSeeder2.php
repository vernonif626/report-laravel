<?php

namespace Database\Seeders;

use App\Models\ActivityField;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ActivityFieldSeeder2 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $activityFields = [
            [
                'name' => "rdc_pa",
                'description' => "Fonte",
                'type' => "select",
                'inputmode' => null,
                'size' => 2,
                'min' => null,
                'max' => null,
                'step' => null,
                'extra' => null,
                'option_values' => "rdc_pa",
            ],
            [
                'name' => "value",
//                'description' => "Valore del contratto (IVA esclusa)",
                'description' => "Importo", // può essere anche il valore dell'incasso che tra l'altro è IVA inclusa
                'type' => "number",
                'inputmode' => "numeric",
                'size' => 2,
                'min' => null,
                'max' => null,
                'step' => null,
                'extra' => null,
                'option_values' => null,
            ],
            [
                'name' => "outcome_in_person",
                'description' => "Esito",
                'type' => "select",
                'inputmode' => null,
                'size' => 3,
                'min' => null,
                'max' => null,
                'step' => null,
                'extra' => null,
                'option_values' => "outcome_in_person",
            ],
            [
                'name' => "outcome_call",
                'description' => "Esito",
                'type' => "select",
                'inputmode' => null,
                'size' => 3,
                'min' => null,
                'max' => null,
                'step' => null,
                'extra' => null,
                'option_values' => "outcome_call",
            ],
            [
                'name' => "outcome_pa",
                'description' => "Esito",
                'type' => "select",
                'inputmode' => null,
                'size' => 3,
                'min' => null,
                'max' => null,
                'step' => null,
                'extra' => null,
                'option_values' => "outcome_pa",
            ],
            /*
            [
                'name' => "",
                'description' => "",
                'type' => "",
                'inputmode' => "",
                'size' => 2,
                'min' => null,
                'max' => null,
                'step' => null,
                'extra' => null,
                'option_values' => null,
            ],
            */
        ];

        foreach ($activityFields as $activityField) {
            ActivityField::create($activityField);
        }
    }
}
