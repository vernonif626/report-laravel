<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder2 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!Permission::where('name', 'esportare report km')->count()) {
            Permission::create(['name' => 'esportare report km']);
        }

        foreach (User::role(['tecnico'])->get() as $user) {
            $user->givePermissionTo('esportare report km');
        }
    }
}
