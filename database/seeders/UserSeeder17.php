<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder17 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Vanessa Cristante
        $pechini = User::create([
            'name'         => 'Vanessa',
            'surname'      => 'Cristante',
            'email'        => 'v.cristante@facile626.it',
            'password'     => (app()->environment('local') ? Hash::make('123') : Hash::make('Dkk0')),
            'f626_user_id' => 77335,
        ]);
        $pechini->assignRole(['venditore', 'operativo']);
    }
}
