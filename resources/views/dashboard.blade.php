<x-app-layout>
    <x-slot name="header">
        <div class="container mt-5">
            <h2>Dashboard</h2>
        </div>
    </x-slot>
    <div class="container">

        {{-- Dashboard addetto --}}
        @can('compilare report')

            <div class="row">
                {{--  Form con scelta della data per la creazione di un nuovo report --}}
                <div class="col mt-3">
                    <div class="card">
                        <div class="card-header fw-bold">
                            Nuovo report
                        </div>
                        <div class="card-body">

                            <form class="row" method="post" action="{{ route('report.store') }}">
                                @csrf
                                <div class="col-sm-3 mb-3">
                                    <input
                                        name="date"
                                        id="report_date"
                                        type="date"
                                        class="form-control"
                                        aria-describedby="dateDesc"
                                        style="max-height: 38px; /*safari patch*/"
                                        required
                                        value="{{ now()->format('Y-m-d') }}"
                                        max="{{ now()->addMonth()->format('Y-m-d') }}"
                                    >
                                    <input
                                        type="hidden"
                                        name="absent"
                                        id="absent"
                                        value="0"
                                    >
                                    <div id="dateDesc" class="form-text">Data del report</div>
                                </div>

                                <div class="col-sm">
                                    <button type="submit" id="btn_new_report" class="btn btn-outline-primary" style="margin-right: 10px;">Nuovo report</button> oppure <a href="#" id="absent_link">segnala assenza in questa data</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            {{-- Verifica ultimi report inseriti --}}
            <div class="row">
                <div class="col-lg-6 mt-3">
                    <div class="card h-100">
                        <div class="card-header fw-bold">
                            Verifica ultimi report inseriti
                        </div>
                        <div class="card-body">
{{--                            <p>--}}
{{--                                <strong>Verifica ultimi report inseriti</strong>--}}
{{--                            </p>--}}
                            <p>
                                @foreach($days as $day)
                                    <span class="badge bg-{{ $day['class'] }}">{{ $day['day'] }}</span>
                                @endforeach
                            </p>
                            <p class="small mb-0">
                                <span class="badge bg-success">&nbsp;</span> inserito <span class="badge bg-danger">&nbsp;</span> mancante <span class="badge bg-warning">&nbsp;</span> non concluso
                            </p>
                        </div>
                    </div>
                </div>

                {{-- Report non conclusi --}}
                <div class="col-lg-6 mt-3">
                    <div class="card h-100">
                        <div class="card-header fw-bold">
                            Report non conclusi
                        </div>
                        <div class="card-body">
                            @if($activity_dates->count())
                                <p>
                                @foreach($activity_dates as $activity_date)
                                    <a href="{{ route('report.edit', [$activity_date]) }}">{{ $activity_date->localizedLongDate }}</a><br>
                                @endforeach
                                </p>
                            @else
                                Nessuno
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        @endcan

        {{-- Dashboard responsabile --}}
        @can('visualizzare report')
            <div class="row mt-5">

                {{-- Report dei tecnici --}}
                @can('visualizzare report tecnici')
                    <div class="col-md mb-3">
                        <h3>Tecnici</h3>
                        <table class="table">
                            <thead>
                                <th scope="row">Nome</th>
                                <th scope="row">Report</th>
                            </thead>
                            <tbody>
                                @foreach($usersTecnici as $user)
                                    <tr>
                                        <td>
                                            <a href="{{ route('report.user_index', $user) }}">{{ $user->fullName }}</a>
                                        </td>
                                        <td>
                                            {{ $user->activityDates->where('archived', 1)->count() }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @endcan

                {{-- Report del customer care e amministrazione --}}
                @can('visualizzare report customer care')
                    {{-- Customer care --}}
                    <div class="col-md mb-3">
                        <h3>Customer care</h3>
                        <table class="table">
                            <thead>
                            <th scope="row">Nome</th>
                            <th scope="row">Report</th>
                            </thead>
                            <tbody>
                            @foreach($usersCustomerCare as $user)
                                <tr>
                                    <td>
                                        <a href="{{ route('report.user_index', $user) }}">{{ $user->fullName }}</a>
                                    </td>
                                    <td>
                                        {{ $user->activityDates->where('archived', 1)->count() }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        {{-- Amministrazione --}}
                        <h3>Amministrazione</h3>
                        <table class="table">
                            <thead>
                            <th scope="row">Nome</th>
                            <th scope="row">Report</th>
                            </thead>
                            <tbody>
                            @foreach($usersAmministrativi as $user)
                                <tr>
                                    <td>
                                        <a href="{{ route('report.user_index', $user) }}">{{ $user->fullName }}</a>
                                    </td>
                                    <td>
                                        {{ $user->activityDates->where('archived', 1)->count() }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @endcan

                {{-- Report dei venditori --}}
                @can('visualizzare report venditori')
                    <div class="col-md mb-3">
                        <h3>Vendite</h3>
                        <table class="table">
                            <thead>
                            <th scope="row">Nome</th>
                            <th scope="row">Report</th>
                            </thead>
                            <tbody>
                            @foreach($usersVenditori as $user)
                                <tr>
                                    <td>
                                        <a href="{{ route('report.user_index', $user) }}">{{ $user->fullName }}</a>
                                    </td>
                                    <td>
                                        {{ $user->activityDates->where('archived', 1)->count() }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @endcan

            </div>
        @endcan

        @can('visualizzare dettagli tecnici')
        <div class="small mt-5">
            Laravel v{{ Illuminate\Foundation\Application::VERSION }} - PHP v{{ PHP_VERSION }}
        </div>
        @endcan
    </div>

    <x-slot name="script">
        <script>
            {{-- Mostra i campi dell'attività scelta --}}
            $('#absent_link').click(function (event) {
                event.preventDefault();
                if (confirm("Confermi l'assenza?\nNon potrai inserire un report per la stessa data.")) {
                    $('#absent').val('1');
                    this.closest('form').submit();
                }
            });

            {{-- Disabilita il pulsante "Nuovo report" se la data è futura --}}
            $('#report_date').change(function() {
                let today = new Date();
                if (new Date($(this).val()) > today) {
                    $('#btn_new_report').prop("disabled",true);
                } else {
                    $('#btn_new_report').prop("disabled",false);
                }
            });
        </script>
    </x-slot>
</x-app-layout>


