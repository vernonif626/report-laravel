<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder19 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Francesco Fanetti
        $fanetti = User::create([
            'name'         => 'Francesco',
            'surname'      => 'Fanetti',
            'email'        => 'f.fanetti@facile626.it',
            'password'     => (app()->environment('local') ? Hash::make('123') : Hash::make('kFD0')),
            'f626_user_id' => 78226,
        ]);
        $fanetti->assignRole(['venditore', 'operativo']);
    }
}
