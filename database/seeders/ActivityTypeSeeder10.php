<?php

namespace Database\Seeders;

use App\Models\ActivityType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ActivityTypeSeeder10 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ActivityType::where('name', "Affiancamento fatto")->update([
            'fields' => "activity_minutes,target_user_id,travel_from*,travel_to*,km*,travel_minutes*",
        ]);
    }
}
