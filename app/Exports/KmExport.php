<?php

namespace App\Exports;

use App\Models\Activity;
use App\Models\ActivityDate;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

/**
 * Esportazione per rimborso chilometrico dei tecnici.<br>
 * Utilizza <a href="https://docs.laravel-excel.com/3.1/getting-started/">Laravel Excel</a>,
 * basato su <a href="https://phpspreadsheet.readthedocs.io/">PhpSpreadsheet</a>.
 */
class KmExport implements FromCollection, WithStrictNullComparison, WithHeadings, ShouldAutoSize
{
    protected string $date_start;
    protected string $date_end;
    protected User $user;

    public function __construct(string $date_start, string $date_end, User $user)
    {
        $this->date_start = $date_start;
        $this->date_end = $date_end ?: $date_start;
        $this->user = $user;
    }

    public function headings(): array
    {
        $headings = [
            'Data',
            'Partenza',
            'Destinazione',
            'ID UL/Cli',
            'ID Task',
            'Attività',
            'ID Corso',
            'Km',
        ];
        if (auth()->user()->can('controllare rimborsi')) {
            $headings[] = 'Km Google';
            $headings[] = 'Km (differenza)';
        }
        return $headings;
    }


    /**
    * @return Collection
    */
    public function collection(): Collection
    {
        $activityDates = ActivityDate::with('activities')->with('user')
            ->where('date', '>=', $this->date_start)
            ->where('date', '<=', $this->date_end)
            ->where('archived', '=', 1)
            ->where('user_id', $this->user->id)
            ->orderBy('date')
            ->get();

        // Creo un array che corrisponde al file Excel del rimborso Km C07.60.01
        $activitiesExport = [];
        $km = 0;
        $kmGoogle = 0;

        foreach ($activityDates as $activityDate) {
            $km += $activityDate->activities->sum('km');
            $kmGoogle += $activityDate->activities->sum('km_google');

            // Attività
            foreach ($activityDate->activities as $activity) {
                if ($activity->km > 0) {
                    if ($this->user->hasRole('tecnico')) {
                        $idUlCli = $activity->address_id;
                    } else {
                        // per ora venditori
                        $idUlCli = $activity->client_id;
                    }
                    $activitiesExport[] = [
                        'Data' => Carbon::create($activity->date)->format('j/n/Y'),
                        'Partenza' => $activity->travel_from,
                        'Destinazione' => $activity->travel_to,
                        'ID UL/Cli' => $idUlCli,
                        'ID Task' => $activity->task_id,
                        'Attività' => $activity->activityType->name,
                        'ID Corso' => $activity->course_id,
                        'Km' => $activity->km,
                    ] + ((auth()->user()->can('controllare rimborsi')) ? ['Km Google' => $activity->km_google, 'Km (differenza)' => sprintf("%+01.1f", $activity->km - $activity->km_google)] : []);
                }
            }
        }

        // Totali del periodo (se l'utente è autorizzato)
        if (auth()->user()->can('controllare rimborsi')) {
            $activitiesExport[] = [
                'Data' => 'Totale',
                'Partenza' => '',
                'Destinazione' => '',
                'ID UL/Cli' => '',
                'ID Task' => '',
                'Attività' => '',
                'ID Corso' => '',
                'Km' => $km,
                'Km Google' => $kmGoogle,
                'Km (differenza)' => sprintf("%+01.1f", $km - $kmGoogle),
            ];
        }
        return collect($activitiesExport);
    }
}
