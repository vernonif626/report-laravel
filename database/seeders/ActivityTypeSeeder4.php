<?php

namespace Database\Seeders;

use App\Models\ActivityCategory;
use App\Models\ActivityType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ActivityTypeSeeder4 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = ActivityCategory::all()->pluck('id', 'name');

        // Tolgo id cliente da "Verifica fabbisogno formativo"
        $at = ActivityType::where('name', "Verifica fabbisogno formativo")->first();
        $at->description = "Verifica del fabbisogno corsi di un cliente o lavoratore o dello stato fruizione corso online, o verifica fabbisogno di corsi in una determinata zona";
        $at->fields = 'activity_minutes';
        $at->save();

        // Nuova attività "Nomina / sostituzione tecnico"
        $at = new ActivityType();
        $at->name = "Nomina / sostituzione tecnico";
        $at->description = "Telefonata per firma in FEA della nomina";
        $at->activity_category_id = $categories['Contatto'];
        $at->fields = "activity_minutes,client_id";
        $at->paulitti_id = 23;
        $at->tecnico = 0;
        $at->customer_care = 1;
        $at->order = 230;
        $at->visible = 1;
        $at->save();

        // Nuova attività "Discente disiscritto corso"
        $at = new ActivityType();
        $at->name = "Discente disiscritto corso";
        $at->description = "Discente disiscritto da qualsiasi tipo di corso";
        $at->activity_category_id = $categories['Corso'];
        $at->fields = "quantity,activity_minutes,client_id";
        $at->paulitti_id = 24;
        $at->tecnico = 0;
        $at->customer_care = 1;
        $at->order = 240;
        $at->visible = 1;
        $at->save();
    }
}
