<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder5 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Alessandro Ecoretti
        $ecoretti = User::create([
            'name'         => 'Alessandro',
            'surname'      => 'Ecoretti',
            'email'        => 'a.ecoretti@facile626.it',
            'password'     => (app()->environment('local') ? Hash::make('123') : Hash::make('rDxQjd')),
            'f626_user_id' => 52548,
        ]);
        $ecoretti->assignRole('responsabile');
        $ecoretti->givePermissionTo('controllare rimborsi', 'visualizzare report', 'visualizzare report tecnici', 'visualizzare report customer care', 'visualizzare report venditori');

        // Lucia Iervasutti
        $iervasutti = User::create([
            'name'         => 'Lucia',
            'surname'      => 'Iervasutti',
            'email'        => 'l.iervasutti@facile626.it',
            'password'     => (app()->environment('local') ? Hash::make('123') : Hash::make('OoV0uu')),
            'f626_user_id' => 46296,
        ]);
        $iervasutti->assignRole('responsabile');
        $iervasutti->givePermissionTo('controllare rimborsi', 'visualizzare report', 'visualizzare report tecnici', 'visualizzare report customer care', 'visualizzare report venditori');
    }
}
