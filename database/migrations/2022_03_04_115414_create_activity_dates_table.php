<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_dates', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->unsignedBigInteger('user_id');
            $table->date('date');
            $table->unsignedSmallInteger('communications')->nullable()->comment('Comunicazioni in ritardo');
            $table->unsignedSmallInteger('tasks')->nullable()->comment('Tasks in ritardo');
            $table->unsignedFloat('hours', 2, 1)->nullable()->comment('Ore timbrate');
            $table->boolean('archived')->default(0)->comment("Conclude l'inserimento dei dati della giornata.");

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_dates');
    }
};
