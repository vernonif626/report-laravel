<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ActivityDate extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'date',
        'communications',
        'tasks',
        'hours',
        'absent',
        'archived',
        'tasks_matched',
    ];

    protected $casts = [
        'date' => 'date',
    ];

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function activities()
    {
        return $this->hasMany(Activity::class);
    }

    protected function localizedLongDate(): Attribute
    {
        $date = Carbon::create($this->date)->locale(app()->getLocale());

        return Attribute::make(
            get: fn ($value) => $date->day . ' ' . $date->monthName . ' ' . $date->year,
        );
    }

    protected function activityMinutes(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => $this->activities()->sum('activity_minutes'),
        );
    }

    protected function travelMinutes(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => $this->activities()->sum('travel_minutes'),
        );
    }

    protected function totalMinutes(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => $this->activities()->sum('activity_minutes') + $this->activities()->sum('travel_minutes'),
        );
    }

    /**
     * Numero totale di contratti inseriti nel report (Facile626 + corsi)
     * @return Attribute
     */
    protected function sumContratti(): Attribute
    {
//        $activityContrattoF626Id = ActivityType::where('name', 'Contratto F626')->first()->id;
//        $activityContrattoCorsiId = ActivityType::where('name', 'Contratto corsi')->first()->id;
//        $contrattiF626 = $this->activities()->where('activity_type_id', $activityContrattoF626Id)->count();
//        $contrattiCorsi = $this->activities()->where('activity_type_id', $activityContrattoCorsiId)->count();
//        $sum_contratti = $contrattiF626 + $contrattiCorsi;
        // I contratti sono dove c'è un valore, escludendo l'incasso
        $incasso_id = ActivityType::where('name', 'Incasso')->first()->id;
        $sum_contratti = $this->activities()->where([ ['value', '>', 0], ['activity_type_id', '<>', $incasso_id] ])->count();

        return Attribute::make(
            get: fn ($value) => $sum_contratti,
        );
    }

    /**
     * Valore totale dei contratti inseriti nel report (Facile626 + corsi)
     * @return Attribute
     */
    protected function sumContrattiValue(): Attribute
    {
        $incasso_id = ActivityType::where('name', 'Incasso')->first()->id;
        // I contratti sono dove c'è un valore, escludendo l'incasso
        $sum_contratti_value = $this->activities()->where([ ['value', '>', 0], ['activity_type_id', '<>', $incasso_id] ])->sum('value');

        return Attribute::make(
            get: fn ($value) => $sum_contratti_value,
        );
    }


    /**
     * Fornisce una stringa formattata in ore e minuti.<br>
     * Di default non torna nulla se i minuti sono minori di 60, essendo di fatto leggibili senza necessità di conversione.
     *
     * @param integer $minutes Minuti totali
     * @param bool $always Fornisce i minuti formattati anche se i minuti totali sono minori di 60
     * @return string
     */
    public static function formattedMinutes(int $minutes, bool $always = false): string
    {
        $durationTxt = '';
//        if ($minutes >= 60 && !$always) {
            $durationTxt .= implode("&nbsp;", [
                floor($minutes / 60) > 0 ? floor($minutes / 60) . 'h' : '',
                ((floor($minutes / 60) > 0 && $minutes % 60 > 0) || $always) ? $minutes % 60 . "'" : ''
            ]);
//            $h = floor($minutes / 60) > 0 ? floor($minutes / 60) . 'h' : '';
//            $m = (floor($minutes / 60) > 0 && $minutes % 60 > 0) ? " " . $minutes % 60 . "'" : '';

//        }
        return $durationTxt;
    }


    /**
     * Fornisce un array con alcune statistiche del dipendente a seconda del ruolo.<br>
     * Vengono mostrate nella pagina di dettaglio del report.
     *
     * @return array
     */
    public function quickStats(): array
    {
        $stats = [];
        if ($this->user->hasRole('addetto customer care')) {
            $appFissato = ActivityType::where('name', "Appuntamento fissato")->first()->id;
            $appFissati = $this->activities()->where('activity_type_id', $appFissato)->count();
            if ($appFissati) {
                //$stats['Appuntamenti fissati'] = $appFissati . " (" . $appFissati / $this->hours . "/ora)";
                $stats['Appuntamenti fissati'] = $appFissati;
                //$stats['Appuntamenti fissati/ora'] = $appFissati / $this->hours;
            }
            $nomina = ActivityType::where('name', "Nomina / sostituzione tecnico")->first()->id;
            $nomine = $this->activities()->where('activity_type_id', $nomina)->count();
            if ($nomine) {
                $stats['Nomine o sostituzioni tecnico'] = $nomine;
                //$stats['Nomine/ora'] = $nomine / $this->hours;
            }
        }
        return $stats;
    }

}
