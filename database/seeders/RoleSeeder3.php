<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleSeeder3 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!Role::where('name', 'amministrativo')->count()) {
            $roleAmministrativo = Role::create(['name' => 'amministrativo']);
            $roleAmministrativo->givePermissionTo('compilare report');
        }
        if (!Role::where('name', 'responsabile amministrazione')->count()) {
            $roleAmministrativo = Role::create(['name' => 'responsabile amministrazione']);
            $roleAmministrativo->givePermissionTo('visualizzare report');
            $roleAmministrativo->givePermissionTo('visualizzare report amministrazione');
            $roleAmministrativo->givePermissionTo('esportare report');
            $roleAmministrativo->givePermissionTo('esportare report amministrazione');
        }

        // Aggiungo i permessi di visualizzare/esportare i report dell'amministrazione al BPM (Paulitti)
        $roleBpm = Role::where('name', 'business process manager')->first();
        $roleBpm->givePermissionTo('visualizzare report amministrazione');
        $roleBpm->givePermissionTo('esportare report amministrazione');
    }
}
