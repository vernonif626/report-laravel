<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder16 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Disabilitazione accessi Felcaro, Gallarati, Zampini
        User::where('email', 's.felcaro@facile626.it')->update([
            'password' => (app()->environment('local') ? Hash::make('123') : Hash::make('iuwerh!£$sfihbgi783iebv87trt')),
            'active' => 0,
        ]);
        User::where('email', 'r.gallarati@facile626.it')->update([
            'password' => (app()->environment('local') ? Hash::make('123') : Hash::make('iuwerh!£$sfihbgi783iebv87trt')),
            'active' => 0,
        ]);
        User::where('email', 'p.zampini@facile626.it')->update([
            'password' => (app()->environment('local') ? Hash::make('123') : Hash::make('iuwerh!£$sfihbgi783iebv87trt')),
            'active' => 0,
        ]);
    }
}
