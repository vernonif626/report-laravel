<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder2 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $u = User::create([
            'name'         => 'Maurizio',
            'surname'      => 'Morabito',
            'email'        => 'm.morabito@facile626.it',
            'password'     => (app()->environment('local') ? Hash::make('123') : Hash::make('1T*Rb')),
            'f626_user_id' => 71493,
        ]);
        $u->save();

        $u->assignRole('tecnico');
        $u->assignRole('operativo');
    }
}
