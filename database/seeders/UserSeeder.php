<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => 'Super',
                'surname' => 'Admin',
                'email' => 'procedure@facile626.it',
                'password' => (app()->environment('local') ? Hash::make('123') : Hash::make('F4c1l3626')),
                'f626_user_id' => 0,
                'roles' => ['admin'],
            ],
            [
                'name' => 'Claudio',
                'surname' => 'Bettin',
                'email' => 'c.bettin@facile626.it',
                'password' => (app()->environment('local') ? Hash::make('123') : Hash::make('RkdW')),
                'f626_user_id' => 66503,
                'roles' => ['tecnico', 'operativo'],
            ],
            [
                'name' => 'Fabiola',
                'surname' => 'Celaj',
                'email' => 'f.celaj@facile626.it',
                'password' => (app()->environment('local') ? Hash::make('123') : Hash::make('xVbhfW')),
                'f626_user_id' => 61273,
                'roles' => ['tecnico', 'operativo'],
            ],
            [
                'name' => 'Lara',
                'surname' => 'Driutti',
                'email' => 'l.driutti@facile626.it',
                'password' => (app()->environment('local') ? Hash::make('123') : Hash::make('oRQOIP')),
                'f626_user_id' => 62426,
                'roles' => ['tecnico', 'operativo'],
            ],
//            [
//                'name' => 'Davide',
//                'surname' => 'Manganiello',
//                'email' => 'd.manganiello@facile626.it',
//                'password' => (app()->environment('local') ? Hash::make('123') : Hash::make('FWVrVg')),
//                'f626_user_id' => 59057,
//                'roles' => ['tecnico', 'operativo'],
//            ],
            [
                'name' => 'Alessio',
                'surname' => 'Schiavon',
                'email' => 'a.schiavon@facile626.it',
                'password' => (app()->environment('local') ? Hash::make('123') : Hash::make('ogHmCx')),
                'f626_user_id' => 49021,
                'roles' => ['tecnico', 'operativo'],
            ],
            [
                'name' => 'Sara',
                'surname' => 'Scollato',
                'email' => 's.scollato@facile626.it',
                'password' => (app()->environment('local') ? Hash::make('123') : Hash::make('#*p$N')),
                'f626_user_id' => 69877,
                'roles' => ['tecnico', 'operativo'],
            ],
            [
                'name' => 'Marta',
                'surname' => 'Sodano',
                'email' => 'm.sodano@facile626.it',
                'password' => (app()->environment('local') ? Hash::make('123') : Hash::make('2xtt6')),
                'f626_user_id' => 67012,
                'roles' => ['tecnico', 'operativo'],
            ],
//            [
//                'name' => 'Maria Elsa',
//                'surname' => 'Villano',
//                'email' => 'e.villano@facile626.it',
//                'password' => (app()->environment('local') ? Hash::make('123') : Hash::make('X^vQx')),
//                'f626_user_id' => 69031,
//                'roles' => ['tecnico', 'operativo'],
//            ],
            [
                'name' => 'Antonio',
                'surname' => 'Matrone',
                'email' => 'a.matrone@facile626.it',
                'password' => (app()->environment('local') ? Hash::make('123') : Hash::make('sWCF')),
                'f626_user_id' => 69031,
                'roles' => ['tecnico', 'operativo'],
            ],
            [
                'name' => 'Natascia',
                'surname' => 'Della Schiava',
                'email' => 'n.dellaschiava@facile626.it',
                'password' => (app()->environment('local') ? Hash::make('123') : Hash::make('Hy6s9H')),
                'f626_user_id' => 255,
                'roles' => ['addetto customer care', 'operativo'],
            ],
            [
                'name' => 'Daniela',
                'surname' => 'Micoli',
                'email' => 'd.micoli@facile626.it',
                'password' => (app()->environment('local') ? Hash::make('123') : Hash::make('&!Eoh')),
                'f626_user_id' => 68713,
                'roles' => ['addetto customer care', 'operativo'],
            ],
            [
                'name' => 'Giulia',
                'surname' => 'Scarel',
                'email' => 'g.scarel@facile626.it',
                'password' => (app()->environment('local') ? Hash::make('123') : Hash::make('anMU')),
                'f626_user_id' => 67006,
                'roles' => ['addetto customer care', 'operativo'],
            ],
            [
                'name' => 'Grazia',
                'surname' => 'Fiorentino',
                'email' => 'g.fiorentino@facile626.it',
                'password' => (app()->environment('local') ? Hash::make('123') : Hash::make('Cf0yx8')),
                'f626_user_id' => 57,
                'roles' => ['responsabile tecnici', 'responsabile customer care', 'responsabile'],
            ],
            [ // TODO aggiungere a Silvia il ruolo 'venditore' o capire se Silvia ed Erika hanno un unico account "vendite" come per gli excel
                'name' => 'Silvia',
                'surname' => 'Bigolaro',
                'email' => 's.bigolaro@facile626.it',
                'password' => (app()->environment('local') ? Hash::make('123') : Hash::make('dUGWme')),
                'f626_user_id' => 57008,
                'roles' => ['responsabile venditori', 'responsabile'],
            ],
            [
                'name' => 'Michele',
                'surname' => 'Paulitti',
                'email' => 'info@for-log.it',
                'password' => (app()->environment('local') ? Hash::make('123') : Hash::make('a^J8q')),
                'f626_user_id' => 65861,
                'roles' => ['business process manager', 'responsabile'],
            ],
        ];

        foreach ($users as $user) {
            $u = User::create([
                'name'         => $user['name'],
                'surname'      => $user['surname'],
                'email'        => $user['email'],
                'password'     => $user['password'],
                'f626_user_id' => $user['f626_user_id'],
            ]);
            $u->save();

            foreach($user['roles'] as $role) {
                $u->assignRole($role);
            }
        }

    }
}
