<x-app-layout>
    <x-slot name="header">
        <div class="container mt-5">
            <h2>Vendite</h2>
        </div>
    </x-slot>
    <div class="container">

        {{-- Filtri --}}
        <div class="row row-cols-lg-auto g-3 mt-3">

            {{-- Oggi / Ieri --}}
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <a class="btn btn-outline-primary" href="{{ route('report.vendite_show', ['date_start' => 'oggi']) }}">Oggi</a>
                        <a class="btn btn-outline-primary" href="{{ route('report.vendite_show', ['date_start' => 'ieri']) }}">Ieri</a>
                    </div>
                </div>
            </div>

            {{-- Singola data --}}
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form method="post" action="{{ route('report.vendite_range') }}" class="row row-cols-lg-auto g-3 align-items-center">
                            @csrf
                            <label for="date_start">Singolo giorno</label>
                            <div class="col-12">
                                <input type="date" class="form-control" id="date_start" name="date_start" style="max-height: 38px; /*safari patch*/" value="{{ $day->toDateString() }}">
                            </div>
                            <div class="col-12">
                                <button type="submit" class="btn btn-primary">Invia</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            {{-- Intervallo --}}
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form method="post" action="{{ route('report.vendite_range') }}" class="row row-cols-lg-auto g-3 align-items-center">
                            @csrf
                            <label for="date_start">Intervallo dal</label>
                            <div class="col-12">
                                <input type="date" class="form-control" id="date_start" name="date_start" style="max-height: 38px; /*safari patch*/" value="{{ $start->toDateString() }}">
                            </div>
                            <label for="date_end">al</label>
                            <div class="col-12">
                                <input type="date" class="form-control" id="date_end" name="date_end" style="max-height: 38px; /*safari patch*/" value="{{ $end->toDateString() }}">
                            </div>
                            <div class="col-12">
                                <button type="submit" class="btn btn-primary">Invia</button>
                            </div>
                        </form>
                   </div>
                </div>
            </div>

        </div>

        <p class="text-muted mt-2">
            In caso di giorno singolo (inclusi oggi e ieri), i valori totali mostrano sempre il progressivo dall'inizio del mese della data scelta.
        </p>

        {{-- Dati nell'intervallo richiesto --}}
        <h5 class="mt-4">{{ $day->locale('it')->isoFormat('dddd D MMMM YYYY') }}</h5>

        <table class="table">
            <thead>
            <th scope="col">Descrizione</th>
            <th scope="col">{{ $dayTxt }}</th>
            <th scope="col">
                @if($isRange)
                    Dal {{ $start->isoFormat('D/M/YYYY') }} al {{ $end->isoFormat('D/M/YYYY') }}
                @else
                    Progressivo mese al {{ $day->day }}/{{ $day->month }}
                @endif
            </th>
            </thead>
            <tbody>
            @foreach($report_values as $row)
                <tr>
                    <td>
                        {{ $row['name'] }}
                        @if($row['description'])
                            <br><span class="small text-muted">{{ $row['description'] }}</span>
                        @endif
                    </td>
                    <td>{{ $row['day'] }}</td>
                    <td>{{ $row['month'] }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {{-- Copia e incolla per WhatsApp quotidiano a Perna --}}
        @if(!$isRange)
            <div class="row mt-5">
                <div class="col-md mb-3">
                    <h6>WhatsApp</h6>
                    <div class="card" style="width: 18rem; background-color: #ffff99">
                        <div class="card-body">
                            <p class="card-text">
                                *Report {{ $day->isoFormat('D/M/YYYY') }}*<br>
                                @foreach($report_values as $row)
                                    *{{ $row['whatsapp'] }}* {{ $row['day'] }} - {{ $row['month'] }}@if($row['whatsapp'] == 'FATT')/...@endif<br>
                                @endforeach
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        @endif

    </div>

{{--    <x-slot name="script">--}}
{{--        <script>--}}
{{--        </script>--}}
{{--    </x-slot>--}}
</x-app-layout>


