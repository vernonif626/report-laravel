<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder9 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::where('email', 'm.morabito@facile626.it')->update([
            'password' => (app()->environment('local') ? Hash::make('123') : Hash::make('iuwerh!£$sfihbgi783iebv87trt')),
        ]);
    }
}
