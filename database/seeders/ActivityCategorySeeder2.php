<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ActivityCategorySeeder2 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('activity_categories')->insert([
            [ 'name' => "Agenda"],
            [ 'name' => "Corso"],
            [ 'name' => "Reception"],
        ]);
    }
}
