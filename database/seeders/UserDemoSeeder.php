<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class UserDemoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $id = DB::table('users')->insert([
            'name' => 'Tecnico',
            'surname' => 'Demo',
            'email' => 'tecnico@facile626.it',
            'password' => Hash::make('tecnico'),
        ]);

        $tecnico = User::find($id);
        $tecnico->assignRole('tecnico');

        $id = DB::table('users')->insert([
            'name' => 'Customer Care',
            'surname' => 'Demo',
            'email' => 'customer_care@facile626.it',
            'password' => Hash::make('customer_care'),
        ]);

        $customer_care = User::find($id);
        $customer_care->assignRole('addetto customer care');
    }
}
