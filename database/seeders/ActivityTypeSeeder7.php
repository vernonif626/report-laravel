<?php

namespace Database\Seeders;

use App\Models\ActivityType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ActivityTypeSeeder7 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ActivityType::where('name', "Consegna documento")->update([
            'fields' => "document_type,activity_minutes,travel_from,travel_to,km,travel_minutes,client_id,address_id,task_id",
        ]);

        ActivityType::where('name', "Firma nomina (telefono)")->update([
            'fields' => "role,activity_minutes,travel_from,travel_to,km,travel_minutes,client_id,address_id,task_id",
        ]);

        ActivityType::where('name', "Primo sopralluogo")->update([
            'fields' => "role,activity_minutes,travel_from,travel_to,km,travel_minutes,client_id,address_id,task_id",
        ]);

        ActivityType::where('name', "Sopralluogo periodico")->update([
            'fields' => "role,activity_minutes,travel_from,travel_to,km,travel_minutes,client_id,address_id,task_id",
        ]);

        ActivityType::where('name', "Sopralluogo extra")->update([
            'fields' => "role,activity_minutes,travel_from,travel_to,km,travel_minutes,client_id,address_id,task_id",
        ]);

        ActivityType::where('name', "Redazione documento")->update([
            'fields' => "document_type,activity_minutes,client_id,address_id",
        ]);

        ActivityType::where('name', "Upload documento")->update([
            'fields' => "document_type,activity_minutes,client_id,address_id",
        ]);
    }
}
