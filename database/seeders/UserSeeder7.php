<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder7 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Simone Cappellaro
        $cappellaro = User::create([
            'name'         => 'Simone',
            'surname'      => 'Cappellaro',
            'email'        => 's.cappellaro@facile626.it',
            'password'     => (app()->environment('local') ? Hash::make('123') : Hash::make('AlU7g8')),
            'f626_user_id' => 64046,
        ]);
        $cappellaro->assignRole('responsabile');
        $cappellaro->givePermissionTo(
            'controllare rimborsi',
            'visualizzare report',
            'visualizzare report tecnici',
            'visualizzare report customer care',
            'visualizzare report venditori',
            'esportare report',
            'esportare report tecnici',
            'esportare report customer care',
            'esportare report venditori',
        );
    }
}
