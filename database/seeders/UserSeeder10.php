<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder10 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Debora Panzeri
        $panzeri = User::create([
            'name'         => 'Debora',
            'surname'      => 'Panzeri',
            'email'        => 'd.panzeri@facile626.it',
            'password'     => (app()->environment('local') ? Hash::make('123') : Hash::make('9UyA')),
            'f626_user_id' => 75184,
        ]);
        $panzeri->assignRole('operativo');
        $panzeri->assignRole('addetto customer care');

        // Disabilitazione accessi Della Schiava e Micoli
        User::where('email', 'n.dellaschiava@facile626.it')->update([
            'password' => (app()->environment('local') ? Hash::make('123') : Hash::make('iuwerh!£$sfihbgi783iebv87trt')),
        ]);
        User::where('email', 'd.micoli@facile626.it')->update([
            'password' => (app()->environment('local') ? Hash::make('123') : Hash::make('iuwerh!£$sfihbgi783iebv87trt')),
        ]);

    }
}
