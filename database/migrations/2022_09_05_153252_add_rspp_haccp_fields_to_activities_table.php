<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('activities', function (Blueprint $table) {
            $table->string('role', 1)->after('activity_type_id')->nullable()->comment('r = RSPP; h = HACCP');
            $table->string('document_type', 2)->after('role')->nullable()->comment('d = DVR; h = manuale HACCP; (d/h)i = integrazione');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('activities', function (Blueprint $table) {
            $table->dropColumn('role');
            $table->dropColumn('document_type');
        });
    }
};
