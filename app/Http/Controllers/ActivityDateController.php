<?php

namespace App\Http\Controllers;

use App\Exports\KmExport;
use App\Exports\ReportAmministrazioneExport;
use App\Exports\ReportCustomerCareExport;
use App\Exports\ReportVenditoriMarketingExport;
use App\Exports\ReportTecniciExport;
use App\Exports\ReportVenditoriExport;
use App\Models\Activity;
use App\Models\ActivityDate;
use App\Models\ActivityType;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Facades\Excel;

class ActivityDateController extends Controller
{
    const API_VERIFY_REPORT = 'report_verify_report.php';

    /**
     * Mostra l'indice dei report inseriti dall'utente
     * @param User $user
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|void
     */
    public function userIndex(User $user)
    {
        if ((auth()->user()->id == $user->id) || auth()->user()->can('visualizzare report')) {
            $reports = ActivityDate::with('activities')->where([['user_id', '=', $user->id], /*['archived', '=', 1]*/])->orderBy('date', 'desc')->get();
            return view('report.user_index', compact(['reports', 'user']));
        } else {
            abort(401);
        }
    }


    /**
     * Mostra il report
     * @param ActivityDate $activityDate
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|void
     */
    public function show(ActivityDate $activityDate)
    {
        if ((auth()->user()->id == $activityDate->user->id) || auth()->user()->can('visualizzare report')) {

            foreach(User::reportRoles() as $role => $data) {
                if ($activityDate->user->hasRole($role)) {
                    $dept = $data['dept'];
                    break;
                }
            }

            $activities = $activityDate->activities()->with('activityType')->get();
            return view('report.show', compact(['activityDate', 'activities', 'dept']));
        } else {
            abort(401);
        }
    }


    /**
     * Salva il report in db
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            // La data deve essere univoca, ma solo per lo stesso user id
            'date' => [
                'required',
                'date',
                Rule::unique('activity_dates')->where(fn ($query) => $query->where('user_id', auth()->user()->id))
            ],
            'tasks' => 'nullable|integer',
            'communications' => 'nullable|integer',
            'hours' => 'nullable',
            'absent' => 'nullable|boolean',
            'tasks_matched' => 'nullable|boolean',
        ]);
        $validated['user_id'] = auth()->user()->id;

        // Se l'addetto inserisce un'assenza torna alla dashboard, altrimenti inizia a compilare il report
        $validated['archived'] = (bool) $validated['absent']; // Se assente, chiude (archivia) immediatamente il report
        $activityDate = ActivityDate::create($validated);
        if($validated['absent']) {
            $route = route('dashboard');
        } else {
            $route = route('report.edit', $activityDate);
        }
        return redirect($route);
    }


    /**
     * Modifica un report non ancora concluso
     * @param ActivityDate $activityDate
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(ActivityDate $activityDate)
    {
        // Non deve essere possibile modificare un record concluso
        if ($activityDate->archived) {
            abort(403, "Report chiuso");
        }

        // Solo il proprietario può modificare un report
        if ($activityDate->user->id <> auth()->user()->id) {
            abort(403, "Report di altro utente");
        }

        // Verifica il ruolo dell'utente e passa alla view solo le relative attività
        $field = 'tecnico'; // fallback, non dovrebbe essere necessario
        foreach(User::reportRoles() as $role => $data) {
            if (auth()->user()->hasRole($role)) {
                $field = $data['field'];
                break;
            }
        }
        $activityList = ActivityType::where($field, true)->where('visible', 1)->get();

        // Lista delle attività già inserite nella data
        $activities = Activity::with('activityType')->where('date', $activityDate->date)->where('user_id', $activityDate->user_id)->get();

        // Lista degli utenti da presentare nella attività di affiancamento o consulenza interna
        $users = User::role(['operativo', 'responsabile'])->orderBy('name')->get();

        return view('report.edit', compact(['activityDate', 'activityList', 'activities', 'users']));
    }


    /**
     * Chiude il report giornaliero
     * @param Request $request
     * @param ActivityDate $activityDate
     * @return \Illuminate\Routing\Redirector
     */
    public function update(Request $request, ActivityDate $activityDate)
    {
        $validated = $request->validate([
            'tasks' => 'required|integer',
            'communications' => 'required|integer',
            'hours' => 'required',
            'tasks_matched' => 'nullable|boolean',
        ]);
        // Le ore di lavoro possono essere un numero decimale formattato con la virgola
        if (isset($validated['hours'])) {
            $validated['hours'] = str_replace(',', '.', $validated['hours']);
        }
        $validated['archived'] = 1;
        $activityDate->update($validated);
        return redirect(route('dashboard'));
    }

    /**
     * Riapre un report già chiuso
     *
     * @param  Request  $request
     * @return bool
     */
    public function unlock(Request $request)
    {
        $validated = $request->validate([
            'activity_date_id' => 'numeric|integer',
        ]);
        $report = ActivityDate::find($validated['activity_date_id']);
        if ($report) {
            $report->archived = 0;
            return $report->save();
        }
        return false;
    }


    /**
     * Porta alla pagina con le esportazioni disponibili
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function exportIndex()
    {
        $trasfertisti = User::permission('trasferta')->orderBy('name')->orderBy('surname')->get();
        return view('report.export', compact(['trasfertisti']));
    }


    /**
     * Esegue l'esportazione del report
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export(Request $request)
    {
        $validated = $request->validate([
            'date_start' => 'required|date',
            'date_end' => 'nullable|date',
            'department' => 'required|string',
            'user_id' => 'nullable|integer',
        ]);
        $date_start = $validated['date_start'];
        $date_end = $validated['date_end'] ?? false;
        $department = $validated['department'];
        $user_id = $validated['user_id'] ?? false;

        // Costruisco il nome, es.
        // "nome_cognome_2022-03-22.xlsx" (singola giornata di un tecnico)
        // "tecnici_2022-03-22.xlsx" (singola giornata per tutti i tecnici),
        // "customer_care_2022-03-14_2022-03-18.xlsx" (intervallo temporale per tutti i tecnici)
        if ($user_id) {
            $prefix = str_replace(' ', '_', mb_strtolower(User::find($user_id)->fullName, 'UTF-8'));
        } else {
            $prefix = str_replace(" ", "_", $department);
        }
        $fileName = $prefix . "_" . implode('_', array_filter([$date_start, $date_end])) . ".xlsx";

        // File Excel
        switch ($department) {
            case 'tecnici':
                $report = Excel::download(new ReportTecniciExport($date_start, $date_end, $user_id), $fileName);
                break;
            case 'customer care':
                $report = Excel::download(new ReportCustomerCareExport($date_start, $date_end, $user_id), $fileName);
                break;
            case 'venditori':
                $report = Excel::download(new ReportVenditoriExport($date_start, $date_end, $user_id), $fileName);
                break;
            case 'venditori_marketing':
                $report = Excel::download(new ReportVenditoriMarketingExport($date_start, $date_end, $user_id), $fileName);
                break;
            case 'amministrazione':
                $report = Excel::download(new ReportAmministrazioneExport($date_start, $date_end, $user_id), $fileName);
                break;
        }
        return $report;
    }


    /**
     * Esegue l'esportazione per i rimborsi chilometrici dei dipendenti
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function exportKm(Request $request)
    {
        $validated = $request->validate([
            'date_start' => 'required|date',
            'date_end' => 'required|date',
            'user_id' => 'nullable|integer',
        ]);
        $date_start = $validated['date_start'];
        $date_end = $validated['date_end'] ?? false;
        $user = isset($validated['user_id']) ? User::find($validated['user_id']) : auth()->user();

        // Costruisco il nome del file, es. "nome_cognome_2022-03-14_2022-03-18.xlsx"
        $userName = str_replace(' ', '_', mb_strtolower($user->fullName, 'UTF-8'));
        $fileName = $userName . "_km_" . implode('_', array_filter([$date_start, $date_end])) . ".xlsx";

        return Excel::download(new KmExport($date_start, $date_end, $user), $fileName);
    }


    /**
     * Verifica con una chiamata al gestionale se risultano task esitati a gestionale ma non presenti nel report.<br>
     * Il metodo viene usato da una chiamata ajax.<br>
     * Il gestionale ritorna un file JSON con un oggetto con un'unica chiave "exit_status".
     *
     * @param Request $request
     * @return \Illuminate\Http\Client\Response
     */
    public static function f626VerifyReport(Request $request)
    {
        $validated = $request->validate([
            'activity_id' => 'integer|required',
        ]);
        $activityDate = ActivityDate::find($validated['activity_id']);
        $url = config('gestionale.url') . config('gestionale.api_folder') . self::API_VERIFY_REPORT;
        $tasks = $activityDate->activities()->whereNotNull('task_id')->get()->pluck('task_id')->toArray();

        // In locale simulo una chiamata che ritorna un paio di task mancanti
        if (config('app.env') == 'local') {
            $fakeData = [['task_id'=>12345, 'description'=>'Sopralluogo RSPP'], ['task_id'=>67890, 'description'=>'Consegna documento']]; // ['missing_tasks' => [12345, 67890]]
            $fakeStatus = 200;
            Http::fake([
                '*.facile626.it/*' => Http::response($fakeData, $fakeStatus),
            ]);
        }
        return Http::post($url, ['user_id' => $activityDate->user->f626_user_id, 'tasks' => implode(',', $tasks), 'date' => $activityDate->date->format('Y-m-d')])->json();
    }

}
