<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ActivityField extends Model
{
    use HasFactory;

    /**
     * Prepara un array con valori e testi da usare nei select del form d'inserimento dell'attività
     *
     * @return array
     */
    public function optionValues(): array
    {
        $array = [];
        switch ($this->option_values) {
            case 'users':
                $users = User::role(['operativo', 'responsabile'])
                    ->where('active', 1)
                    ->orderBy('name')
                    ->get();
                foreach ($users as $user) {
                    $array[] = ['value' => $user->id, 'text' => $user->fullName];
                }
                $array[] = ['value' => 0, 'text' => "Altra persona non in elenco"];
                break;

            case 'rdc_pa':
                $array = [
                    ['value' => "pa",  'text' => "PA"],
                    ['value' => "rdc", 'text' => "RDC"],
                    ['value' => "segnalazione", 'text' => "Segnalazione"],
                ];
                break;

            case 'outcome_in_person':
                $array = [
                    ['value' => "fatto_e_nuovo_app", 'text' => "Consulenza fatta e preso nuovo appuntamento"],
                    ['value' => "spostato",          'text' => "Consulenza NON fatta, appuntamento spostato"],
                    ['value' => "perso",             'text' => "Consulenza NON fatta, cliente perso"],
                    ['value' => "non_interessato",   'text' => "Consulenza fatta, cliente non interessato"],
                    ['value' => "contratto_f626",    'text' => "Consulenza fatta, contratto F626"],
                    ['value' => "contratto_corsi",   'text' => "Consulenza fatta, contratto corsi"],
                ];
                break;

            case 'outcome_call':
                $array = [
                    ['value' => "richiamare",      'text' => "Richiamare"],
                    ['value' => "inesistente",     'text' => "Numero inesistente"],
                    ['value' => "non_risponde",    'text' => "Non risponde"],
                    ['value' => "non_interessato", 'text' => "Non interessato"],
                    ['value' => "appuntamento",    'text' => "Appuntamento in agenda"],
                ];
                break;

            case 'outcome_pa':
                $array = [
                    ['value' => "richiamare",      'text' => "Richiamare"],
                    ['value' => "non_interessato", 'text' => "Non interessato"],
                    ['value' => "appuntamento",    'text' => "Appuntamento in agenda"],
                ];
                break;

            case 'role':
                $array = [
                    ['value' => "r", 'text' => "RSPP"],
                    ['value' => "h", 'text' => "HACCP"],
                ];
                break;

            case 'document_type':
                $array = [
                    ['value' => "d",  'text' => "DVR"],
                    ['value' => "di", 'text' => "DVR (integrazione)"],
                    ['value' => "h",  'text' => "Manuale HACCP"],
                    ['value' => "hi", 'text' => "Manuale HACCP (integrazione)"],
                ];
                break;
        }
        return $array;
    }
}
