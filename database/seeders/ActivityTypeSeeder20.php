<?php

namespace Database\Seeders;

use App\Models\ActivityCategory;
use App\Models\ActivityType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ActivityTypeSeeder20 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = ActivityCategory::all()->pluck('id', 'name');

        // Nuova attività "Verifica incassi in sospeso eSOLVER/F626"
        ActivityType::create(
            [
                'name' => "Verifica incassi in sospeso eSOLVER/F626",
                'description' => null,
                'average_time' => null, // Tempo medio da definire
                'activity_category_id' => $categories['Amministrazione'],
                'fields' => "activity_minutes",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 0,
                'venditore' => 0,
                'amministrativo' => 1,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        // Aggiungere la quantità
        ActivityType::where('name', "Registrazione e invio incassi F626")->update(
            [
                'fields' => "quantity,activity_minutes",
            ]
        );
        ActivityType::where('name', "eSOLVER - Registrazione incasso")->update(
            [
                'fields' => "quantity,activity_minutes",
            ]
        );
        ActivityType::where('name', "Protocollo contratto ed emissione fattura corrente")->update(
            [
                'fields' => "quantity,activity_minutes",
            ]
        );

    }
}
