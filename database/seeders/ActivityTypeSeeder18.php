<?php

namespace Database\Seeders;

use App\Models\ActivityCategory;
use App\Models\ActivityType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ActivityTypeSeeder18 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = ActivityCategory::all()->pluck('id', 'name');

        // Nuova attività del customer care "Chiamata gradimento"
        ActivityType::create(
            [
                'name' => "Chiamata di gradimento",
                'description' => null,
                'average_time' => 10, // Confermato da Ecoretti
                'activity_category_id' => $categories['Contatto'],
                'fields' => "activity_minutes,client_id",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 1,
                'venditore' => 0,
                'amministrativo' => 0,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

        // Nuova attività del customer care "Appuntamento modificato o annullato"
        ActivityType::create(
            [
                'name' => "Appuntamento modificato o annullato",
                'description' => "Modifica o annullamento dell'appuntamento da parte dell'azienda",
                'average_time' => 5, // Confermato da Ecoretti
                'activity_category_id' => $categories['Contatto'],
                'fields' => "activity_minutes,client_id",
                'paulitti_id' => 0, // Non usato
                'tecnico' => 0,
                'customer_care' => 1,
                'venditore' => 0,
                'amministrativo' => 0,
                'order' => 0, // Non usato
                'visible' => 1,
            ]
        );

    }
}
