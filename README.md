## Report Web
[URL di produzione](https://report.facile626.it/)

L’obiettivo del progetto è quello di velocizzare il processo di inserimento, recupero e verifica dei dati dei report compilati quotidianamente dai tecnici, e in seguito customer care e venditori.
I report venivano compilati su file Excel, inviati quotidianamente ai responsabili dell’elaborazione statistica o commerciale dei dati.
Le funzionalità del progetto verranno incluse nel nuovo gestionale in fase di sviluppo.

## 1.0.0
1/5/2022

La prima versione ha abbattuto i tempi di recupero dei dati dei tecnici, passando dall’apertura di ogni singolo file excel e successivo copia-e-incolla nel file excel generale, a un pulsante che scarica tutti i dati dei tecnici in formato Excel formattato come da specifica del file di destinazione.

Il sito inoltre presenta ai tecnici le attività in maniera più semplice, e per ogni attività da riportare vengono presentati solo i campi relativi all’attività scelta. Nel file Excel era lasciata al tecnico la possibilità di scegliere quali campi compilare, con conseguenti errori.

Gli utenti possono accedere con l’e-mail aziendale e la stessa password del gestionale.

## 1.1.0
1/6/2022

Rifinitura dei campi dopo il feedback dei tecnici, e attivazione del report al reparto customer care.

Aggiunta della funzione di esportazione delle attività con km in modo da far risparmiare tempo ai tecnici nella compilazione del rimborso chilometrico di fine mese.

## 1.1.1
6/7/2022

Nasconde i tempi totali (viaggio, attività, somma) agli utenti, lasciandoli visibili solo ai responsabili.

## 1.2.0
28/7/2022

Aggiunge i dati ottenuti dalle API Google Distance Matrix e i tempi medi per le attività di tecnici e customer care.

La pagina di dettaglio del report mostra alcune informazioni aggiuntive.

## 1.3.0
4/8/2022

Report disponibili anche ai venditori, con relative esportazioni per i rimborsi chilometrici e per le statistiche.

## 1.3.1
25/8/2022

È possibile inserire report con data massima fino a un mese da oggi, per poter inserire le assenze in anticipo.

## 1.3.2
29/8/2022

Aggiunge una vista con i report delle vendite.

## 1.4.0
19/9/2022

Aggiunge l'esportazione per il marketing con i relativi privilegi.

## 1.4.1
28/9/2022

Aggiunge i dati di viaggio all'affiancamento ricevuto

## 1.4.2
6/10/2022

Aggiunge due colonne (numero contratti e valore contratti) all'indice dei report compilati dai venditori.

## 1.4.3
24/10/2022

Aggiunge i dati di viaggio all'affiancamento fatto

## 1.4.4
25/10/2022

Revisione report vendite giornaliero

## 1.4.5
26/10/2022

In caso di riapertura del report (es. per correzione di errori) vengono mantenuti i valori precedentemente inseriti di task, comunicazioni e ore timbrate.
Aggiunge l'attività "Redazione modello documento".

## 1.4.6
21/11/2022

Aggiunge la fonte "Segnalazione" oltre a RDC e PA (richiesta di Silvia Bigolaro).
Aggiunge i dati di viaggio all'attività "Manutenzione veicolo" (ticket 8670 di Marta Sodano).

## 1.4.7
1/12/2022

Nasconde dalla dashboard dei responsabili le persone non più in forza.

## 1.4.8
7/12/2022

Aggiunge alla dashboard un riscontro visivo con l'indicazione se sono presenti report compilati, mancanti o parziali.

Verde: compilato; rosso: mancante; giallo: parziale

## 1.5.0
12/1/2023

Report disponibile anche all'amministrazione, con relativa esportazione per le statistiche.
