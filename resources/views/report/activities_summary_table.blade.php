{{-- Tabella con le attività già inserite --}}
@if($activities->count())
    @php
        $tecnico = $activityDate->user->hasAnyRole('tecnico');
        $cc = $activityDate->user->hasAnyRole('addetto customer care');
        $venditore = $activityDate->user->hasAnyRole('venditore');
        $amministrativo = $activityDate->user->hasAnyRole('amministrativo');
    @endphp
    <div class="table-responsive mb-4">
        <table class="table">
            {{-- HEAD --}}
            <thead>
            <tr>
                <th scope="col">Attività</th>

                @if($tecnico || $cc || $amministrativo)
                    <th scope="col">Q.tà</th>
                @endif

                <th scope="col">Minuti<br>attività</th>

                @if($tecnico || $venditore)
                    <th scope="col">Da</th>
                    <th scope="col">A</th>
                    <th scope="col">Km *</th>
                    <th scope="col">Minuti<br>viaggio&nbsp;*</th>
                @endif

                <th scope="col">ID cliente</th>

                @if($tecnico)
                    <th scope="col">ID UL</th>
                @endif

                @if($tecnico || $venditore || $amministrativo)
                    <th scope="col">ID task</th>
                @endif

                @if($tecnico)
                    <th scope="col">ID corso</th>
                @endif

                @if($venditore)
                    <th scope="col">Fonte</th>
                    <th scope="col">Valore</th>
                    <th scope="col">Esito</th>
                @endif

                @if($tecnico || $cc || $venditore)
                    <th scope="col">Persona</th>
                @endif
            </tr>
            </thead>

            {{-- BODY --}}
            <tbody>
            @foreach($activities as $activity)
                <tr>
                    {{-- Attività --}}
                    <td>
                        @if(Route::currentRouteName() == 'report.edit')
                            <a class="link-secondary js-delete" data-id="{{ $activity->id }}" href="{{ route('activity.destroy', $activity) }}"><i class="fa-regular fa-trash-can"></i></a>
                        @endif
                        {{ $activity->name }}
                        @if($activity->activityType->description && auth()->user()->can('visualizzare totali'))
                            <br><span class="text-muted small">{{ $activity->activityType->description }}</span>
                        @endif
                    </td>

                    {{-- Quantità --}}
                    @if($tecnico || $cc || $amministrativo)
                        <td>{{ $activity->quantity }}</td>
                    @endif

                    {{-- Minuti attività --}}
                    <td>{{ $activity->activity_minutes }}</td>

                    {{-- Dati trasferta --}}
                    @if($tecnico || $venditore)
                        <td>{{ $activity->travel_from ?: '' }}</td>
                        <td>{{ $activity->travel_to ?: '' }}</td>
                        <td>{{ $activity->km ?: '' }}{!! isset($activity->km_google) ? '&nbsp;<span class="text-muted">(' . $activity->km_google . ')</span>' : '' !!}</td>
                        <td>{{ $activity->travel_minutes ?: '' }}{!! isset($activity->travel_minutes_google) ? '&nbsp;<span class="text-muted">(' . $activity->travel_minutes_google . ')</span>' : '' !!}</td>
                    @endif

                    {{-- ID cliente --}}
                    <td>{{ $activity->client_id }}</td>

                    {{-- ID UL --}}
                    @if($tecnico)
                        <td>{{ $activity->address_id }}</td>
                    @endif

                    {{-- ID task --}}
                    @if($tecnico || $venditore || $amministrativo)
                        <td>{{ $activity->task_id }}</td>
                    @endif

                    {{-- ID corso --}}
                    @if($tecnico)
                        <td>{{ $activity->course_id }}</td>
                    @endif

                    {{-- Campi specifici venditori --}}
                    @if($venditore)
                        <td>{{ $activity->rdc_pa }}</td>
                        <td>{{ $activity->value }}</td>
                        <td>{{ $activity->outcome_in_person }}{{ $activity->outcome_call }}{{ $activity->outcome_pa }}</td>
                    @endif

                    {{-- Persona --}}
                    @if($tecnico || $cc || $venditore)
                        <td>{{ $activity->target_user_name }}</td>
                    @endif
                </tr>
            @endforeach
            </tbody>

            {{-- FOOTER --}}
            @can('visualizzare totali')
                <tfoot>

                    {{-- Totali dichiarati --}}
                    <tr>
                        <td><strong>Totali dichiarati</strong></td>

                        {{-- Quantità --}}
                        @if($tecnico || $cc)
                            <td></td>
                        @endif

                        {{-- Minuti attività --}}
                        <td>
                            @if($activityDate->activityMinutes)
                                <strong>{{ $activityDate->activityMinutes }}'</strong>&nbsp;<small>{!! $activityDate::formattedMinutes($activityDate->activityMinutes) !!}</small>
                            @endif
                        </td>

                        {{-- Dati trasferta --}}
                        @if($tecnico || $venditore)
                            <td colspan="2"></td>
                            <td><strong>{{ $activities->sum('km') }}&nbsp;Km</strong></td>
                            <td>
                                @if($activityDate->travelMinutes)
                                    <strong>{{ $activityDate->travelMinutes }}'</strong>&nbsp;<small>{!! $activityDate::formattedMinutes($activityDate->travelMinutes) !!}</small>
                                @endif
                            </td>
                        @endif

                        {{-- ID cliente --}}
                        <td></td>

                        {{-- ID UL --}}
                        @if($tecnico)
                            <td></td>
                        @endif

                        {{-- ID task --}}
                        @if($tecnico || $venditore)
                            <td></td>
                        @endif

                        {{-- ID corso --}}
                        @if($tecnico)
                            <td></td>
                        @endif

                        {{-- Campi specifici venditori --}}
                        @if($venditore)
                            <td></td>
                            <td>{{ $activities->sum('value') }}</td>
                            <td></td>
                        @endhasanyrole

                        {{-- Persona --}}
                        <td></td>
                    </tr>


                    {{-- Totali km / tempo di viaggio calcolati da Google --}}
                    @if($tecnico || $venditore)
                        <tr>
                            <td><strong>Totali Google</strong></td>

                            {{-- Quantità --}}
                            @if($tecnico || $cc)
                                <td></td>
                            @endif

                            {{-- Minuti attività --}}
                            <td></td>

                            {{-- Dati trasferta --}}
                            <td colspan="2"></td>
                            <td>
                                @if($activities->sum('km_google'))
                                    <strong>{{$activities->sum('km_google')}}&nbsp;Km</strong>
                                @endif
                            </td>
                            <td>
                                @if($activities->sum('travel_minutes_google'))
                                    <strong>{{$activities->sum('travel_minutes_google')}}'</strong>&nbsp;<small>{!! $activityDate::formattedMinutes($activities->sum('travel_minutes_google')) !!}</small>
                                @endif
                            </td>

                            {{-- ID cliente --}}
                            <td></td>

                            {{-- ID UL --}}
                            @if($tecnico)
                                <td></td>
                            @endif

                            {{-- ID task --}}
                            @if($tecnico || $venditore)
                                <td></td>
                            @endif

                            {{-- ID corso --}}
                            @if($tecnico)
                                <td></td>
                            @endif

                            {{-- Campi specifici venditori --}}
                            @if($venditore)
                                <td colspan="3"></td>
                            @endif

                            {{-- Persona --}}
                            <td></td>
                        </tr>

                        {{-- Differenza tra quanto dichiarato e quanto calcolato da Google --}}
                        <tr>
                            <td><strong>Differenza</strong></td>

                            {{-- Quantità --}}
                            @if($tecnico || $cc)
                                <td></td>
                            @endif

                            {{-- Minuti attività --}}
                            <td></td>

                            {{-- Dati trasferta --}}
                            <td colspan="2"></td>
                            <td>
                                @if($activities->sum('km') && $activities->sum('km_google'))
                                    <strong>{{sprintf("%+01.1f", $activities->sum('km') - $activities->sum('km_google'))}}&nbsp;Km</strong>
                                @endif
                            </td>
                            <td>
                                @if($activityDate->travelMinutes && $activities->sum('travel_minutes_google'))
                                    <strong>{{sprintf("%+d", $activityDate->travelMinutes - $activities->sum('travel_minutes_google'))}}'</strong>&nbsp;<small>{!! $activityDate::formattedMinutes($activityDate->travelMinutes - $activities->sum('travel_minutes_google')) !!}</small>
                                @endif
                            </td>

                            {{-- ID cliente --}}
                            <td></td>

                            {{-- ID UL --}}
                            @if($tecnico)
                                <td></td>
                            @endif

                            {{-- ID task --}}
                            @if($tecnico || $venditore)
                                <td></td>
                            @endif

                            {{-- ID corso --}}
                            @if($tecnico)
                                <td></td>
                            @endif

                            {{-- Campi specifici venditori --}}
                            @if($venditore)
                                <td colspan="3"></td>
                            @endhasanyrole

                            {{-- Persona --}}
                            <td></td>
                        </tr>
                    @endif

                </tfoot>
            @endcan
        </table>
        @if($activity->user->can('trasferta'))
            <p class="text-muted"><em>* Il valore tra parentesi riporta il dato fornito da Google</em></p>
        @endif
    </div>

@endif
