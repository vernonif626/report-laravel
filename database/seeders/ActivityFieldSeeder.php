<?php

namespace Database\Seeders;

use App\Models\ActivityField;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ActivityFieldSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $activityFields = [
            [
                'name' => "quantity",
                'description' => "Quantità",
                'type' => "number",
                'inputmode' => "numeric",
                'size' => 2,
                'min' => null,
                'max' => null,
                'step' => null,
                'extra' => null,
                'option_values' => null,
            ],
            [
                'name' => "activity_minutes",
                'description' => "Minuti totali per l'attività",
                'type' => "number",
                'inputmode' => "numeric",
                'size' => 2,
                'min' => null,
                'max' => "600",
                'step' => null,
                'extra' => null,
                'option_values' => null,
            ],
            [
                'name' => "travel_from",
                'description' => "Località di partenza",
                'type' => "text",
                'inputmode' => "text",
                'size' => 3,
                'min' => null,
                'max' => null,
                'step' => null,
                'extra' => null,
                'option_values' => null,
            ],
            [
                'name' => "travel_to",
                'description' => "Località di destinazione",
                'type' => "text",
                'inputmode' => "text",
                'size' => 3,
                'min' => null,
                'max' => null,
                'step' => null,
                'extra' => null,
                'option_values' => null,
            ],
            [
                'name' => "km",
                'description' => "Km percorsi",
                'type' => "number",
                'inputmode' => "numeric",
                'size' => 2,
                'min' => null,
                'max' => null,
                'step' => null,
                'extra' => null,
                'option_values' => null,
            ],
            [
                'name' => "travel_minutes",
                'description' => "Minuti di viaggio",
                'type' => "number",
                'inputmode' => "numeric",
                'size' => 2,
                'min' => null,
                'max' => null,
                'step' => null,
                'extra' => null,
                'option_values' => null,
            ],
            [
                'name' => "client_id",
                'description' => "ID cliente",
                'type' => "number",
                'inputmode' => "numeric",
                'size' => 2,
                'min' => null,
                'max' => null,
                'step' => null,
                'extra' => null,
                'option_values' => null,
            ],
            [
                'name' => "address_id",
                'description' => "ID unità locale",
                'type' => "number",
                'inputmode' => "numeric",
                'size' => 2,
                'min' => null,
                'max' => null,
                'step' => null,
                'extra' => null,
                'option_values' => null,
            ],
            [
                'name' => "task_id",
                'description' => "ID task",
                'type' => "number",
                'inputmode' => "numeric",
                'size' => 2,
                'min' => null,
                'max' => null,
                'step' => null,
                'extra' => null,
                'option_values' => null,
            ],
            [
                'name' => "course_id",
                'description' => "ID corso",
                'type' => "number",
                'inputmode' => "numeric",
                'size' => 2,
                'min' => null,
                'max' => null,
                'step' => null,
                'extra' => null,
                'option_values' => null,
            ],
            [
                'name' => "target_user_id",
                'description' => "Persona",
                'type' => "select",
                'inputmode' => null,
                'size' => 3,
                'min' => null,
                'max' => null,
                'step' => null,
                'extra' => null,
                'option_values' => "users",
            ],
            /*
            [
                'name' => "",
                'description' => "",
                'type' => "",
                'inputmode' => "",
                'size' => 2,
                'min' => null,
                'max' => null,
                'step' => null,
                'extra' => null,
                'option_values' => null,
            ],
            */
        ];

        foreach ($activityFields as $activityField) {
            ActivityField::create($activityField);
        }
    }
}
