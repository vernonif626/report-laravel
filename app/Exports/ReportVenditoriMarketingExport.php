<?php

namespace App\Exports;

use App\Models\Activity;
use App\Models\ActivityDate;
use App\Models\ActivityType;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

/**
 * Esportazione report dei venditori per il marketing. <br>
 * Utilizza <a href="https://docs.laravel-excel.com/3.1/getting-started/">Laravel Excel</a>,
 * basato su <a href="https://phpspreadsheet.readthedocs.io/">PhpSpreadsheet</a>.
 */
class ReportVenditoriMarketingExport implements FromCollection, WithStrictNullComparison, WithHeadings
{
    const ROLE = 'venditore';

    protected string $date_start;
    protected string $date_end;
    protected int $user_id;

    public function __construct($date_start, $date_end=null, $user_id=false)
    {
        $this->date_start = $date_start;
        $this->date_end = $date_end ?: $date_start;
        $this->user_id = $user_id;
    }

    public function headings(): array
    {
        return [
            'Data',
            'Venditore',
            'RDC ricevuti',
            'Numeri inesistenti',
            'RDC chiamati',
            'App. presi da RDC',
            'App. saltati/persi',
            'App. fatti da RDC',
            'Contratti fatti da RDC',
            'Valore totale',
        ];
    }


    /**
    * @return Collection
    */
    public function collection(): Collection
    {
        $activityDates = ActivityDate::with('activities')->with('user')
            ->where('date', '>=', $this->date_start)
            ->where('date', '<=', $this->date_end)
            ->where('archived', '=', 1)
            ->where('absent', 0)
            // Dove il ruolo dell'utente è ROLE
            ->whereHas('user', function (Builder $query) {
                $users = User::role(self::ROLE)->get()->pluck('id');
                $query->whereIn('id', $users);
            });
        if ($this->user_id) {
            $activityDates = $activityDates->where('user_id', $this->user_id);
        }
        $activityDates = $activityDates->orderBy('date')->get();

        // Creo l'array per l'esportazione
        $activitiesExport = [];

        $rdcChiamatiGiaContati = [];

        foreach ($activityDates as $activityDate) {

            $rdcChiamati = 0;

            // RDC chiamati parte 1:
            // 5 "non risponde" sullo stesso cliente inclusa la settimana precedente = 1 RDC lavorato
            // (dopo 5 "non risponde" si chiude il task automaticamente)
            $contattiLavoratiStartDate = Carbon::create($this->date_start)->subWeek()->toDateString();
            $contattiLavoratiActivities = Activity::where([
                ['date', '>=', $contattiLavoratiStartDate],
                ['date', '<=', $this->date_end],
                ['rdc_pa', 'rdc'],
                ['activity_type_id', ActivityType::where('name', 'Contatto lavorato')->first()->id],
                ['outcome_call', 'non_risponde'],
            ]);
            if ($this->user_id) {
                $contattiLavoratiActivities = $contattiLavoratiActivities->where('user_id', $this->user_id);
            }
            $contattiLavoratiActivities = $contattiLavoratiActivities->get();

            $clients = [];
            foreach ($contattiLavoratiActivities as $contattiLavoratiActivity) {
                if (!array_key_exists($contattiLavoratiActivity->client_id, $clients)) { // in produzione la riga 126 dà errore "undefined array key"
                    $clients[$contattiLavoratiActivity->client_id] = 0;
                }
                $clients[$contattiLavoratiActivity->client_id] ++;
                if ($clients[$contattiLavoratiActivity->client_id] == 5 && !in_array($contattiLavoratiActivity->client_id, $rdcChiamatiGiaContati)) {
                    $rdcChiamati++;
                    $rdcChiamatiGiaContati[] = $contattiLavoratiActivity->client_id;
                }
            }

            $rdcRicevuti = $activityDate->activities()->where('activity_type_id', ActivityType::where('name', 'RDC ricevuto')->first()->id)->count();
//            $rdcRicevuti = Activity::where([ ['activity_date_id', $activityDate->id], ['activity_type_id', ActivityType::where('name', 'RDC ricevuto')->first()->id] ])->count();

            $inesistenti = $activityDate->activities()->where([['rdc_pa', 'rdc'], ['outcome_call', 'inesistente']])->count();

            // RDC chiamati parte 2: aggiungo quelli che non sono interessati o che fissano un appuntamento
            $rdcChiamati += $activityDate->activities()->where([['rdc_pa', 'rdc'], ['outcome_call', 'non_interessato']])->count();
            $rdcChiamati += $activityDate->activities()->where([['rdc_pa', 'rdc'], ['outcome_call', 'appuntamento']])->count();

            $appuntamentiPresi = $activityDate->activities()->where([['rdc_pa', 'rdc'], ['outcome_call', 'appuntamento']])->count();

            $appuntamentiPersi = $activityDate->activities()->where('activity_type_id', ActivityType::where('name', 'Primo appuntamento')->first()->id)
                ->where([ ['rdc_pa', 'rdc'], ['outcome_in_person', 'perso'] ])
                ->count();

            $appuntamentiFatti = $activityDate->activities()
                ->where('activity_type_id', ActivityType::where('name', 'Primo appuntamento')->first()->id)
                ->where('rdc_pa', 'rdc')
                ->whereNot([ ['outcome_in_person', 'spostato'], ['outcome_in_person', 'perso'] ])
                ->count();

            $appuntamentiFatti += $activityDate->activities()
                ->where('activity_type_id', ActivityType::where('name', 'Ripasso contratto')->first()->id)
                ->where('rdc_pa', 'rdc')
                ->whereNot([ ['outcome_in_person', 'spostato'], ['outcome_in_person', 'perso'] ])
                ->count();

            $appuntamentiFatti += $activityDate->activities()
                ->where('activity_type_id', ActivityType::where('name', 'Contratto F626')->first()->id)
                ->where('rdc_pa', 'rdc')
                ->count();

            $appuntamentiFatti += $activityDate->activities()
                ->where('activity_type_id', ActivityType::where('name', 'Contratto corsi')->first()->id)
                ->where('rdc_pa', 'rdc')
                ->count();

            $contrattiFatti = $activityDate->activities()->where([['value', '>', '0'], ['rdc_pa', 'rdc']])->count();

            $contrattiValore = $activityDate->activities()->where([['value', '>', '0'], ['rdc_pa', 'rdc']])->sum('value');

            $activitiesExport[] = [
                'Data' => $activityDate->date,
                'Venditore' => $activityDate->user->full_name,
                'RDC ricevuti' => $rdcRicevuti,
                'Numeri inesistenti' => $inesistenti,
                'RDC chiamati' => $rdcChiamati,
                'App. presi da RDC' => $appuntamentiPresi,
                'App. saltati/persi' => $appuntamentiPersi,
                'App. fatti da RDC' => $appuntamentiFatti,
                'Contratti fatti da RDC' => $contrattiFatti,
                'Valore totale' => $contrattiValore,
            ];

        }
        return collect($activitiesExport);
    }
}
