<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\Models\ActivityType;
use Carbon\Carbon;
use Carbon\Exceptions\Exception;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function vendite_show(string $date_start, string $date_end = "")
    {
        if (!$date_end) {
            $isRange = false;
            switch ($date_start) {
                case 'oggi':
                    $dayTxt = "Oggi";
                    $day = today();
                    $start = today()->startOfMonth();
                    $end = today()->endOfMonth();
                    break;
                case 'ieri':
                    $dayTxt = "Ieri";
                    $day = today()->yesterday();
                    $start = today()->yesterday()->startOfMonth();
                    $end = today()->yesterday()->endOfMonth();
                    break;
                default:
                    try {
                        $day = new Carbon($date_start); // mai fidarsi dell'utente
                    } catch (Exception $e) {
                        abort(500, 'Data non valida');
                    }
                    $dayTxt = (new Carbon($date_start))->isoFormat('D/M/YYYY');
                    $start = (new Carbon($date_start))->startOfMonth();
                    $end = (new Carbon($date_start))->endOfMonth();
                    break;
            }
        } else {
            $isRange = true;
            try {
                $start = new Carbon($date_start);
            } catch (Exception $e) {
                abort(500, 'Data non valida');
            }
            try {
                $end = new Carbon($date_end);
            } catch (Exception $e) {
                abort(500, 'Data non valida');
            }
            $day = $end;
            $dayTxt = (new Carbon($date_end))->isoFormat('D/M/YYYY');
        }

        // Array per il WHERE
        $day_where = ['date', $day->toDateString()];
        $start_where = ['date', '>=', $start->toDateString()];
        $end_where = ['date', '<=', $end->toDateString()];

        // Valore contratti
        $incasso_id = ActivityType::where('name', 'Incasso')->first()->id;
        $downgrade_id = ActivityType::where('name', 'Downgrade')->first()->id;
        $total_value_today = Activity::where([
            $day_where,
            ['value', '>', 0],
            ['activity_type_id', '<>', $incasso_id],
            ['activity_type_id', '<>', $downgrade_id]
        ])->sum('value');
        $total_value_month = Activity::where([
            $start_where,
            $end_where,
            ['value', '>', 0],
            ['activity_type_id', '<>', $incasso_id],
            ['activity_type_id', '<>', $downgrade_id]
        ])->sum('value');

        // RDC ricevuto
        $activityRdcId = ActivityType::where('name', 'RDC ricevuto')->first()->id;
        $total_rdc_today = Activity::where([$day_where, ['activity_type_id', $activityRdcId]])->count();
        $total_rdc_month = Activity::where([$start_where, $end_where, ['activity_type_id', $activityRdcId]])->count();

        // PA inserito
        $activityPaId = ActivityType::where('name', 'PowerApp inserito')->first()->id;
        $total_pa_today = Activity::where([$day_where, ['activity_type_id', $activityPaId]])->count();
        $total_pa_month = Activity::where([$start_where, $end_where, ['activity_type_id', $activityPaId]])->count();

        // Contatti lavorati
        $activityContattoLavoratoId = ActivityType::where('name', 'Contatto lavorato')->first()->id;
        $total_contatti_today = Activity::where([$day_where, ['activity_type_id', $activityContattoLavoratoId]])->count();
        $total_contatti_month = Activity::where([$start_where, $end_where, ['activity_type_id', $activityContattoLavoratoId]])->count();

        // Primi appuntamenti
        $activityPrimoAppuntamentoId = ActivityType::where('name', 'Primo appuntamento')->first()->id;
        $total_app_today = Activity::where([$day_where, ['activity_type_id', $activityPrimoAppuntamentoId], ['outcome_in_person', '<>', 'spostato'], ['outcome_in_person', '<>', 'perso']])
            ->count();
        $total_app_month = Activity::where([$start_where, $end_where, ['activity_type_id', $activityPrimoAppuntamentoId], ['outcome_in_person', '<>', 'spostato'], ['outcome_in_person', '<>', 'perso']])
            ->count();

        // Ripassi contratto
        $activityRipassoContrattoId = ActivityType::where('name', 'Ripasso contratto')->first()->id;
        $total_rip_today = Activity::where([$day_where, ['activity_type_id', $activityRipassoContrattoId], ['outcome_in_person', '<>', 'spostato'], ['outcome_in_person', '<>', 'perso']])
            ->count();
        $total_rip_month = Activity::where([$start_where, $end_where, ['activity_type_id', $activityRipassoContrattoId], ['outcome_in_person', '<>', 'spostato'], ['outcome_in_person', '<>', 'perso']])
            ->count();

        // Appuntamenti presi
        $total_app_presi_today = Activity::where([$day_where, ['outcome_in_person', 'appuntamento']])
            ->orWhere([$day_where, ['outcome_pa', 'appuntamento']])
            ->orWhere([$day_where, ['outcome_call', 'appuntamento']])
            ->count();
        $total_app_presi_month = Activity::where([$start_where, $end_where, ['outcome_in_person', 'appuntamento']])
            ->orWhere([$start_where, $end_where, ['outcome_pa', 'appuntamento']])
            ->orWhere([$start_where, $end_where, ['outcome_call', 'appuntamento']])
            ->count();

        // Contratti
        $activityContrattoF626Id = ActivityType::where('name', 'Contratto F626')->first()->id;
        $activityContrattoCorsiId = ActivityType::where('name', 'Contratto corsi')->first()->id;

        $total_ctr_today = Activity::where([$day_where, ['activity_type_id', $activityContrattoF626Id]])
            ->orWhere([$day_where, ['activity_type_id', $activityContrattoCorsiId]])
            ->orWhere([$day_where, ['outcome_in_person', 'contratto_f626']])
            ->orWhere([$day_where, ['outcome_in_person', 'contratto_corsi']])
            ->count();

        $total_ctr_month = Activity::where([$start_where, $end_where, ['activity_type_id', $activityContrattoF626Id]])
            ->orWhere([$start_where, $end_where, ['activity_type_id', $activityContrattoCorsiId]])
            ->orWhere([$start_where, $end_where, ['outcome_in_person', 'contratto_f626']])
            ->orWhere([$start_where, $end_where, ['outcome_in_person', 'contratto_corsi']])
            ->count();

        // Creo l'array con tutti i valori da passare alla vista
        $report_values = [
            [
                'name' => "RDC ricevuti",
                'whatsapp' => 'RDC',
                'description' => "",
                'day' => $total_rdc_today,
                'month' => $total_rdc_month,
            ],
            [
                'name' => "PowerApp",
                'whatsapp' => 'PA',
                'description' => "",
                'day' => $total_pa_today,
                'month' => $total_pa_month,
            ],
            [
                'name' => "Contatti lavorati",
                'whatsapp' => 'LAVORATI',
                'description' => "",
                'day' => $total_contatti_today,
                'month' => $total_contatti_month,
            ],
            [
                'name' => "Appuntamenti presi",
                'whatsapp' => 'APP PRESI',
                'description' => "Esito appuntamento su PA inserito, Contatto lavorato o Ripasso PA",
                'day' => $total_app_presi_today,
                'month' => $total_app_presi_month,
            ],
            [
                'name' => "Primi appuntamenti fatti",
                'whatsapp' => 'APP FATTI',
                'description' => "Primi appuntamenti con consulenza fatta (non spostati o persi)",
                'day' => $total_app_today,
                'month' => $total_app_month,
            ],
            [
                'name' => "Ripassi contratto fatti",
                'whatsapp' => 'RIP FATTI',
                'description' => "Ripassi contratto con consulenza fatta (non spostati o persi)",
                'day' => $total_rip_today,
                'month' => $total_rip_month,
            ],
            [
                'name' => "Contratti F626 + corsi firmati",
                'whatsapp' => 'CTR',
                'description' => "",
                'day' => $total_ctr_today,
                'month' => $total_ctr_month,
            ],
            [
                'name' => "Valore contratti",
                'whatsapp' => 'FATT',
                'description' => "",
                'day' => $total_value_today,
                'month' => $total_value_month,
            ],
        ];

        return view('report.vendite_index', compact([
            'isRange',
            'start',
            'end',
            'day',
            'dayTxt',
            'report_values',
        ]));
    }

    public function vendite_range(Request $request)
    {
        $validated = $request->validate([
            'date_start' => 'required|date',
            'date_end' => 'date',
        ]);
        $validated['date_end'] = $validated['date_end'] ?? "";
        return $this->vendite_show($validated['date_start'], $validated['date_end']);
    }

}
