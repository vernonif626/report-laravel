<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!Permission::where('name', 'compilare report')->count()) {
            Permission::create(['name' => 'compilare report']);
        }
        if (!Permission::where('name', 'visualizzare report')->count()) {
            Permission::create(['name' => 'visualizzare report']);
        }
        if (!Permission::where('name', 'visualizzare report tecnici')->count()) {
            Permission::create(['name' => 'visualizzare report tecnici']);
        }
        if (!Permission::where('name', 'visualizzare report customer care')->count()) {
            Permission::create(['name' => 'visualizzare report customer care']);
        }
        if (!Permission::where('name', 'visualizzare report venditori')->count()) {
            Permission::create(['name' => 'visualizzare report venditori']);
        }
        if (!Permission::where('name', 'esportare report')->count()) {
            Permission::create(['name' => 'esportare report']);
        }
        if (!Permission::where('name', 'esportare report tecnici')->count()) {
            Permission::create(['name' => 'esportare report tecnici']);
        }
        if (!Permission::where('name', 'esportare report customer care')->count()) {
            Permission::create(['name' => 'esportare report customer care']);
        }
        if (!Permission::where('name', 'esportare report venditori')->count()) {
            Permission::create(['name' => 'esportare report venditori']);
        }
    }
}
