<?php

namespace Database\Seeders;

use App\Models\ActivityType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\ActivityCategory;
use Illuminate\Support\Facades\DB;

class ActivityTypeSeeder2 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = ActivityCategory::all()->pluck('id', 'name');

        // Aggiornamento nome, descrizione e campi di alcune attività

        $at = ActivityType::where('name', "Consegna documento")->first();
        $at->description = "Spiegazione documento in presenza";
        $at->fields = 'activity_minutes,travel_from,travel_to,km,travel_minutes,client_id,address_id,task_id';
        $at->save();

        $at = ActivityType::where('name', "Consegna nomina")->first();
        $at->name = "Firma nomina (telefono)";
        $at->description = "Firma della nomina telefonica. Quella in presenza è inclusa nel tempo del sopralluogo.";
        $at->fields = "activity_minutes,client_id,address_id";
        $at->save();

        $at = ActivityType::where('name', "Affiancamento a supporto")->first();
        $at->name = "Affiancamento fatto";
        $at->save();

        $at = ActivityType::where('name', "Consulenza interna")->first();
        $at->description = "Consulenza fornita ai colleghi, su loro richiesta, per tematiche RSPP/HACCP";
        $at->save();

        $at = ActivityType::where('name', "Revisione materiale corso")->first();
        $at->description = "Aggiornamento materiale collegato ai corsi (Powerpoint, domande, ecc.)";
        $at->save();

        $at = ActivityType::where('name', "Organizzazione agenda")->first();
        $at->description = "Richiesta modifica agenda a customer care";
        $at->save();

        $at = ActivityType::where('name', "Articolo")->first();
        $at->description = "Redazione articolo su richiesta ufficio marketing";
        $at->save();

        $at = ActivityType::where('name', "Primo sopralluogo")->first();
        $at->fields = 'activity_minutes,travel_from,travel_to,km,travel_minutes,client_id,address_id,task_id';
        $at->save();

        $at = ActivityType::where('name', "Sopralluogo periodico")->first();
        $at->fields = 'activity_minutes,travel_from,travel_to,km,travel_minutes,client_id,address_id,task_id';
        $at->save();

        $at = ActivityType::where('name', "Sopralluogo covid")->first();
        $at->description = "Sopralluogo per redazione check-list covid";
        $at->fields = 'activity_minutes,travel_from,travel_to,km,travel_minutes,client_id,address_id,task_id';
        $at->save();

        $at = ActivityType::where('name', "Sopralluogo annullato da cliente")->first();
//        $at->name = "Sopralluogo annullato da cliente (presenza)";
//        $at->description = "Sopralluogo annullato dal cliente dopo essere arrivati a destinazione";
        $at->fields = 'activity_minutes,travel_from,travel_to,km,travel_minutes,client_id,address_id,task_id';
        $at->save();

        $at = ActivityType::where('name', "Visita extra")->first();
        $at->name = "Sopralluogo extra";
        $at->fields = 'activity_minutes,travel_from,travel_to,km,travel_minutes,client_id,address_id,task_id';
        $at->save();

        $at = ActivityType::where('name', "Docenza")->first();
//        $at->name = "Docenza (presenza)";
//        $at->description = "Attività di docenza in presenza";
        $at->fields = 'quantity,activity_minutes,travel_from,travel_to,km,travel_minutes,course_id';
        $at->save();

        $at = ActivityType::where('name', "Riunione")->first();
//        $at->name = "Riunione (presenza)";
//        $at->description = "Riunione aziendale o con colleghi in presenza";
        $at->fields = 'activity_minutes,travel_from,travel_to,km,travel_minutes';
        $at->save();

        $at = ActivityType::where('name', "Corso fruito")->first();
        $at->fields = 'quantity,activity_minutes,travel_from,travel_to,km,travel_minutes';
        $at->save();

        $at = ActivityType::where('name', "Redazione documento")->first();
        $at->description = "Stesura documento (DVR o manuale HACCP)";
        $at->save();

        $at = ActivityType::where('name', "Stampa documenti")->first();
        $at->description = "Stampa documenti su supporto cartaceo";
        $at->save();

        $at = ActivityType::where('name', "Upload documento")->first();
        $at->description = "Upload documento a gestionale";
        $at->save();

        $at = ActivityType::where('name', "Allineamento dati")->first();
        $at->description = "Revisione task del gestionale";
        $at->save();


        // La nuova attività "Attività extra a gestionale" sostituisce "Aggiornamento gestionale" e "Compilazione gestionale"
        $at = ActivityType::where('name', "Aggiornamento gestionale")->first();
        $at->visible = 0;
        $at->save();

        $at = ActivityType::where('name', "Compilazione gestionale")->first();
        $at->visible = 0;
        $at->save();

        $at = new ActivityType();
        $at->name = "Attività extra a gestionale";
        $at->description = "Attività a gestionale non previste da report o task";
        $at->activity_category_id = $categories['Gestionale'];
        $at->fields = "activity_minutes,client_id,address_id";
        $at->paulitti_id = 71;
        $at->tecnico = 1;
        $at->customer_care = 0;
        $at->order = 100;
        $at->visible = 1;
        $at->save();


        // La nuova attività "Contatto / risposta extra al cliente" sostituisce "Contatto cliente" e "Risposta a cliente"
        $at = ActivityType::where('name', "Contatto cliente")->first();
        $at->visible = 0;
        $at->save();

        $at = ActivityType::where('name', "Risposta a cliente")->first();
        $at->visible = 0;
        $at->save();

        $at = new ActivityType();
        $at->name = "Contatto / risposta extra al cliente";
        $at->description = "Attività non previste da report o task (telefonate, e-mail)";
        $at->activity_category_id = $categories['Contatto'];
        $at->fields = "activity_minutes,client_id,address_id";
        $at->paulitti_id = 72;
        $at->tecnico = 1;
        $at->customer_care = 0;
        $at->order = 130;
        $at->visible = 1;
        $at->save();


        // Nuove attività

//        $at = new ActivityType();
//        $at->name = "Docenza (FAD)";
//        $at->description = "Attività di docenza in videoconferenza";
//        $at->activity_category_id = $categories['Docenza'];
//        $at->fields = "quantity,activity_minutes,course_id";
//        $at->paulitti_id = 53;
//        $at->tecnico = 1;
//        $at->customer_care = 0;
//        $at->order = 70;
//        $at->visible = 1;
//        $at->save();

//        $at = new ActivityType();
//        $at->name = "Sopralluogo annullato da cliente (telefono)";
//        $at->description = "Sopralluogo annullato dal cliente in anticipo rispetto all'appuntamento";
//        $at->activity_category_id = $categories['Imprevisto'];
//        $at->fields = "client_id,address_id";
//        $at->paulitti_id = 53;
//        $at->tecnico = 1;
//        $at->customer_care = 0;
//        $at->order = 70;
//        $at->visible = 1;
//        $at->save();

        $at = new ActivityType();
        $at->name = "Comunicazioni";
        $at->description = "Apertura o risposta a comunicazioni a gestionale";
        $at->activity_category_id = $categories['Gestionale'];
        $at->fields = "activity_minutes";
        $at->paulitti_id = 74;
        $at->tecnico = 1;
        $at->customer_care = 0;
        $at->order = 125;
        $at->visible = 1;
        $at->save();

//        $at = new ActivityType();
//        $at->name = "Gestione sanzione/ispezione (telefono/e-mail)";
//        $at->description = "Tempo dedicato all'assistenza al cliente in caso di sanzioni o ispezioni (telefonate, e-mail)";
//        $at->activity_category_id = $categories['Imprevisto'];
//        $at->fields = "activity_minutes,client_id,address_id";
//        $at->paulitti_id = 73;
//        $at->tecnico = 1;
//        $at->customer_care = 0;
//        $at->order = 390;
//        $at->visible = 1;
//        $at->save();

        $at = new ActivityType();
        $at->name = "Gestione sanzione/ispezione";
        $at->description = "Tempo dedicato all'assistenza al cliente in caso di sanzioni o ispezioni (telefonate, e-mail, appuntamenti con cliente o organi ispettivi)";
        $at->activity_category_id = $categories['Imprevisto'];
        $at->fields = "activity_minutes,travel_from,travel_to,km,travel_minutes,client_id,address_id";
        $at->paulitti_id = 73;
        $at->tecnico = 1;
        $at->customer_care = 0;
        $at->order = 390;
        $at->visible = 1;
        $at->save();

        $at = new ActivityType();
        $at->name = "Rientro a casa";
        $at->description = "Km di viaggio per rientro a casa o in hotel";
        $at->activity_category_id = $categories['Sopralluogo'];
        $at->fields = "travel_from,travel_to,km,travel_minutes";
        $at->paulitti_id = 75;
        $at->tecnico = 1;
        $at->customer_care = 0;
        $at->order = 400;
        $at->visible = 1;
        $at->save();

    }
}
