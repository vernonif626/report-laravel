<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_types', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('fields')->comment('Campi della tabella activity_user da mostrare per questa attività, separati da virgola');
            $table->unsignedBigInteger('activity_category_id');
            $table->unsignedSmallInteger('paulitti_id')->comment("Codice dell'attività nell'Excel di Paulitti");
            $table->boolean('tecnico')->default(0);
            $table->boolean('customer_care')->default(0);
            $table->unsignedSmallInteger('order')->default(1000);

            $table->foreign('activity_category_id')
                ->references('id')
                ->on('activity_categories')
                ->onDelete('cascade');
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_types');
    }
};
