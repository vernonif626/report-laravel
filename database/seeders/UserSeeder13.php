<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder13 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Giovannella Pechini
        $pechini = User::create([
            'name'         => 'Giovannella',
            'surname'      => 'Pechini',
            'email'        => 'g.pechini@facile626.it',
            'password'     => (app()->environment('local') ? Hash::make('123') : Hash::make('9sU^')),
            'f626_user_id' => 75306,
        ]);
        $pechini->assignRole(['venditore', 'operativo']);
    }
}
