<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class UserSeeder4 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Elimino l'utente Isabella Lui
        $lui = User::where('surname', 'Lui')->first();
        $lui->delete();

        // Tolgo i permessi di responsabile a Grazia Fiorentino
        $fiorentino = User::where('surname', 'Fiorentino')->first();
        $roleResponsabile = Role::where('name', 'responsabile')->first();
        $roleRespTecnici = Role::where('name', 'responsabile tecnici')->first();
        $roleRespCC = Role::where('name', 'responsabile customer care')->first();
        $fiorentino->removeRole($roleResponsabile);
        $fiorentino->removeRole($roleRespTecnici);
        $fiorentino->removeRole($roleRespCC);

        // Assegno il ruolo di responsabile a Maurizio Morabito
        $morabito = User::where('surname', 'Morabito')->first();
        $morabito->assignRole('responsabile tecnici');
        $morabito->assignRole('responsabile customer care');
        $morabito->assignRole('responsabile');
    }
}
