<x-app-layout>
    <x-slot name="header">
        <div class="container mt-5 mb-5">
            <h2>
                Report del {{ $activityDate->localizedLongDate }}
            </h2>
            <p>
                @can('visualizzare report')<strong>{{ $activityDate->user->fullName }}</strong><br>@endcan
                Ore timbrate: <strong>{{ $activityDate->hours }}</strong><br>
                Task in ritardo: <strong>{{ $activityDate->tasks }}</strong><br>
                Comunicazioni in ritardo: <strong>{{ $activityDate->communications }}</strong>
            </p>
            @can('visualizzare totali')
                <p>
                @if($activityDate->totalMinutes > 0)
                    Totale lavorato: <strong>{!! \App\Models\ActivityDate::formattedMinutes($activityDate->totalMinutes, true) !!}</strong><br>
                    @foreach($activityDate->quickStats() as $description => $value)
                        {{ $description }}: <strong>{{ $value }}</strong><br>
                    @endforeach
                @endif
                </p>
            @endcan

            @can('esportare report')
                <form method="post" action="{{ route('report.export') }}">
                    @csrf
                    <input
                        name="date_start"
                        type="hidden"
                        value="{{ $activityDate->date->format('Y-m-d') }}"
                    >
                    <input
                        name="department"
                        type="hidden"
                        value="{{ $dept }}"
                    >
                    <input
                        name="user_id"
                        type="hidden"
                        value="{{ $activityDate->user->id }}"
                    >
                    <button type="submit" class="btn btn-sm btn-outline-primary">Scarica il file Excel di questo report</button>
                </form>
            @endcan
        </div>
    </x-slot>
    <div class="container">
        @include('report.activities_summary_table', compact(['activityDate', 'activities']))
    </div>
</x-app-layout>
